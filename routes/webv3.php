<?php 

use App\Events\OrderSuccess;
use App\PurchaseProduct;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

Route::get('/v3', function(){
	return view('v3.mainlayout');
});

Route::get('/v3/ngetes', function(){
	return view('v3.ngetes');
});


Route::get('/v3/redis/publish', function(){
	
	$id = 24;

	$data = [
        'event' => 'ConfirmPayment',
        'data' => $id
    	];
    	
    Redis::publish('pos-channel', json_encode($data));


});

Route::post('/v3/generate-token', function(Request $request){
	
	$user = $request->user();

	$token =  md5(date("Y-m-d H:i:s")).(1453+$user->id);

	$user->sms_banking_token = $token;

	$user->save();

	return $token;


});



Route::get('/pay/aja/{price}', function($price){

		// end

	$id = Redis::get('pos_sell_price.1.'.$price);

	if($id){

		Redis::del('pos_sell_price.1.'.$price);

		$data = PurchaseProduct::where('id',$id)->first();

		$data->status = 'success';

		$data->save();

		event(new OrderSuccess($data));

		$component_price = json_decode(($data->component_price));

		Redis::SADD('list_unique.1.'.$component_price->total_price,$component_price->unique);

		$data = [
        'event' => 'ConfirmPayment',
        'data' => $id
    	];
    	
	    Redis::publish('pos-channel', json_encode($data));

		
	}

	
});

Route::get('/v3/email/token', 'EmailController@getUserToken');


Route::post('/v3/email/register', 'EmailController@register');

Route::post('/v3/email/updateprofile', 'EmailController@updateprofile');

Route::get('/pos/delete/expired', function(){

		// end
	$date = Carbon::today()->subDays(2);

	$expired =  PurchaseProduct::where('status','checkout')->where('created_at','<',$date)->get();

	$expired->each(function ($item, $key) {

			// 1. cancel status
            
            $item->status = 'cancel';

            $item->save();

            // 2. delete redis sell price

            Redis::del('pos_sell_price.'.$item->author_id.'.'.$item->sell_price);

            // 3. push redis unik price array

            $component_price = json_decode($item->component_price);

            Redis::SADD('list_unique.'.$item->author_id.'.'.$component_price->total_price,$component_price->unique);
        
        }); 


	
});

Route::post('/v3/find-customer','V3POSController@find');

Route::resource('/v3/pixels', 'PixelsController');

Route::resource('/v3/adwords', 'AdwordsController');

Route::resource('/v3/domains', 'DomainsController');

Route::resource('/v3/orderlink', 'V3OrderlinkController');

Route::resource('/v3/pixelink', 'V3PixelinkController');

Route::resource('/v3/store', 'StoreController');

Route::resource('/v3/products', 'V3ProductsController');

Route::resource('/v3/pos', 'V3POSController');






Route::post('/v3/imageupload', function(Request $request){

	$image = $request->image;

	$name =  md5(date("Y-m-d H:i:s")).'.'.$image->getClientOriginalExtension();


	$path = $image->storeAs('public/images/'.$request->user()->id, $name);

	return '/storage/images/'.$request->user()->id.'/'.$name;

	


});
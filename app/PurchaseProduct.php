<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseProduct extends Model
{
    //
    protected $fillable = ['products','store_id','buyer_id','status','sell_price','component_price','courier','courier_package','noresi','pay_with','expired'];

    public function buyer()
    {
        return $this->belongsTo('App\UserCustomer');
    }

    public function store()
    {
        return $this->belongsTo('App\Store');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Affiliate_link extends Model
{
    //
    protected $fillable = ['author_id', 'url', 'alias'];
}

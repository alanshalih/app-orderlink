<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

class UserTrial
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(count($request->user()->roles)==0){
            if($request->user()->created_at < Carbon::now()->subWeek()){
                return redirect('http://ceksini.info/c/vcxlTDHD');
            }
        }
        return $next($request);
    }
}

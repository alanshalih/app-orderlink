<?php

namespace App\Http\Middleware;

use Closure;

class RedirectToOrderlink
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->root() != 'http://app.orderlink.in' && env('APP_ENV') === 'production' )
        return redirect('https://app.orderlink.in'.$request->getRequestUri());

    

        return $next($request);
    }
}

<?php

namespace App\Http\Controllers;

use App\Googleid;
use App\Orderlink;
use App\Orderlink_stat;
use App\Pixel;
use App\Userdomain;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class OrderlinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        //
        
        $grabLink = $this->fetchAll($req)->first();

        if($grabLink)
        
            return redirect('/orderlink/'.$grabLink->id);
        
        else
            {

                $pixel = $this->getPixelAll($req); 

                $google = $this->getGoogleAll($req);

                $domain1 = Userdomain::where('author_id',$req->user()->id)->where('status','running')->get();

                $domain2 = Userdomain::where('author_id',2)->get();

                $domains = $domain1->merge($domain2);          


                return view('orderlink.noquery',compact('pixel','google','domains'));
            }
            
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
          $pixel_id = null;
        $google_id = null;

        if($request->pixels){
            $pixel_id = Pixel::create(['pixel_id' => $request->pixels,'author_id'=>$request->User()->id])->id;
        }else{
            if($request->pixel_id){
                $pixel_id = $request->pixel_id;
            }
        }

        if($request->googles){
            $google_id = Googleid::create(['google_id' => $request->googles,'author_id'=>$request->User()->id])->id;
        }else{
             if($request->google_id){
                $google_id = $request->google_id;
            }
        }

        $isisms = rawurlencode($request->isisms);

        $newlink = Orderlink::create(['tipelink'=>$request->tipelink,
                               'author_id'=>$request->user()->id,
                               'nomor'=> $request->nomor,
                               'isisms'=> $isisms,
                               'alias'=>$request->alias,
                               'desktop_url'=>$request->desktop_url,
                               'pixel_event'=>$request->pixel_event,
                               'hit'=>0,
                               'pixel_id'=>$pixel_id,
                               'google_id'=>$google_id]);

         



        return redirect('/orderlink/'.$newlink->id)->with('status', 'Selamat! Orderlink anda sudah dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $req, $id)
    {
        //
        

        $browserChart = $this->getChart($id, 'browser');

        $platformChart = $this->getChart($id, 'platform');


        $unikHit = $this->getUnikHit($id,'today');

        $totalHit = $this->getHitStat($id, 'today');

        $grabLink = $this->fetchAll($req)->paginate(10);
   
        if(count($grabLink)>0){
            $getOneLink = $this->fetchAll($req)->where('id',$id)->first();

            $getOneLink->isisms = rawurldecode($getOneLink->isisms);

       

            $pixel = $this->getPixelAll($req); 

            $google = $this->getGoogleAll($req);    

            Redis::sadd('user.'.$req->user()->id.'.progress','orderlinkShow');

            $domain1 = Userdomain::where('author_id',$req->user()->id)->where('status','running')->get();

            $domain2 = Userdomain::where('author_id',2)->get();

            $domains = $domain1->merge($domain2);  

            return view('orderlink/show',compact('grabLink','getOneLink','pixel','google', 'totalHit', 'unikHit','domains','browserChart','platformChart'));
        }else{
            return redirect('errors.404');
        }
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
                  $pixel_id = null;
        $google_id = null;

        if($request->pixels){
            $pixel_id = Pixel::create(['pixel_id' => $request->pixels,'author_id'=>$request->User()->id])->id;
        }else{
            if($request->pixel_id){
                $pixel_id = $request->pixel_id;
            }
        }

        if($request->googles){
            $google_id = Googleid::create(['google_id' => $request->googles,'author_id'=>$request->User()->id])->id;
        }else{
             if($request->google_id){
                $google_id = $request->google_id;
            }
        }
        
        $orderlink = Orderlink::where('id',$id)->first();
        
        Redis::del($orderlink->alias);

        $isisms = rawurlencode($request->isisms);

        $orderlink->update([
                               'nomor'=> $request->nomor,
                               'isisms'=> $isisms,
                               'alias'=>$request->alias,
                               'desktop_url'=>$request->desktop_url,
                               'pixel_id'=>$pixel_id,
                               'pixel_event'=>$request->pixel_event,
                               'google_id'=>$google_id]);


        return  redirect('/orderlink/'.$id)->with('status', 'Orderlink telah diedit');
    



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //


        $p = Orderlink::find($id);

        Redis::ZREM('orderlink',$id);

        Redis::DEL($p->alias);

        $p->delete();

        return redirect('/orderlink')->with('status', 'Link telah dihapus');
    }

     public function fetchAll(Request $req)
    {
        $all = Orderlink::where('author_id',$req->user()->id)->latest();

        return $all;    
    }

    public function getPixelAll(Request $req)
    {
        $all = Pixel::where('author_id',$req->user()->id)->latest()->get();

        return $all;    
    }

    public function getGoogleAll(Request $req)
    {
        $all = Googleid::where('author_id',$req->user()->id)->latest()->get();

        return $all;    
    }

      public function getHitStat($id,$scope)
    {
        $format = '%H:00';

        $date = Carbon::today();



        if($scope == "lastweek"){

            $format = '%d/%m';

            $date = Carbon::today()->subWeek();

        }else if($scope== "lastmonth"){

            $format = '%d/%m';

            $date = Carbon::today()->subMonth();

        }else if($scope == "lastyear"){

             $format = '%b';

            $date = Carbon::today()->subYear();

        }

        $hit = Orderlink_stat::selectRaw("DATE_FORMAT(created_at,'".$format."') as week, count(id) as hit")
        ->where('orderlink_id', $id)
        ->where('created_at','>',$date)
        ->groupBy('week')
        ->pluck('hit','week');

        return ($hit);
    }



    public function getUnikHit($id,$scope)
    {

        $format = '%H:00';

        $date = Carbon::today();

        if($scope == "lastweek"){

            $format = '%d/%m';

            $date = Carbon::today()->subWeek();

        }else if($scope== "lastmonth"){
                  $format = '%d/%m';

            $date = Carbon::today()->subMonth();

        }else if($scope == "lastyear"){
                    $format = '%b';

            $date = Carbon::today()->subYear();
            
        }

        $hit = Orderlink_stat::selectRaw("DATE_FORMAT(created_at,'".$format."') as week, count(DISTINCT(fingerprint)) as hit")
        ->where('orderlink_id', $id)
        ->where('created_at','>',$date)
        ->groupBy('week')
        ->pluck('hit','week');

        return ($hit);
    }



    public function KlikStat($id, Request $request)
    {

        // return $request->all();
        $a = $this->getHitStat($id, $request->scope);

        $b =  $this->getUnikHit($id, $request->scope);

        return array('hit' => $a, 'unik' => $b);
    }

        public function getChart($id, $attr)
    {


       return DB::table('orderlink_stats')
                 ->select($attr, DB::raw('count(*) as total'))
                 ->where('orderlink_id', $id)
                 ->groupBy($attr)
                 ->pluck('total',$attr);

       
    }
}

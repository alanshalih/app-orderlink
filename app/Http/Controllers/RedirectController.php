<?php

namespace App\Http\Controllers;

use App\C_campaign;
use App\C_campaign_stat;
use App\Orderlink;
use App\Orderlink_stat;
use App\Pixelink;
use App\Pixelink_stat;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Jenssegers\Agent\Agent;

class RedirectController extends Controller
{
    //

    public function monthlyCounter($user)
    {
        if(Redis::exists('user.'.$user.'.monthlyCounter'))
        {
            return Redis::incr('user.'.$user.'.monthlyCounter');
        }
        else
        {
            Redis::setex('user.'.$user.'.monthlyCounter', 60*60*24*30, 1);

            return 1;
        }
    }

    function get_client_ip() {
    $ipaddress = '';
       if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];

    if(isset(($_SERVER['HTTP_CF_CONNECTING_IP']))){
        return ($_SERVER['HTTP_CF_CONNECTING_IP']);
    }

    return $ipaddress;
}

    public function cloak(Request $Request){
          
        return $this->get_client_ip();
        $getcampaign = Pixelink::where('alias','http://disini.pw/bsm')->first();

         $a=$getcampaign->allowed_country;

                     if(isset($getcampaign->cloak_link)){
                if($getcampaign->cloak_link){
                    $agent = new Agent();
                    
                    if($agent->isRobot()){
                        return redirect($getcampaign->cloak_redirect);
                    }else{

                        if(isset($getcampaign->allowed_country)){
                             $a=$getcampaign->allowed_country;

                             $ip = $this->get_client_ip();

                            $c = geoip()->getLocation($ip)['iso_code'];

                            return $c;

                            if(strpos($a, $c) !== false) {
                                return redirect($getcampaign->url);
                            }else{
                                return redirect($getcampaign->cloak_redirect);
                            }
                        }
                       
                    }
                }
            }


    }

  
    public function goto(Request $req)
    {
       
        // if(strcmp($req->header('referer'),'http:\/\/l.facebook.com\/l.php?u=http%3A%2F%2Flaramate.dev%2Fayam&h=jAQFGbLbc'){
        //     return 'ayam';
        // }

 
      
        $ua = (object) parse_user_agent($req->header('User-Agent'));

        

        if($getcampaign = Redis::exists($req->url()))
        {
            $getcampaign = json_decode(Redis::get($req->url()));

        }
        else
        {
            $getcampaign = Pixelink::
            where('alias',$req->url())
            ->with('google','pixel')->first();

            Redis::set($req->url(),$getcampaign);
        }



        



        if($getcampaign)
        {
           




            if($this->monthlyCounter($getcampaign->author_id) > Redis::get('user.'.$getcampaign->author_id.'.Limiter'))
            {
                if(Redis::get('user.'.$getcampaign->author_id.'.dompet') > 0){
                    Redis::decr('user.'.$getcampaign->author_id.'.dompet');
                }else{
                    return  view('errors.maxlimit');
                }
                
            }
            

            Redis::ZINCRBY('pixelink', 1, $getcampaign->id);



            $referer = (strlen($req->header('referer')) < 255 ? $req->header('referer') : substr($req->header('referer'), 0, 255));
            
            $statsave = Pixelink_stat::create(['campaign_id' => $getcampaign->id,
                     'ip'=>$req->ip(),
                     'referrer'=> $referer,
                     'platform'=>$ua->platform,
                     'browser'=>$ua->browser,
                     'fingerprint'=>md5($this->get_client_ip().$req->header('User-Agent'))]);

            if(isset($getcampaign->cloak_link)){
                if($getcampaign->cloak_link){
                    $agent = new Agent();
                    
                    if($agent->isRobot()){
                        return redirect($getcampaign->cloak_redirect);
                    }else{

                        if(isset($getcampaign->allowed_country)){
                             $a=$getcampaign->allowed_country;

                             $ip = $this->get_client_ip();

                            $c = geoip()->getLocation($ip)['iso_code'];

                            if(strpos($a, $c) !== false) {
                                return redirect($getcampaign->url);
                            }else{
                                return redirect($getcampaign->cloak_redirect);
                            }
                        }
                       
                    }
                }
            }
            
            if($getcampaign->pixel || $getcampaign->google )
                return view('pixelink.goto',compact('getcampaign'));
            else
                return redirect($getcampaign->url);

        }else{
            return redirect ('404');
        }


    	


    }

    public function contentlink(Request $req)
    {
       
      
        $ua = (object) parse_user_agent($req->header('User-Agent'));

        

        if($getcampaign = Redis::exists($req->url()))
        {
            $getcampaign = json_decode(Redis::get($req->url()));

        }
        else
        {
            $getcampaign = C_campaign::
            where('alias',$req->url())
            ->with('pixel', 'google', 'templates')->first();

            Redis::set($req->url(),$getcampaign);
        }


    

        if($getcampaign)
        {
  

            $templates = ( $getcampaign->templates);


            if($this->monthlyCounter($getcampaign->author_id) > Redis::get('user.'.$getcampaign->author_id.'.Limiter'))
            {
                if(Redis::get('user.'.$getcampaign->author_id.'.dompet') > 0){
                    Redis::decr('user.'.$getcampaign->author_id.'.dompet');
                }else{
                    return  view('errors.maxlimit');
                }
            }
            

            Redis::ZINCRBY('contentlink', 1, $getcampaign->id);



             $referer = (strlen($req->header('referer')) < 255 ? $req->header('referer') : substr($req->header('referer'), 0, 255));

            $statsave = C_campaign_stat::create(['campaign_id' => $getcampaign->id,
                     'ip'=>$req->ip(),
                     'referrer'=>$referer,
                     'platform'=>$ua->platform,
                     'browser'=>$ua->browser,
                     'fingerprint'=>md5($this->get_client_ip().$req->header('User-Agent'))]);
            

             return view('contentlink.page',compact('getcampaign'));

        }else{
            return redirect ('404');
        }


        


    }

    public function sms(Request $req)
    {   
        return $this->orderlink($req,'orderlink.sms');
    }

    public function bbm(Request $req)
    {   
        return $this->orderlink($req,'orderlink.bbm');
    }

    public function wa(Request $req)
    {   
       return $this->orderlink($req,'orderlink.wa');
    }

    public function wav2(Request $req)
    {   
       return $this->orderlink($req,'orderlink.wav2');
    }


       public function line(Request $req)
    {   
        return $this->orderlink($req,'orderlink.line');
    }

    public function telegram(Request $req)
    {   
        return $this->orderlink($req,'orderlink.telegram');
    }

    public function email(Request $req)
    {   
       return $this->orderlink($req,'orderlink.email');
    }

    public function orderlink(Request $req, String $tipelink){
        
        $ua = (object) parse_user_agent($req->header('User-Agent'));



        if($getcampaign = Redis::exists($req->url()))
        {
            $getcampaign = json_decode(Redis::get($req->url()));

        }
        else
        {
            $getcampaign = Orderlink::
            where('alias',$req->url())
            ->with('google','pixel')->first();

            Redis::set($req->url(),$getcampaign);
        }


        if($getcampaign)
        {
            if($tipelink == 'orderlink.sms'){
                if($ua->browser == 'Facebook'){
                     return redirect('intent://'.str_replace('http://','',$getcampaign->alias).'#Intent;scheme=http;end');
                }
               
            }


            if($this->monthlyCounter($getcampaign->author_id) > Redis::get('user.'.$getcampaign->author_id.'.Limiter'))
            {
                if(Redis::get('user.'.$getcampaign->author_id.'.dompet') > 0){
                    Redis::decr('user.'.$getcampaign->author_id.'.dompet');
                }else{
                    return  view('errors.maxlimit');
                }
            }
            

            Redis::ZINCRBY('orderlink', 1, $getcampaign->id);


             $referer = (strlen($req->header('referer')) < 255 ? $req->header('referer') : substr($req->header('referer'), 0, 255));
            
            $statsave = Orderlink_stat::create(['orderlink_id' => $getcampaign->id,
                     'ip'=>$req->ip(),
                     'referrer'=>$referer,
                     'platform'=>$ua->platform,
                     'browser'=>$ua->browser,
                     'fingerprint'=>md5($this->get_client_ip().$req->header('User-Agent'))]);



            

            $agent = new Agent();

            if($agent->isDesktop()){

                if(($getcampaign->desktop_url)){
                    if (strpos($getcampaign->desktop_url, 'http') !== false) {
                        return redirect($getcampaign->desktop_url);
                    }else{
                        $decodedMessage = rawurldecode($getcampaign->isisms);


                        return view('orderlink.wadesktopfull',compact('getcampaign','decodedMessage'));
                    }

                }
                else{
                    if($tipelink == 'orderlink.wav2' ){

                        $decodedMessage = rawurldecode($getcampaign->isisms);


                        return view('orderlink.wadesktopfull',compact('getcampaign','decodedMessage'));
                    }
                }
                
            }else{
                $platform = $ua->platform;
                return view($tipelink, compact('getcampaign','platform'));
            }

        }else{
            return redirect ('404');
        }
    }


    
}

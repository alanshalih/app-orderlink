<?php

namespace App\Http\Controllers;

use App\Events\StoreCreated;
use App\Store;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        return Store::where('owner_id', $request->user()->id)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $store = json_decode(json_encode($request->storedata));



        $store->owner_id = $request->user()->id;

        $store->banks = json_encode($store->banks);

        $store->adwords = json_encode($store->adwords);

         $store->pixels = json_encode($store->pixels);

        

        $s = Store::create((array) $store);

        event(new StoreCreated($s));

        return $s;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store)
    {
        //
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        
        $store = json_decode(json_encode($request->storedata));



        $store->owner_id = $request->user()->id;

        $store->banks = json_encode($store->banks);

        $store->adwords = json_encode($store->adwords);

         $store->pixels = json_encode($store->pixels);

        

        // $s = Store::create((array) $store);

        Store::where('id',$id)->update((array) $store);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store)
    {
        //
    }
}

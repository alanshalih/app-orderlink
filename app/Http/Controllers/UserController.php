<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class UserController extends Controller
{

    protected $role = 'admin';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $latestUser = User::latest()->first();
        return redirect ('/user/'.$latestUser->id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //


        $users = User::latest()->get();

        $user = User::where('id',$id)->first();

        $roles = Role::get();

        return view('admin.user',compact('users','user','roles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $users = User::latest()->get();

        $user = User::where('id',$id)->first();

        $roles = Role::get();

        return view('admin.edit-user',compact('users','user','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //
        $user = User::where('id',$id)->first();

        $user->update($request->all());

        return redirect ('/user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $p = User::where('id',$id);

        Redis::DEL('user.'.$id.'.Limiter');

        $p->delete();

        return redirect('/user')->with('status', 'User telah dihapus');
    }

    public function filterUser()
    {
        
        $users = User::whereHas('roles', function($q) {

            $q->where('name', $this->role);

        })->get();

        $user = $users[0];

        $roles = Role::get();

        return view('admin.user',compact('users','user','roles'));

    }

    public function admin()
    {
        $this->role = 'admin';

        $role = $this->filterUser();

        return $role;

    }

    public function platinum()
    {
        $this->role = 'platinum';
        
        $role = $this->filterUser();

        return $role;

    }

     public function gold()
    {
        $this->role = 'gold';
        
        $role = $this->filterUser();

        return $role;

    }

     public function diamond()
    {
        $this->role = 'diamond';
        
        $role = $this->filterUser();

        return $role;

    }

    public function free()
    {
        $users = User::whereDoesntHave('roles', function($q) {

        })->get();

        $user = $users[0];

        $roles = Role::get();

        return view('admin.user',compact('users','user','roles'));

    }

    public function pro()
    {
        $users = User::whereHas('roles', function($q) {

        })->get();

        $user = $users[0];

        $roles = Role::get();

        return view('admin.user',compact('users','user','roles'));

    }

    

    public function berikanRole(Request $request)
    {


        DB::table('role_user')->insert([
            'user_id' => $request->user,
            'role_id' => $request->role,
            'ends_at' => Carbon::now()->addMonths($request->bulan) 
        ]);

        $getUser = User::whereId($request->user)->first();
     
        Redis::set('user.'.$request->user.'.Limiter', $getUser->roles[0]->hit_limit);

        Redis::setex('user.'.$request->user.'.Role', 60*60*24*30*$request->bulan, $getUser->roles[0]->display_name);


       return redirect('/userv2/'.$request->user);
      
    }

    public function hapusRole(Request $request)
    {

        $user = User::find($request->user)->roles()->sync([]);

        Redis::del('user.'.$request->user.'.Limiter');

        Redis::del('user.'.$request->user.'.Role');
      
        return redirect('/user/'.$request->user);
    }

    public function getAllUsers(){

        return User::with('roles')->get();


    }

    public function adminlogin(){
        return view('auth.userlogin');
    }

    public function postadminlogin(Request $request){
       $user =  User::where('email',$request->email)->first();
        Auth::login($user);

        return redirect('/');
    }


}

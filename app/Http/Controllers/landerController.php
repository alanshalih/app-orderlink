<?php

namespace App\Http\Controllers;

use App\Landing_page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class landerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $data = Landing_page::where('author_id',$request->user()->id)->get();

        return view('lander.list', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('lander.contactPage.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
    

        $characters = 'orderlink';

        $url =  ($request->user()->id).str_shuffle ( $characters );

        $lp = Landing_page::create(['content'=>$request->data, 'author_id'=>$request->user()->id,'title'=>$request->title,'url'=>$url]);

        return redirect('/page/'.$lp->id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Landing_page::where('id',$id)->first();
        return view('lander.contactPage.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        if($request->img){
             $img = str_replace('data:image/jpeg;base64,', '', $request->img);
             $img = str_replace(' ', '+', $img);
             $data = base64_decode($img);

            Storage::put('public/page/preview-'.$id.'.jpg', $data);

            Landing_page::where('id',$id)->update(['thumbnail'=>'/storage/page/preview-'.$id.'.jpg']);

        }else{
            Landing_page::where('id',$id)->update($request->all());
        }



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Landing_page::destroy($id);

        return redirect('page');
    }

    public function RandomString()
    {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < 5; $i++) {
            $randstring = $randstring + 'ayam';
        }
        return $randstring;
    }

    public function page($id)
    {
        //
        $getcampaign = Landing_page::
            where('url',$id)->first();

        return view('lander.show',compact('getcampaign'));
    }

}

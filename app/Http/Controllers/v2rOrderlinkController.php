<?php

namespace App\Http\Controllers;

use App\Googleid;
use App\Orderlink;
use App\Orderlink_stat;
use App\Pixel;
use App\Userdomain;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class v2rOrderlinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $orderlink = new OrderlinkController();

        $all =  $orderlink->fetchAll($request)->get();

        $all->each(function ($item, $key) {

            $hit = Redis::ZSCORE("orderlink", $item->id);
            
            $item->hit = ($hit > 0) ? $hit : 0 ;
        
        });        




        $pixels = $orderlink->getPixelAll($request); 

        $googles = $orderlink->getGoogleAll($request);

        $domain1 = Userdomain::where('author_id',$request->user()->id)->where('status','running')->get();

        $domain2 = Userdomain::where('author_id',2)->get();

        $domains = $domain1->merge($domain2);   

        // return $pixels;

        return view('v2.orderlink.list', compact('all','domains','pixels','googles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $pixel = Pixel::firstOrNew(array('pixel_id' => $request->pixel, 'author_id' => $request->user()->id));

        $pixel->save();


        $google = Googleid::firstOrNew(array('google_id' => $request->google, 'author_id' => $request->user()->id));

        $google->save();

        $isisms = rawurlencode($request->isisms);

        $newlink = Orderlink::create(['tipelink'=>$request->tipelink,
                               'author_id'=>$request->user()->id,
                               'nomor'=> $request->nomor,
                               'isisms'=> $isisms,
                               'alias'=>$request->alias,
                               'desktop_url'=>$request->desktop_url,
                               'pixel_event'=>$request->pixel_event,
                               'hit'=>0,
                               'pixel_id'=>$pixel->id,
                               'google_id'=>$google->id]);
        


        return $newlink;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function unikHitCounter($id)
    {
        return DB::table('orderlink_stats')->where('orderlink_id',$id)->distinct('fingerprint')->count('fingerprint');
    }

    public function hitCounter($id)
    {
        return Orderlink::find($id)->stat()->count();    
    }
}

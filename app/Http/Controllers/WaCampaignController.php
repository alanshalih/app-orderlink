<?php

namespace App\Http\Controllers;

use App\Mail\WaCampaignMail;
use App\Mail\TesEmail;
use App\Wacampaign;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Jenssegers\Agent\Agent;
use Carbon\Carbon;

class WaCampaignController extends Controller
{
    //
    public function klik($nomor){

      $agent = new Agent();

      $wa = Wacampaign::where('phone',$nomor)->first();

     if($wa){
        $clickcounter = Redis::incr('wa.'.$nomor.'counter');

        if($agent->isDesktop()){
            return view('orderlink.wadesktop',compact('wa','clickcounter'));
        }else{
            return view('orderlink.wav3',compact('wa','clickcounter'));
        }
     }else{
        return redirect('http://wa.orderlink.in');
     }

        
    }

    public function stat($nomor, Request $request){

      $stat =  Redis::get('wa.'.$nomor.'counter');

      if(isset($request->token)){

        $wa = Wacampaign::where('phone',$nomor)->
        where('token',$request->token)->first();

      

        if(count($wa)>0){
          $wa->emailverified = true;

          $wa->save();

          return view('orderlink.wastat',compact('wa','stat'));
        }
        
    }
    
        return redirect('http://wa.orderlink.in');
    

      

    

    }
    public function signup(Request $request){
    	$data = Wacampaign::firstOrNew(array('phone' => $request->phone));

      if(($request->email)){


             if($request->cookie('waref') && !$data->email){
                
                $upline = Wacampaign::where('id',$request->cookie('waref'))->first();

                $upline->limit_klik += 100;

                $upline->save();

                $data->upline = $upline->id;

             }

            
        }


        $data->fill($request->all());


        if(($request->email)){
        //     // if(!$data->emailverified)
             Mail::to($request->email)->send(new WaCampaignMail($data,$request->token, $request->name)); 
            }

        if(($request->message)){

        	$data->message = rawurlencode($request->message);

        }

        

          $data->save();

        return $data;
    }

    public function referal($id)
    {
      return redirect ('http://wa.orderlink.in')->cookie('waref', $id, 31104000);
    }

    public function test(Request $request)
    {
      Mail::to($request->email)->send(new TesEmail()); 
    }

    public function list(){
      // $today = Carbon::today();
      // $date =   $today->subDays(2);  
      // $list = Wacampaign::where('created_at','>',$date)->get();
      $list = Wacampaign::get();
      return $list;
    }
}

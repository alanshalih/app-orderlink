<?php

namespace App\Http\Controllers;

use App\Googleid;
use App\Pixel;
use App\User_list;
use Illuminate\Http\Request;

class AssetController extends Controller
{
    //
      public function pixel(Request $request)
    {
    	$pixel = Pixel::where('author_id',$request->user()->id)->get();

    	$google = Googleid::where('author_id',$request->user()->id)->get();
    	
    	return view('asset/pixel', compact('pixel','google'));
    }

    public function createPixel(Request $request)
    {
    	$pixel = Pixel::create(['pixel_id' => $request->pixel,'author_id'=>$request->User()->id]);

    	return $pixel;
    }

    public function updatePixel($id, Request $request)
    {
    	$pixel = Pixel::find($id)->update($request->all());

    	return $pixel;
    }

    public function deletePixel($id)
    {

    	$pixel = Pixel::destroy($id);

    	return $pixel;
    }

    public function createPixelGoogle(Request $request)
    {
    	$pixel = Googleid::create(['google_id' => $request->pixel,'author_id'=>$request->User()->id]);

    	return $pixel;
    }

    public function updatePixelGoogle($id, Request $request)
    {
    	$pixel = Googleid::find($id)->update(['google_id' => $request->pixel_id,'name' => $request->name]);

    }

    public function deletePixelGoogle($id)
    {

    	$pixel = Googleid::destroy($id);

    	return $pixel;
    }

    public function list(Request $request){

        $lists = User_list::where('author_id', $request->user()->id)->get();

    	return view('asset/list',compact('lists'));

    }
}

<?php

namespace App\Http\Controllers;

use App\Notifications\UserNotifier;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class NotificationController extends Controller
{
    //
    public function sendNotification(Request $request){

    		$avatar =  $request->user()->avatar ?? 'https://www.gravatar.com/avatar/'.md5($request->user()->email);

    		Notification::send(User::all(), new UserNotifier($request->text,$request->link,$avatar));

    		return redirect('/admin/compose/notif')->with('status', 'Notif dikirim!');


    }

    public function composeNotif(){
    	return view('admin.sendNotif');
    }
}

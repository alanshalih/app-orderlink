<?php

namespace App\Http\Controllers;


use App\C_template;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ContentLinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //


        $userCL = C_template::where('author_id',$request->user()->id)
                ->latest()->get();


        return view('contentlink/mylist', compact('userCL'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        return view('contentlink.template.'.$request->t.'.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //



       $userCL = C_template::where('id',$request->id)
                ->first();

        if($request->cover){
            $userCL->cover = $request->cover;

            $userCL->save();


            return $userCL;
        }


       $data = (json_decode($request->data));

       $modalId = $data->id;

       $userCL->update($request->all());

       $userCL->title = $data->title;

       $userCL->modalId = $modalId;

       

       $userCL->modalClass = $request->modalClass;

       $userCL->data_backdrop = $data->data_backdrop;

       $userCL->save();

       

       return $userCL;
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return C_template::where('id',$id)
                ->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        $contentlink = C_template::where('id',$id)
                ->first();



        return view('contentlink.template.'.$contentlink->modalId.'.edit', compact('contentlink'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        C_template::destroy($id);

        return redirect('/contentlink')->with('status', 'Contentlink Template anda telah dihapus');
    }

    public function save(Request $request)
    {


        return C_template::create([
            'author_id'=>$request->user()->id,
            'modalId'=>$request->modalId,
            'cover'=>$request->cover,
            ]);
    }

    public function save2(Request $request)
    {



       $userCL = C_template::where('id',$request->id)
                ->first();



       $data = (json_decode($request->data));

       $modalId = $data->id;

       $userCL->update($request->all());

       $userCL->title = $data->title;

       $userCL->modalId = $modalId;

       

       $userCL->modalClass = $request->modalClass;

       $userCL->data_backdrop = $data->data_backdrop;

       $userCL->save();

       

       return $userCL;
    }

     public function save3(Request $request)
    {
        $img = str_replace('data:image/jpeg;base64,', '', $request->cover);
$img = str_replace(' ', '+', $img);
$data = base64_decode($img);

        Storage::put('public/contentlink/c_template'.$request->id.'.jpg', $data);

        $userCL = C_template::where('id',$request->id)
                ->first();

        $userCL->cover = '/storage/contentlink/c_template'.$request->id.'.jpg';

        $userCL->save();
    }







    public function template()
    {
        return view('contentlink.template.choosetemplate');
    }

}

<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('facebook')->fields([
            'first_name','email'
        ])->redirect();   
    }   

    public function callback(Request $request)
    {
    	
        // when facebook call us a with token   
        $providerUser = Socialite::driver('facebook')->fields([
            'first_name','email'
        ])->user(); 

        if(!($providerUser->email)){
        	return redirect('/register')->with('status', 'Maaf, Facebook anda tidak memiliki email!');
        }


         $user = User::where('email',$providerUser->email)->first();

                

       if(!($user)){

       		$upline = User::where('id',$request->cookie('olinkref'))->first();

   

       		
	         if($upline){

             $user = User::Create([
              'name' =>$providerUser->user['first_name'],
              'email' =>$providerUser->user['email'],
              'upline' => $upline->id,
              'token' => rand ( 1001 , 9900000 ),
              'avatar' => $providerUser->avatar
              ]);


          }else{

                $user = User::Create([
              'name' =>$providerUser->user['first_name'],
              'email' =>$providerUser->user['email'],
              'token' => rand ( 1001 , 9900000 ),
              'avatar' => $providerUser->avatar
              ]);

          }
                  Redis::set('user.'.$user->id.'.Limiter', 100);

       }else{
       		if(!($user->avatar))
		       {
		       		$user->avatar = $providerUser->avatar;

		       		$user->save();
		       }
       }

       

        Auth::login($user);

    	return redirect('/home')->with('status', 'Selamat Datang di Orderlink!');
    }
}

<?php

namespace App\Http\Controllers;

use App\Mail\WebinarConfirmation;
use App\Webinar_list;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class WebinarController extends Controller
{
    //
    public function Submit(Request $request){
    	
    	$w = Webinar_list::firstorcreate(['email'=>$request->email,
    						  'firstname'=>$request->firstname,
    						  'upline'=>$request->cookie('olinkref')]);

        Mail::to($request->email)->send(new WebinarConfirmation($request->firstname));    	

    	return redirect('http://orderlink.in/wbnplc-2-facebook-ad-insights-daeng-faqih/');

    }

    public function getAffiliate()
    {


       return DB::table('webinar_lists')
                 ->select('upline', DB::raw('count(*) as downline'))
                 ->groupBy('upline')
                 ->pluck('downline','upline');

       
    }

    public function getPeserta()
    {


        $webinarlists = Webinar_list::all();

        return view('webinar',compact('webinarlists'));

       
    }
}

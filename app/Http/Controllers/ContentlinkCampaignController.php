<?php

namespace App\Http\Controllers;

use App\C_campaign;
use App\C_campaign_stat;
use App\Googleid;
use App\Pixel;
use App\User_list;
use App\Userdomain;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class ContentlinkCampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //



        $mycontentlink = DB::table('c_templates')->where('author_id',$request->user()->id)->select('id','title')->get();
        
        $campaign = (C_campaign::where('author_id',$request->user()->id));



        if($campaign->count() == 0)
        {
            $pixel = $this->getPixelAll($request); 

            $google = $this->getGoogleAll($request);   


            $domain1 = Userdomain::where('author_id',$request->user()->id)->where('status','running')->get();

            $domain2 = Userdomain::where('author_id',2)->get();

            $domains = $domain1->merge($domain2);             


            return view('contentlink.campaign.noquery',compact('pixel','google','mycontentlink','domains'));
        }
        else
            return redirect('/contentlink/campaign/'.$campaign->latest()->first()->id);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        if($request->listContent){
              $listContent = explode(',', $request->listContent);
        $listEvent = explode(',', $request->listEvent);
        $listTime = explode(',', $request->listTime);
        $listRedirectTo = explode(',', $request->listRedirectTo);
        $listMaksShow = explode(',', $request->listMaksShow);
        $listShowOn = explode(',', $request->listShowOn);
        }
      

        $pixel_id = null;
        $google_id = null;

        if($request->pixels){
            $pixel_id = Pixel::create(['pixel_id' => $request->pixels,'author_id'=>$request->User()->id])->id;
        }else{
            if($request->pixel_id){
                $pixel_id = $request->pixel_id;
            }
        }

        if($request->googles){
            $google_id = Googleid::create(['google_id' => $request->googles,'author_id'=>$request->User()->id])->id;
        }else{
           if($request->google_id){
            $google_id = $request->google_id;
            }
        }



    $campaign =  C_campaign::create(['title'=>$request->title,
     'author_id'=>$request->user()->id,
     'url'=> $request->url,
     'alias'=>$request->alias,
     'hit'=>0,
     'pixel_event'=>$request->pixel_event,
     'pixel_id'=>$pixel_id,
     'share_button'=>$request->share_button,
     'stuffing_url'=>$request->stuffing_url,
     'google_id'=>$google_id]);

    if(isset($listContent)){
            
            foreach($listContent as $key=>$value) {
            // do stuff
                $campaign->templates()->attach($value, ['event' => $listEvent[$key],
                    'time' => $listTime[$key],
                    'showOn' => $listShowOn[$key],
                    'maksShow' => $listMaksShow[$key],
                    'redirectTo' => $listRedirectTo[$key]]);
            }
    }


   //  if($request->user()->bitly_access_token){
   //     $client = new Client(['base_uri' => 'https://api-ssl.bitly.com']);
       
   //     $response = $client->request('GET', '/v3/user/link_save?access_token='.$request->user()->bitly_access_token.'&longUrl='.$request->alias);


   //     $data = $response->getBody();

   //     $data = json_decode($data);

   //     $campaign->bitly = $data->data->link_save->link;

   //     $campaign->save();
   // }

   return redirect('/contentlink/campaign')->with('status','Selamat! Contentlink Campaign anda sudah dibuat!');

}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $req)
    {
        //



        // dd(Pixelink_stat::latest()->first()->created_at > Carbon::now()->subDay());

        


        $unikHit = $this->getUnikHit($id,'today');

        $totalHit = $this->getHitStat($id, 'today');


        $browserChart = $this->getChart($id, 'browser');

        $platformChart = $this->getChart($id, 'platform');

        


        $grabLink = $this->fetchAll($req)->paginate(10);

        if(count($grabLink)>0){
           $getOneLink = $this->fetchAll($req)->where('id',$id)->with('contentlinkProps')->first();

            $pixel = $this->getPixelAll($req); 

            $google = $this->getGoogleAll($req); 

            $mycontentlink = DB::table('c_templates')->where('author_id',$req->user()->id)->select('id','title')->get();

            Redis::sadd('user.'.$req->user()->id.'.progress','contentlinkShow');

            $domain1 = Userdomain::where('author_id',$req->user()->id)->where('status','running')->get();

            $domain2 = Userdomain::where('author_id',2)->get();

            $domains = $domain1->merge($domain2);     

    

            return view('contentlink.campaign.show',compact('grabLink','getOneLink','pixel','google','totalHit','unikHit','mycontentlink','scope','domains','browserChart','platformChart'));
        }else{
            return redirect('/contentlink/campaign');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         if($request->listContent){
              $listContent = explode(',', $request->listContent);
        $listEvent = explode(',', $request->listEvent);
        $listTime = explode(',', $request->listTime);
        $listRedirectTo = explode(',', $request->listRedirectTo);
        $listMaksShow = explode(',', $request->listMaksShow);
        $listShowOn = explode(',', $request->listShowOn);
        }


        $pixel_id = null;
        
        $google_id = null;

        if($request->pixels){
            $pixel_id = Pixel::create(['pixel_id' => $request->pixels,'author_id'=>$request->User()->id])->id;
        }else{
            if($request->pixel_id){
                $pixel_id = $request->pixel_id;
            }
        }

        if($request->googles){
            $google_id = Googleid::create(['google_id' => $request->googles,'author_id'=>$request->User()->id])->id;
        }else{
           if($request->google_id){
            $google_id = $request->google_id;
            }
        }

        $campaign =  C_campaign::where('id',$id)->first();

        Redis::del($campaign->alias);

        $campaign->update(['title'=>$request->title,
     'alias'=>$request->alias,
     'pixel_event'=>$request->pixel_event,
     'pixel_id'=>$pixel_id,
     'share_button'=>$request->share_button,
     'stuffing_url'=>$request->stuffing_url,
     'google_id'=>$google_id]);

        $campaign->templates()->sync([]);

        if(isset($listContent)){

            foreach($listContent as $key=>$value) {
            // do stuff
    
                $campaign->templates()->attach($value, ['event' => $listEvent[$key],
                    'time' => $listTime[$key],
                    'showOn' => $listShowOn[$key],
                    'maksShow' => $listMaksShow[$key],
                    'redirectTo' => $listRedirectTo[$key]]);
            }
        }

        return redirect('/contentlink/campaign/'.$id)->with('status','Contentlink Campaign anda sudah diedit!');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

      $p = C_campaign::find($id);

      Redis::ZREM('contentlink',$id);

      Redis::DEL($p->alias);

      $p->delete();

      return redirect('/contentlink/campaign')->with('status', 'Link telah dihapus');
  }

  public function track(Request $request){

    $result = null;

    if($request->pakaiAutoresponder == 'ya')
    {
        $client = new Client(); //GuzzleHttp\Client
        $result = $client->post($request->actionurl, [
            'form_params' => $request->all()
            ]);
    }else{
        User_list::create(['email'=>$request->email, 'template_id'=>$request->template_id, 'website'=>$request->header('referer'), 'author_id'=>$request->author_id]);
    }

    if($request->afterSubmit == 'tetap')
    {
        return redirect($request->header('referer'))->with('status', $request->afterSubmitProps);
    }
    else if($request->afterSubmit == 'redirect')
    {
        return redirect($request->afterSubmitProps);
    }else{
        return $result;
    }


}

public function fetchAll(Request $req)
{
    $all = C_campaign::where('author_id',$req->User()->id)->latest();

    return $all;    
}



public function getPixelAll(Request $req)
{
    $all = Pixel::where('author_id',$req->User()->id)->latest()->get();

    return $all;    
}

public function getGoogleAll(Request $req)
{
    $all = Googleid::where('author_id',$req->User()->id)->latest()->get();

    return $all;    
}



public function refreshCache(Request $request)
{
        //
    Redis::del($request->data);
}

public function storeApi(Request $request){


    $pixel_id = Pixel::firstOrCreate(['pixel_id'=>$request->pixel_id,'author_id'=>$request->user()->id])->id;

    $google_id = Googleid::firstOrCreate(['google_id'=>$request->google_id,'author_id'=>$request->user()->id])->id;

    $campaign =  C_campaign::create(['title'=>$request->title,
     'author_id'=>$request->user()->id,
     'url'=> $request->url,
     'alias'=>$request->alias,
     'hit'=>0,
     'pixel_id'=>$pixel_id,
     'pixel_event'=>$request->pixel_event,
     'google_id'=>$google_id,
     'share_button'=>$request->shareButton,
     'stuffing_url'=>$request->cookieStuffing]);

    if($request->mycontentlink){
        foreach($request->mycontentlink as $key=>$value) {
            // do stuff
        $campaign->templates()->attach($value, ['event' => $request->event[$key],
            'time' => $request->time[$key],
            'showOn' => $request->showOn[$key],
            'maksShow' => $request->maksShow[$key],
            'redirectTo' => $request->redirectTo[$key]]);
        }
    }

    if($request->user()->bitly_access_token){
       $client = new Client(['base_uri' => 'https://api-ssl.bitly.com']);
       
       $response = $client->request('GET', '/v3/user/link_save?access_token='.$request->user()->bitly_access_token.'&longUrl='.$request->alias);

       $data = $response->getBody();

       $data = json_decode($data);

       $campaign->bitly = $data->data->link_save->link;

       $campaign->save();

       return $campaign->bitly;
   }else{
        return 'success!';
   }

   


}

public function trackLink(Request $request){
    return redirect ($request->to);
}

  public function getHitStat($id,$scope)
    {
        $format = '%H:00';

        $date = Carbon::today();



        if($scope == "lastweek"){

            $format = '%d/%m';

            $date = Carbon::today()->subWeek();

        }else if($scope== "lastmonth"){

            $format = '%d/%m';

            $date = Carbon::today()->subMonth();

        }else if($scope == "lastyear"){

             $format = '%b';

            $date = Carbon::today()->subYear();

        }

        $hit = C_campaign_stat::selectRaw("DATE_FORMAT(created_at,'".$format."') as week, count(id) as hit")
        ->where('campaign_id', $id)
        ->where('created_at','>',$date)
        ->groupBy('week')
        ->pluck('hit','week');

        return ($hit);
    }



    public function getUnikHit($id,$scope)
    {

        $format = '%H:00';

        $date = Carbon::today();

        if($scope == "lastweek"){

            $format = '%d/%m';

            $date = Carbon::today()->subWeek();

        }else if($scope== "lastmonth"){
                  $format = '%d/%m';

            $date = Carbon::today()->subMonth();

        }else if($scope == "lastyear"){
                    $format = '%b';

            $date = Carbon::today()->subYear();
            
        }

        $hit = C_campaign_stat::selectRaw("DATE_FORMAT(created_at,'".$format."') as week, count(DISTINCT(fingerprint)) as hit")
        ->where('campaign_id', $id)
        ->where('created_at','>',$date)
        ->groupBy('week')
        ->pluck('hit','week');

        return ($hit);
    }



    public function KlikStat($id, Request $request)
    {

        // return $request->all();
        $a = $this->getHitStat($id, $request->scope);

        $b =  $this->getUnikHit($id, $request->scope);

        return array('hit' => $a, 'unik' => $b);
    }


    public function getChart($id, $attr)
    {


       return DB::table('c_campaign_stats')
                 ->select($attr, DB::raw('count(*) as total'))
                 ->where('campaign_id', $id)
                 ->groupBy($attr)
                 ->pluck('total',$attr);

       
    }




}

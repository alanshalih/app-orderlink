<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class VerifyUserController extends Controller
{
    //
    public function VerifikasiAkun(String $token)
    {
        $getUser = User::where('token',$token)->first();

        if($getUser)
        {

            $getUser->token = null;

            $getUser->isVerified = true;

            $getUser->save();

            return redirect ('/home')
            ->with('status', 'Terima Kasih Sudah Verifikasi Akun Anda');

        }else
        {
            return redirect ('/404');
        }


    }

}

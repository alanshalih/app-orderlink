<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AffiliateController extends Controller
{
    //
    public function index(Request $request)
    {
    	$links = Affiliate_link::where('author_id',$request->user()->id)->get();

		$downline = User::where('upline',$request->user()->id)->get();

		$webinarlists = Webinar_list::where('upline',$request->user()->id)->get();

		return view('affiliate', compact('links','downline','webinarlists'));
    }

    public function delete_link($id){

		$p = Affiliate_link::find($id);

        Redis::ZREM('affiliate_link',$id);

        Redis::DEL($p->alias);

        $p->delete();

        return redirect('/affiliate')->with('status', 'Link telah dihapus');
    }

    public function create_link(Request $request){

    	$link = 'http://app.orderlink.in/a/'.$request->user()->id.dechex(time());
		
		$aff_link = Affiliate_link::create(['author_id'=>$request->user()->id,
								'url'=> $request->link,
								'alias'=>$link]);
		Redis::set($link, $aff_link);

		return redirect('/affiliate')->with('status', 'Link telah dibikin');
	}
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Notifications\WelcomeNotifier;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Validator;
use GuzzleHttp\Client;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    public function showRegistrationForm(Request $request)
    {
        $upline = User::where('id',$request->cookie('olinkref'))->first();


        return view('auth.register', compact('upline'));
    }

    public function RegisterFormv2(Request $request){

        // dd($$request->header('referer'));

        $status = '';

        // return $request->header('referer');

        if($request->header('referer') == 'https://orderlink.in/wp-admin/admin.php?page=wuoy-member-access-product&product=940&read=gold-12'){
            $status = 'gold-12';
        }else if ($request->header('referer') == 'https://orderlink.in/wp-admin/admin.php?page=wuoy-member-access-product&product=939&read=platinum-12'){
            $status = 'platinum-12';
        }else{
            return redirect('https://orderlink.in');
        }

        // return $status;


        return view('auth.RegisterFormv2', compact('request','status'));
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {


        $this->validator($request->all())->validate();

        event(new Registered($user = User::create([
            'name' =>$request->name,
            'email' =>$request->email,
            'password' => bcrypt($request->password),
            'upline' => $request->upline,
            'fingerprint' => $request->fingerprint(),
            'token' => rand ( 1001 , 9900000 )
            ])));

        

        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath())->with('status', 'Selamat Datang di Orderlink!');
    }

    public function registerv2(Request $request)
    {


        $this->validator($request->all())->validate();

        event(new Registered($user = User::create([
            'name' =>$request->name,
            'email' =>$request->email,
            'password' => bcrypt($request->password),
            'upline' => $request->upline,
            'isVerified' => true,
            'fingerprint' => $request->fingerprint(),
            'token' => rand ( 1001 , 9900000 )
            ])));



        

        $this->guard()->login($user);

               if($request->status == 'gold-12')
            return view('goldthankyou',compact('user'));
        else if($request->status == 'platinum-12')
                return view('platinumthankyou',compact('user'));
    }


    protected function registered(Request $request, $user)
    {

        Redis::set('user.'.$user->id.'.Limiter', 100);

        // $user->notify(new WelcomeNotifier($user->name, $user->token));

        // $client = new Client(['base_uri' => 'https://api.elasticemail.com/v2']);

        // $result = $client->post('/contact/quickadd', [
        //      'form_params' => [
        //                         'apikey' => 'b8d566e6-41bd-413d-8646-049b1cac94f6',
        //                         'emails' => $user->email,
        //                         'firstName' => $user->name,
        //                         'publicListID' => 'a8caf7d1-15e4-4fb4-a14e-dd12f8651ba0'
        //                     ]
        //     ]);
    }


   
    
}

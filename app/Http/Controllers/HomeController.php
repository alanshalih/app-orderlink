<?php

namespace App\Http\Controllers;

use App\C_campaign;
use App\Notifications\WelcomeNotifier;
use App\Orderlink;
use App\Pixelink;
use App\Userdomain;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {

        if($req->user()->isVerified)
        {


         $klikToday =  $this->hitungKlik($req,Carbon::today());


         $klikBulanini =  Redis::get('user.'.$req->user()->id.'.monthlyCounter');

         Redis::sadd('user.'.$req->user()->id.'.progress','homeVerified');


         $pixelinkStat = $this->PixelinkStat($req, '%d/%m', Carbon::today()->subWeek());
         $orderlinkStat = $this->OrderlinkStat($req, '%d/%m', Carbon::today()->subWeek());

         // return $orderlinkStat;
         $contentlinkStat = $this->ContentlinkStat($req, '%d/%m', Carbon::today()->subWeek());

         $totalLink = $this->totalLink($req);

         $dompet =  Redis::get('user.'.$req->user()->id.'.dompet');



         return view('home',compact('klikToday','klikBulanini','pixelinkStat','orderlinkStat', 'contentlinkStat', 'totalLink','dompet'));
     }
     else
     {

        Redis::sadd('user.'.$req->user()->id.'.progress','homeNotVerified');

        return view('homeNotVerified');

    }


}

public function hitungKlik(Request $req, $scope = null){



    return  DB::table('pixelinks')
    ->join('pixelink_stats', 'pixelinks.id', '=', 'campaign_id')->where('author_id',$req->user()->id)->where('pixelink_stats.created_at','>',$scope)
    ->count()+DB::table('orderlinks')
    ->join('orderlink_stats', 'orderlinks.id', '=', 'orderlink_id')->where('author_id',$req->user()->id)->where('orderlink_stats.created_at','>',$scope)
    ->count()+DB::table('c_campaigns')
    ->join('c_campaign_stats', 'c_campaigns.id', '=', 'campaign_id')->where('author_id',$req->user()->id)->where('c_campaigns.created_at','>',$scope)
    ->count();




}

public function totalLink(Request $req)
{
    return Pixelink::where('author_id',$req->user()->id)->count()
    + Orderlink::where('author_id',$req->user()->id)->count()
    + C_campaign::where('author_id',$req->user()->id)->count();
}

public function PixelinkStat(Request $request, $format,$scope)
{
    $hit = DB::table('pixelinks')
    ->join('pixelink_stats', 'pixelinks.id', '=', 'campaign_id')
    ->where('author_id',$request->user()->id)
    ->selectRaw("DATE_FORMAT(pixelink_stats.created_at,'".$format."') as week, count(pixelink_stats.id) as hit")
    ->where('pixelink_stats.created_at','>',$scope)
    ->groupBy('week')
    ->pluck('hit','week');

    return ($hit);
}

public function OrderlinkStat(Request $request, $format,$scope)
{
    $hit = DB::table('orderlinks')
    ->join('orderlink_stats', 'orderlinks.id', '=', 'orderlink_id')
    ->where('author_id',$request->user()->id)
    ->selectRaw("DATE_FORMAT(orderlink_stats.created_at,'".$format."') as week, count(orderlink_stats.id) as hit")
    ->where('orderlink_stats.created_at','>',$scope)
    ->groupBy('week')
    ->pluck('hit','week');


    return ($hit);
}

public function ContentlinkStat(Request $request, $format,$scope)
{
    $hit = DB::table('c_campaigns')
    ->join('c_campaign_stats', 'c_campaigns.id', '=', 'campaign_id')
    ->where('author_id',$request->user()->id)
    ->selectRaw("DATE_FORMAT(c_campaign_stats.created_at,'".$format."') as week, count(c_campaign_stats.id) as hit")
    ->where('c_campaign_stats.created_at','>',$scope)
    ->groupBy('week')
    ->pluck('hit','week');

    return ($hit);
}

public function resendEmail(Request $req)
{
   $req->user()->notify(new WelcomeNotifier($req->user()->name, $req->user()->token));

   return redirect('/home')->with('status', 'Email sudah dikirim!');
}

public function sendEmail(Request $req)
{
   $req->user()->notify(new WelcomeNotifier($req->user()->name, $req->user()->token));

}

public function markAsRead(Request $request)
{
    $request->user()->unreadNotifications->markAsRead();
}

public function userProfile(Request $request){
    $user = $request->user();

    $domains =  Userdomain::where('author_id', $request->user()->id)->get();

    return view('setting',compact('user', 'domains'));
    
}
public function deleteUserDomain($id){

    $domains =  Userdomain::destroy($id);

    return redirect('/setting');
    
}

public function updateProfile(Request $request){

  
    $user = $request->user();

    $user->update($request->all());

    if($request->password){
        $user->password = bcrypt($request->password);

        $user->save(); 
    }

    

    return redirect('/setting');
    
}
}

<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class EmailController extends Controller
{
    //
    public function getUserToken(Request $request)
    {
    	return $request->user()->select(['elastic_api_key','elastic_email_profile'])->first();
    }

    public function register(Request $request)
    {
    			$client = new Client();


                $response = $client->get('https://api.elasticemail.com/v2/account/addsubaccount', [
                    'form_params' => [
                        'apikey' => 'b8d566e6-41bd-413d-8646-049b1cac94f6',
                        'email' => 'user-email-'.$request->user()->id.'@orderlink.in',
                        'requiresEmailCredits' => true,
                        'password' => 'i5lamoke',
                        'confirmPassword' => 'i5lamoke',

                    ]
                ]);

                 $addcredit = $client->get('https://api.elasticemail.com/v2/account/addsubaccountcredits', [
                    'form_params' => [
                        'apikey' => 'b8d566e6-41bd-413d-8646-049b1cac94f6',
                        'subAccountEmail' => 'user-email-'.$request->user()->id.'@orderlink.in',
                        'credits' =>100,
                        'notes' => 'first credit'
                    ]
                ]);



                

                $body = $response->getBody();

                $data = json_decode($body, true);



                $user = $request->user();

                $user->elastic_api_key = $data['data'];

                $user->save();

                return $data['data'];


    }

    public function updateprofile(Request $request)
    {

        $user = $request->user();

        $user->elastic_email_profile = json_encode($request->data);

        $user->save();

        $client = new Client();




                $response = $client->get('https://api.elasticemail.com/v2/account/updateprofile', [
                    'form_params' => [
                        'apikey' => $user->elastic_api_key,
                        'address1' => $request->address,
                        'countryID' => 101,
                        'firstName' => $request->firstname,
                        'lastName' => $request->lastname,
                        'state' => $request->state,
                        'city' => $request->city,
                        'zip' => $request->zip

                    ]
                ]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Googleid;
use App\Pixel;
use App\Userdomain;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ViralResearchController extends Controller
{
    //

    public function index(Request $request)
    {

        $mycontentlink = DB::table('c_templates')->where('author_id',$request->user()->id)->select('id','title')->get();
      
        $domain1 = Userdomain::where('author_id',$request->user()->id)->where('status','running')->get();

        $domain2 = Userdomain::where('author_id',2)->get();

        $google = Googleid::where('author_id',$request->User()->id)->latest()->get();

        $facebook = Pixel::where('author_id',$request->User()->id)->latest()->get();

        $domains = $domain1->merge($domain2); 

        return view('viral-research.index', compact('domains','mycontentlink','google','facebook'));
    }

    public function checker(Request $request){
        return view('viral-research.viral-checker');
    }

    public function saveLatestFetchPost(Request $request)
    {

    	$posts =  $request->all();


    	$request->session()->put('key', json_encode($posts));


    }

    public function getLatestFetchPost(Request $request)
    {

    	 return  session('key');

    }

    public function savePage(Request $request)
    {

    	$page =  $request->page;

		if($page){
            $request->session()->push('choosen_page', ($page));
        }

		return $page;

    }

    public function getPages(Request $request)
    {
    	$session = (session('choosen_page'));

    	$array_number = count($session);

    	$new_array = array();

    	$limit = ($array_number > 10 ? 10 : $array_number);

    	for ($i = 0; $i < $limit; $i++) {

		    $new_array[$i] = $session[($array_number-($i+1))];
		}
    	 return $new_array;

    }
}

<?php

namespace App\Http\Controllers;

use App\Googleid;
use App\Pixel;
use App\Pixelink;
use App\Pixelink_stat;
use App\Userdomain;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class PixelinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        //



        $grabLink = $this->fetchAll($req)->first();
        if($grabLink)
            return redirect('/pixelink/'.$grabLink->id);
        else
        {
            $pixel = $this->getPixelAll($req); 

            $google = $this->getGoogleAll($req);

            $domain1 = Userdomain::where('author_id',$req->user()->id)->where('status','running')->get();

            $domain2 = Userdomain::where('author_id',2)->get();

            $domains = $domain1->merge($domain2);             

            return view('pixelink.noquery',compact('pixel','google','domains'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //



        $pixel_id = null;
        $google_id = null;

        

        if($request->pixels){
            $pixel_id = Pixel::create(['pixel_id' => $request->pixels,'author_id'=>$request->User()->id])->id;
        }else{
            if($request->pixel_id){
                $pixel_id = $request->pixel_id;
            }
        }

        if($request->googles){
            $google_id = Googleid::create(['google_id' => $request->googles,'author_id'=>$request->User()->id])->id;
        }else{
            if($request->google_id){
                $google_id = $request->google_id;
            }
        }





        $pnew = Pixelink::create(['title'=>$request->title,
         'author_id'=>$request->user()->id,
         'url'=> $request->url,
         'alias'=>$request->alias,
         'hit'=>0,
         'pixel_id'=>$pixel_id,
         'pixel_event'=>$request->pixel_event,
         'google_id'=>$google_id]);



     //    if($request->user()->bitly_access_token){
     //     $client = new Client(['base_uri' => 'https://api-ssl.bitly.com']);

     //    $response = $client->request('GET', '/v3/user/link_save?access_token='.$request->user()->bitly_access_token.'&longUrl='.$request->alias);

     //     $data = $response->getBody();

     //     $data = json_decode($data);

     //     $pnew->bitly = $data->data->link_save->link;

     //     $pnew->save();
     // }


     return redirect('/pixelink/'.$pnew->id)->with('status', 'Selamat! Pixelink anda sudah dibuat');



 }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $req, $id)
    {
        //

        // $a =  $this->getChart($id,'platform');

        // $b =  $this->getChart($id,'browser');

        // $c = array('a' => $a, 'b' => $b);

        // dd($c);
        // $keyed = $a->mapWithKeys(function ($item) {
        //     return [$item['email'] => $item['name']];
        // });

        // return $keyed->all();

        Redis::sadd('user.'.$req->user()->id.'.progress','pixelinkShow');
        // dd(Pixelink_stat::latest()->first()->created_at > Carbon::now()->subDay());




        $unikHit = $this->getUnikHit($id,'today');

        $totalHit = $this->getHitStat($id, 'today');

        

        $browserChart = $this->getChart($id, 'browser');

        $platformChart = $this->getChart($id, 'platform');


        


        $grabLink = $this->fetchAll($req)->paginate(10);

        if(count($grabLink)>0){
            $getOneLink = $this->fetchAll($req)->where('id',$id)->first();

            $pixel = $this->getPixelAll($req); 

            $google = $this->getGoogleAll($req);

            $domain1 = Userdomain::where('author_id',$req->user()->id)->where('status','running')->get();

            $domain2 = Userdomain::where('author_id',2)->get();

            $domain3 = Userdomain::where('author_id',3)->get();

            $domains = $domain1->merge($domain2);

            $cloakdomain = $domain1->merge($domain3); 


            return view('pixelink/show',compact('grabLink','getOneLink','pixel','google','totalHit','unikHit', 'scope','domains', 'browserChart', 'platformChart','cloakdomain'));
        }else{
            return redirect('errors.404');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    
        $pixel_id = null;
        $google_id = null;

        

        if($request->pixels){
            $pixel_id = Pixel::create(['pixel_id' => $request->pixels,'author_id'=>$request->User()->id])->id;
        }else{
            if($request->pixel_id){
                $pixel_id = $request->pixel_id;
            }
        }

        if($request->googles){
            $google_id = Googleid::create(['google_id' => $request->googles,'author_id'=>$request->User()->id])->id;
        }else{
            if($request->google_id){
                $google_id = $request->google_id;
            }
        }

        $pixelink = Pixelink::where('id',$id)->first();

        
        Redis::del($pixelink->alias);

        $pixelink->update(['title'=>$request->title,
         'alias'=>$request->alias,
         'pixel_id'=>$pixel_id,
          'pixel_event'=>$request->pixel_event,
         'google_id'=>$google_id]);

        

        return redirect('/pixelink/'.$id)->with('status', 'Pixelink anda sudah diedit');



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $p = Pixelink::find($id);

        Redis::ZREM('pixelink',$id);

        Redis::DEL($p->alias);

        $p->delete();

        return redirect('/pixelink')->with('status', 'Link telah dihapus');
    }

     /**
     * Mengambil seluruh data dalam database yang sesuai dengan user saat ini
     *
     * @param  Request  $req
     * @return \Illuminate\Http\Response
     */
     public function fetchAll(Request $req)
     {
        $all = Pixelink::where('author_id',$req->User()->id)->latest();

        return $all;    
    }

    public function getPixelAll(Request $req)
    {
        $all = Pixel::where('author_id',$req->User()->id)->latest()->get();

        return $all;    
    }

    public function getGoogleAll(Request $req)
    {
        $all = Googleid::where('author_id',$req->User()->id)->latest()->get();

        return $all;    
    }

    public function getHitStat($id,$scope)
    {
        $format = '%H:00';

        $date = Carbon::today();



        if($scope == "lastweek"){

            $format = '%d/%m';

            $date = Carbon::today()->subWeek();

        }else if($scope== "lastmonth"){

            $format = '%d/%m';

            $date = Carbon::today()->subMonth();

        }else if($scope == "lastyear"){

             $format = '%b';

            $date = Carbon::today()->subYear();

        }

        $hit = Pixelink_stat::selectRaw("DATE_FORMAT(created_at,'".$format."') as week, count(id) as hit")
        ->where('campaign_id', $id)
        ->where('created_at','>',$date)
        ->groupBy('week')
        ->pluck('hit','week');

        return ($hit);
    }



    public function getUnikHit($id,$scope)
    {

        $format = '%H:00';

        $date = Carbon::today();

        if($scope == "lastweek"){

            $format = '%d/%m';

            $date = Carbon::today()->subWeek();

        }else if($scope== "lastmonth"){
                  $format = '%d/%m';

            $date = Carbon::today()->subMonth();

        }else if($scope == "lastyear"){
                    $format = '%b';

            $date = Carbon::today()->subYear();
            
        }

        $hit = Pixelink_stat::selectRaw("DATE_FORMAT(created_at,'".$format."') as week, count(DISTINCT(fingerprint)) as hit")
        ->where('campaign_id', $id)
        ->where('created_at','>',$date)
        ->groupBy('week')
        ->pluck('hit','week');

        return ($hit);
    }



    public function KlikStat($id, Request $request)
    {

        // return $request->all();
        $a = $this->getHitStat($id, $request->scope);

        $b =  $this->getUnikHit($id, $request->scope);

        return array('hit' => $a, 'unik' => $b);
    }


    public function getChart($id, $attr)
    {


       return DB::table('pixelink_stats')
                 ->select($attr, DB::raw('count(*) as total'))
                 ->where('campaign_id', $id)
                 ->groupBy($attr)
                 ->pluck('total',$attr);

       
    }

    public function updateCustomTextLoader(){
        
    }

    public function updateCloak(Request $request){


        $p = Pixelink::where('id',$request->id)->first();

        if(Redis::exists($p->alias))
        {
            Redis::del($p->alias);
        }
        
        
        if($request->cloak_link){
            $p->cloak_link = $request->cloak_link;
        }

        if($request->alias){
            $p->alias = $request->alias;
        }

        if($request->allowed_country){
            $p->allowed_country = $request->allowed_country;
        }

        if($request->cloak_redirect){
            $p->cloak_redirect = $request->cloak_redirect;
        }

        $p->update($request->all());

        return $p;
        
    }



}

<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class RajaOngkirController extends Controller
{
    //
    public function province(){

    	if(Redis::exists('rajaongkir.provinces')){
    		$response =  Redis::get('rajaongkir.provinces');

    		$data =  json_decode($response);

    		return $data->rajaongkir->results;
    	}else{
    		$client = new Client();
	    	$response = $client->request('GET', 'http://pro.rajaongkir.com/api/province', [
			    'query' => ['key' => 'a8100a0b7bbcf6b2a796e9bf62d606b2']
			]);

			$data = $response->getBody();
			// Redis::set('rajaongkir.provinces',$response);
			Redis::setex('rajaongkir.provinces', 60*60*24*30, $data);

			$jdecode = json_decode($data);

			return $jdecode->rajaongkir->results;
    	}
    	

    	
    }

    public function city(Request $request){

        if($request->province_id){
            if(Redis::exists('rajaongkir.city.'.$request->province_id)){
            $response =  Redis::get('rajaongkir.city.'.$request->province_id);

            $data =  json_decode($response);

            return $data->rajaongkir->results;
            }else{
                $client = new Client();
                $response = $client->request('GET', 'http://pro.rajaongkir.com/api/city', [
                    'query' => ['key' => 'a8100a0b7bbcf6b2a796e9bf62d606b2',
                                'province' => $request->province_id]
                ]);

                $data = $response->getBody();
                // Redis::set('rajaongkir.provinces',$response);
                Redis::setex('rajaongkir.city.'.$request->province_id, 60*60*24*30, $data);

                $jdecode = json_decode($data);

                return $jdecode->rajaongkir->results;
            }

        }else{
            return 'id provinsi belum di set';
        }

        
        

        
    }

    public function subdistrict(Request $request){

        if($request->city_id){
            if(Redis::exists('rajaongkir.subdistrict.'.$request->city_id)){
            $response =  Redis::get('rajaongkir.subdistrict.'.$request->city_id);

            $data =  json_decode($response);

            return $data->rajaongkir->results;
            }else{
                $client = new Client();
                $response = $client->request('GET', 'http://pro.rajaongkir.com/api/subdistrict', [
                    'query' => ['key' => 'a8100a0b7bbcf6b2a796e9bf62d606b2',
                                'city' => $request->city_id]
                ]);

                $data = $response->getBody();
                // Redis::set('rajaongkir.provinces',$response);
                Redis::setex('rajaongkir.subdistrict.'.$request->city_id, 60*60*24*30, $data);

                $jdecode = json_decode($data);

                return $jdecode->rajaongkir->results;
            }

        }else{
            return 'id city belum di set';
        }

        
        

        
    }

    public function cost(Request $request){

      
            if(Redis::exists('rajaongkir.cost.'.$request->origin.'.'.$request->subdistrict_id.'.'.$request->weight)){
            $response =  Redis::get('rajaongkir.cost.'.$request->origin.'.'.$request->subdistrict_id.'.'.$request->weight);

            $data =  json_decode($response);

            return json_encode($data->rajaongkir->results[0]);
            }else{
                $client = new Client();
                // $response = $client->request('POST', 'http://pro.rajaongkir.com/api/cost', [
                //     'query' => ['key' => 'a8100a0b7bbcf6b2a796e9bf62d606b2',
                //                 'origin' => $request->origin,
                //                 'originType' => 'subdistrict',
                //                 'destination' => $request->subdistrict_id,
                //                 'destinationType'=> 'subdistrict',
                //                 'weight' => $request->weight,
                //                 'courier' => $request->courier]
                // ]);

                $response = $client->request('POST', 'http://pro.rajaongkir.com/api/cost', [
                    'form_params' => ['key' => 'a8100a0b7bbcf6b2a796e9bf62d606b2',
                                'origin' => $request->origin,
                                'originType' => 'city',
                                'destination' => $request->subdistrict_id,
                                'destinationType'=> 'subdistrict',
                                'weight' => $request->weight,
                                'courier' => $request->courier]
                ]);

                $data = $response->getBody();
                // Redis::set('rajaongkir.provinces',$response);
                Redis::setex('rajaongkir.cost.'.$request->origin.'.'.$request->subdistrict_id.'.'.$request->weight, 60*60*24*30, $data);

                $jdecode = json_decode($data);

                return json_encode($jdecode->rajaongkir->results[0]);
            }


    }
}

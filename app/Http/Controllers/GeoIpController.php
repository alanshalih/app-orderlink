<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GeoIpController extends Controller
{
    //
    public function test(Request $req){

    	$a = "ID,US";

    	$ip = geoip()->getClientIP();

    	$c = geoip()->getLocation($ip)['iso_code'];

    	if(strpos($a, $c) !== false) {
		    echo 'true';
		}
    }
}

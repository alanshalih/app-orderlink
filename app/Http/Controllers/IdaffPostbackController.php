<?php

namespace App\Http\Controllers;

use App\Idaff_postback;
use App\User;
use Illuminate\Http\Request;

class IdaffPostbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Idaff_postback::count();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $idaff = Idaff_postback::firstOrNew(array('cemail' => $request->cemail));

        $idaff->fill($request->all());

        $idaff->referer = $request->header('referer');

        $idaff->user_agent = $request->header('User-Agent');

        $idaff->save();


        return $idaff;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate(Request $request)
    {
        //
        $idaff = Idaff_postback::where('cemail',$request->user()->email)
        ->where('user_agent',null)->first();

        if($idaff){

            $user = User::where('email',$idaff->cemail)->first();

            if(count($user->roles)==0){

                if($idaff->grand_total < 355000){
                    return view('goldthankyou',compact('user'));
                }else if ($idaff->grand_total < 405000){
                    return view('platinumthankyou',compact('user'));
                }
            }else{
                return redirect('/home');
            }


        }else{
            return redirect('http://orderlink.in');
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

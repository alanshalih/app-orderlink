<?php

namespace App\Http\Controllers;

use App\Events\OrderConfirm;
use App\Events\OrderCreated;
use App\PurchaseProduct;
use App\UserCustomer;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class V3POSController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        return PurchaseProduct::where('store_id', $request->store_id)->with('buyer')->latest()->get();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //
       
        $sell_price = 0;

        $nol = 0;

        $from = -500;

        $to = 500;

        $unique = 0;

        $component_price = json_decode(json_encode($request->component_price));

        if(!Redis::exists('list_unique.'.$request->user()->id.'.'.$component_price->total_price)){
            $arr500 = [];
            for($i = $from; $i<=$to; $i++){
                $nol++;
                $arr500[$nol] = $i;
            }
            Redis::sadd('list_unique.'.$request->user()->id.'.'.$component_price->total_price,$arr500);
        }

        $unique = Redis::SPOP('list_unique.'.$request->user()->id.'.'.$component_price->total_price);

        $sell_price = $component_price->total_price+$unique;

        $component_price->unique = $unique;

         $findcustomer = UserCustomer::firstOrNew(array('phone' => $request->phone,'store_id' => $request->store_id));

        $findcustomer->fill(['firstname' => $request->firstname,
                            'lastname' => $request->lastname,
                            'email' => $request->email,
                            'address' => $request->address,
                            'bbm' => $request->bbm,
                            'line' => $request->line,
                            'province' => json_encode($request->province),
                            'city' => json_encode($request->city),
                            'subdistrict' => json_encode($request->subdistrict)]);

        $findcustomer->save();



        $pp = PurchaseProduct::create(['products' => json_encode($request->products),
                                'buyer_id' => $findcustomer->id,
                                'sell_price' => $sell_price,
                                'component_price' => json_encode($component_price),
                                'courier' => $request->courier,
                                'courier_package' => json_encode($request->courier_package),
                                'pay_with' => $request->pay_with,
                                'store_id' => $request->store_id,
                                'expired' => Carbon::now()->addHours($findcustomer->store->order_expired_in_hours)]);

        event(new OrderCreated($pp));


        Redis::set('pos_sell_price.'.$request->user()->id.'.'.$sell_price,$pp->id);

        
        
        return $pp->with('buyer')->first();

      



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function find(Request $request)
    {

        $mycustomer =  UserCustomer::where('phone',$request->phone)->where('store_id', $request->store_id)->first();


        if($mycustomer){
            return $mycustomer;
        }else{
            return UserCustomer::where('phone',$request->phone)->first();
        }
    }

    public function confirmpage2($id)
    {
        $purchase = PurchaseProduct::where('id', $id)->first();

        if($purchase->status != 'checkout'){
            return redirect('/invoice/'.$id);
        }
        return view('v3.confirm', compact('purchase'));
    }

    public function confirmpage1($store, $id)
    {
        $purchase = PurchaseProduct::where('id', $id)->first();

        if($purchase->status != 'checkout'){
            return redirect('/invoice/'.$id);
        }
        return view('v3.confirm', compact('purchase'));
    }

    public function checkoutconfirm(Request $request, $id)
    {
        $order = PurchaseProduct::where('id',$id)->first();

        $order->update(['status'=>'confirm', 'products'=>$request->products]);


        $buyer = $order->buyer;

        $buyer->address = $request->address;

        $buyer->save();

        event(new OrderConfirm($order));

        return redirect('/invoice/'.$id);
    }

    public function invoice1($store, Request $request, $id)
    {
        
         $purchase = PurchaseProduct::where('id', $id)->first();
        return view('v3.invoice', compact('purchase'));
    }

    public function invoice2(Request $request, $id)
    {
        
         $purchase = PurchaseProduct::where('id', $id)->first();
        return view('v3.invoice', compact('purchase'));
    }

   
}

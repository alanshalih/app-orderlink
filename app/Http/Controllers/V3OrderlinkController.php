<?php

namespace App\Http\Controllers;

use App\Orderlink;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class V3OrderlinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $orderlink = new OrderlinkController();

        $all =  $orderlink->fetchAll($request)->get();

        $all->each(function ($item, $key) {

            $hit = Redis::ZSCORE("orderlink", $item->id);

            $item->isisms = rawurldecode($item->isisms);
            
            $item->hit = ($hit > 0) ? $hit : 0 ;
        
        });  

        return $all;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


        $requestin = json_decode(json_encode($request->orderlink));

        $o =  Orderlink::create([
                    'nomor' => $requestin->nomor,
                    'author_id' => $request->user()->id,
                    'tipelink' => $requestin->tipelink,
                    'api_version' => $requestin->api_version,
                    'isisms' => rawurlencode($requestin->isisms),
                    'pixels' => json_encode($requestin->pixels),
                    'pixel_event' => json_encode($requestin->pixel_event),
                    'alias' => $requestin->alias,
                    'desktop_url' => $requestin->desktop_url,
                    'title' => $requestin->title,
                    'tags' => json_encode($requestin->tags)
                    ]);

        $o->isisms = rawurldecode($o->isisms);

        return $o;


        

        // $requestin = $requestin->merge(['author_id' => Auth::id()]);

        // Orderlink::create($requestin);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $requestin = json_decode(json_encode($request->orderlink));

        $data = Orderlink::where('id',$id);

        $data->update([
                    'nomor' => $requestin->nomor,
                    'author_id' => $request->user()->id,
                    'tipelink' => $requestin->tipelink,
                    'api_version' => $requestin->api_version,
                    'isisms' => rawurlencode($requestin->isisms),
                    'pixels' => json_encode($requestin->pixels),
                    'pixel_event' => json_encode($requestin->pixel_event),
                    'alias' => $requestin->alias,
                    'desktop_url' => $requestin->desktop_url,
                    'title' => $requestin->title,
                    'tags' => json_encode($requestin->tags)
                    ]);



        $getitem = $data->first();

        $getitem->isisms = rawurldecode($getitem->isisms);

        return $getitem;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $p = Orderlink::find($id);

        Redis::ZREM('orderlink',$id);

        Redis::DEL($p->alias);

        $p->delete();

    }
}

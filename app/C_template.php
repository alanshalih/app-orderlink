<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class C_template extends Model
{
    //
        //
    protected $fillable = ['data','template','author_id', 'modalId', 'title', 'cover'];

    public function campaigns()
	{
	    return $this->belongsToMany('App\C_campaign')
	    	->withPivot('event', 'time', 'showOn', 'maksShow', 'redirectTo')
	    	->withTimestamps();
	}

}

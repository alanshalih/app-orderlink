<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WebinarConfirmation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(String $name)
    {
        //
        $this->name = $name;
    }

    protected $name;
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Haloo..'.$this->name.', Konfirmasi Email Ini Ya..')
                    ->view('email.webinarconfirmation')
                    ->with([
                        'name' => $this->name,
                    ]);
    }
}

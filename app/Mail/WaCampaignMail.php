<?php

namespace App\Mail;

use App\Wacampaign;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WaCampaignMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $data;

    public function __construct(Wacampaign $data)
    {
        //
        $this->data = $data;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         return $this->subject($this->data->name.', Ini Detil WA Auto-Order anda')
                    ->view('email.wacamapaign')
                    ->with([
                        'data' => $this->data,
                    ]);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
        protected $fillable = [
        'title', 'price','thumbnail','store_id', 'weight','short_description','long_description','show_in_marketplace', 'province', 'city', 'domain'
    ];

    public function author()
    {
        return $this->belongsTo('App\User');
    }
}

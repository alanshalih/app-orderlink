<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_list extends Model
{
    //
    protected $fillable = ['template_id','email','nama','nohp','website','author_id'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Pixelink extends Model
{
    //

    protected $fillable = ['title','author_id', 'url', 'alias', 'pixel_id','google_id', 'hit','pixel_event'];

    public function pixel()
    {
        return $this->belongsTo('App\Pixel');
    }

        public function google()
    {
        return $this->belongsTo('App\Googleid');
    }
    
    public function stat()
    {
        return $this->hasMany('App\Campaign_stat')->get();
    }

    public function getWithHit()
    {
        return DB::table('campaigns')
            ->join('campaign_stats', 'campaigns.id', '=', 'campaign_stats.campaign_id')
            ->select(DB::raw('count(campaign_stats.id) as hit, campaigns.id'))
            ->groupBy('campaigns.id')
            ->where('author_id',Auth::user()->id)
            ->orderBy('campaigns.created_at','desc')
            ->get();
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Support_inbox extends Model
{
    //
    protected $fillable = [
                    'body_html',
            'body_text',
            'from_email',
            'from_name',
            'subject',
            'to_list'
    ];
}

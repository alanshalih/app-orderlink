<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    //
    protected $fillable = [
        'title', 'avatar', 'domain', 'owner_id','pixels','adwords','banks','maxunique','minunique',
        'need_confirm_email',
'order_confirmed_email',
'order_paid_email',
'payment_reminder_email',
'order_canceled_email',
'need_confirm_sms',
'order_confirmed_sms',
'order_paid_sms',
'payment_reminder_sms',
'order_canceled_sms',
'name_sender',
'email_sender'
    ];

    public function owner()
    {
        return $this->belongsTo('App\User');
    }
}

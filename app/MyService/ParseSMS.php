<?php

namespace App\MyService;

use App\PurchaseProduct;
use Carbon\Carbon;

class ParseSMS
{

	public $order;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(PurchaseProduct $order)
    {
        //
    	$this->order = $order;

    }

    public function content($emailtemplate)
    {

    	$template =  json_decode($this->order->store[$emailtemplate])->content;



    	return $this->parseData($template);

    }

    public function parseData($template){

        $nama_toko = $this->order->store->title;

        $nama_depan = $this->order->buyer->firstname;

        $id_order = $this->order->id;

        $link_halaman_konfirmasi = $this->order->store->domain.'/confirm/order/'.$this->order->id;

        $total_pembayaran = number_format($this->order->sell_price,0, '.', '.');

        $banks = json_decode($this->order->store->banks);

        $daftar_rekening = '';

        for($i = 0; $i < count($banks) ; $i++){
            $daftar_rekening = $daftar_rekening.$banks[$i]->bank_account.'-'.$banks[$i]->bank.';';
        }

        $from = array('{nama_depan}', "{id_order}", '{nama_toko}', '{total_pembayaran}', '{daftar_rekening}','{link_halaman_konfirmasi}');

        $to   = array($nama_depan, $id_order, $nama_toko, $total_pembayaran, $daftar_rekening, $link_halaman_konfirmasi);

        $template = str_replace($from, $to, $template);

        return $template;

    }

    public function smsCounter($template)
    {
        return ceil(strlen($template)/160);

    }
}

<?php

namespace App\MyService;

use App\PurchaseProduct;
use Carbon\Carbon;

class ParseEmail
{

	public $order;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(PurchaseProduct $order)
    {
        //
    	$this->order = $order;

    }

    public function subject($emailtemplate)
    {

    	$template =  json_decode($this->order->store[$emailtemplate])->subject;

    	return $this->parseData($template);

    }

    public function content($emailtemplate)
    {

    	$template =  json_decode($this->order->store[$emailtemplate])->content;

    	$prefix = '<table border="0" cellpadding="0" cellspacing="0" align="center"
     width="560" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
     max-width: 560px;" class="wrapper"><tr><td>';

     return $prefix.$this->parseData($template).'</td></tr></table>';



 }

 public function parseData($template){

     $nama_depan = $this->order->buyer->firstname;

     $id_order = $this->order->id;

     $logo_toko = '<img src="'.$this->order->store->avatar.'" width="100px" />';

     $nama_toko = $this->order->store->title;

     $link_konfirmasi = $this->order->store->domain.'/confirm/order/'.$this->order->id;

     $tombol_halaman_konfirmasi = '<a style="background-color : #1976d2; 
     border: none;
     color: white;
     padding: 15px 32px;
     text-align: center;
     text-decoration: none;
     display: inline-block;
     font-size: 16px;" href="'.$link_konfirmasi.'">Lihat Invoice</a>';

     

     $link_halaman_konfirmasi = '<a href="'.$link_konfirmasi.'">'.$link_konfirmasi.'</a>';

     $alamat = $this->order->address;


     $price = json_decode($this->order->component_price);

     $district = json_decode($this->order->buyer->subdistrict);

     $alamat = $this->order->buyer->address.'<br>Kecamatan '.$district->subdistrict_name.', Kabupaten/Kota '.$district->city.', Provinsi '.$district->province;

     $total_pembayaran = number_format($this->order->sell_price,0, '.', '.');

     $order_expired = Carbon::parse($this->order->expired)->formatLocalized('%d-%m-%Y %H:%M');

     

     $banks = json_decode($this->order->store->banks);


     $list_bank = '';

     for( $i= 0 ; $i < count($banks) ; $i++ ){
        $list_bank= $list_bank.' <tr>
        <th style=" border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;" >'.$banks[$i]->bank.'</th>
        <th style=" border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;">'.$banks[$i]->bank_account.'</th>
        <th style=" border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;">an.'.$banks[$i]->bank_username.'</th>
    </tr>';

     }



     $daftar_rekening='<table style="font-family: arial, sans-serif;
     border-collapse: collapse;
     width: 100%;">

     <tr>
       '.$list_bank.'
    </tr>

</table>';

     $invoice='<table style="font-family: arial, sans-serif;
     border-collapse: collapse;
     width: 100%;">

     <tr>
        <th style=" border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;" colspan="3">Total Harga Produk</th>
        <th style=" border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;">'.number_format($price->base_price,0, '.', '.').'</th>
    </tr>
    <tr>
        <th style=" border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;" colspan="3">Biaya Kirim</th>
        <th style=" border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;">'.number_format($price->courier_cost,0, '.', '.').'</th>
    </tr>
    <tr>
        <th style=" border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;" colspan="3">Diskon</th>
        <th style=" border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;">'.number_format($price->discount,0, '.', '.').'</th>
    </tr>
    <tr>
        <th style=" border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;" colspan="3">Biaya Lain</th>
        <th style=" border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;">'.number_format($price->another_cost,0, '.', '.').'</th>
    </tr>
    <tr>
        <th style=" border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;" colspan="3">Angka Unik</th>
        <th style=" border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;">'.$price->unique.'</th>
    </tr>
    <tr style="background-color: #1a237e; color : white">
        <th style=" border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;" colspan="3">Substotal</th>
        <th style=" border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;">Rp.'.number_format($this->order->sell_price,0, '.', '.').'</th>
    </tr>
</table>';



$from = array('{nama_depan}', "{id_order}", '{logo_toko}', '{tombol_halaman_konfirmasi}','{nama_toko}','{invoice}','{alamat}', '{order_expired}', '{total_pembayaran}', '{daftar_rekening}','{link_halaman_konfirmasi}');

$to   = array($nama_depan, $id_order, $logo_toko, $tombol_halaman_konfirmasi, $nama_toko, $invoice, $alamat, $order_expired, $total_pembayaran, $daftar_rekening, $link_halaman_konfirmasi);

$t = str_replace($from, $to, $template);





return $t;

}
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class C_campaign_stat extends Model
{
    //
        //
    protected $fillable = ['campaign_id','ip','referrer','user_agent','fingerprint','platform','browser', 'source', 'medium', 'term','content', 'campaign'];

    public function scopeThisWeek($query)
    {
    	return $query
    	->where('created_at','>=',Carbon::now()->startOfWeek());
    }
}

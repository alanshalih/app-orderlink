<?php

namespace App\Listeners;

use App\Events\OrderCreated;
use App\MyService\ParseEmail;
use GuzzleHttp\Client;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendOrderCreatedEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderCreated  $event
     * @return void
     */
    public function handle(OrderCreated $event)
    {
        //
        $api_key = $event->order->store->owner->elastic_api_key;
        
        if($api_key){
            if(json_decode($event->order->store[$event->email_type])->issend){
                $client = new Client();

                $email = new ParseEmail($event->order);

                $response = $client->get('https://api.elasticemail.com/v2/email/send', [
                    'form_params' => [
                        'apikey' => $api_key,
                        'bodyHtml' => $email->content($event->email_type),
                        'msgTo' => $event->order->buyer->email,
                        'subject' =>$email->subject($event->email_type),
                        'fromName' => $event->order->store->name_sender,
                        'from' => $event->order->store->email_sender,
                        'isTransactional' => true,
                    ]
                ]);
            }
        }
    }
}

<?php

namespace App\Listeners;

use App\Events\OrderSuccess;
use App\MyService\ParseSMS;
use GuzzleHttp\Client;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Redis;

class SendOrderSuccessSMS implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderSuccess  $event
     * @return void
     */
    public function handle(OrderSuccess $event)
    {
        //
        if(json_decode($event->order->store[$event->sms_type])->issend){
                $client = new Client();

                $sms = new ParseSMS($event->order);

                $content = $sms->content($event->sms_type);

                $smscounter = $sms->smsCounter($content);

                Redis::INCRBY('smscounter.user.'.$event->order->store->owner->id,$smscounter);

                Redis::INCRBY('smscounter.orderlink',$smscounter);

                $response = $client->post('http://smsgateway.me/api/v3/messages/send', [
                    'form_params' => [
                        'number' => $event->order->buyer->phone,
                        'message' => $content,
                        'device' => 46266,
                        'email' => 'maulanashalihin@gmail.com',
                        'password' => 'i5lamoke'



                    ]
                ]);
            }
    }
}

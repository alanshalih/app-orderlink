<?php

namespace App\Listeners;

use App\Events\OrderCreated;
use GuzzleHttp\Client;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AddCustomerToContacts implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderCreated  $event
     * @return void
     */
    public function handle(OrderCreated $event)
    {
        //
         $client = new Client();


          $response = $client->get('https://api.elasticemail.com/v2/contact/update', [
                    'form_params' => [
                        'apikey' => $event->order->store->owner->elastic_api_key,
                        'email' => $event->order->buyer->email,
                        'firstName' => $event->order->buyer->firstname,
                        'lastName' =>  $event->order->buyer->lastname,

                ]
                ]);

                $response = $client->get('https://api.elasticemail.com/v2/list/addcontacts', [
                    'form_params' => [
                        'apikey' => $event->order->store->owner->elastic_api_key,
                        'emails' => $event->order->buyer->email,
                        'listName' => $event->order->store->title

                ]
                ]);
    }
}

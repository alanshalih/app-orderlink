<?php

namespace App\Listeners;

use App\Events\StoreCreated;
use GuzzleHttp\Client;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AddStoreListEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StoreCreated  $event
     * @return void
     */
    public function handle(StoreCreated $event)
    {
        //
         $client = new Client();

        $response = $client->get('https://api.elasticemail.com/v2/list/add', [
                'form_params' => [
                    'apikey' => $event->store->owner->elastic_api_key,
                    'listName' => $event->store->title,
                     'createEmptyList' => true,

            ]
            ]);
    }
}

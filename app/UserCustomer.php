<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCustomer extends Model
{
    //
     protected $fillable = ['firstname', 'lastname','store_id','phone','email','bbm','line','province','city','subdistrict','address'];

     public function store()
    {
        return $this->belongsTo('App\Store');
    }
 
}

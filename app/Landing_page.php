<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Landing_page extends Model
{
    //

        protected $fillable = ['content','author_id','title','url'];
}

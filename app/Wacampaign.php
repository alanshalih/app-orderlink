<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wacampaign extends Model
{
    //
      protected $fillable = ['phone','message','email','name','upline','token'];
}

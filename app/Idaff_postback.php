<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idaff_postback extends Model
{
    //
    protected $fillable = ['invoice','amount','cname','cemail','cmphone','status','ipaddress','grand_total','user_agent','referer'];
}

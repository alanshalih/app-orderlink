<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orderlink_stat extends Model
{
    //
    protected $fillable = ['orderlink_id','ip','referrer','user_agent','fingerprint','platform','browser', 'source', 'medium', 'term','content', 'campaign'];
}

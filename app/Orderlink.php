<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orderlink extends Model
{
    //
    protected $fillable = ['author_id','title','tags', 'tipelink', 'nomor','isisms', 'hit','alias','pixel_id','google_id','desktop_url','pixel_event','api_version','pixels'];

     public function stat()
    {
        return $this->hasMany('App\Orderlink_stat')->get();
    }

        public function pixel()
    {
        return $this->belongsTo('App\Pixel');
    }

       public function google()
    {
        return $this->belongsTo('App\Googleid');
    }


}

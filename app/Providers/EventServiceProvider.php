<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\OrderCreated' => [
            'App\Listeners\SendOrderCreatedEmail',
            'App\Listeners\SendOrderCreatedSMS',
            'App\Listeners\AddCustomerToContacts',

        ],
        'App\Events\StoreCreated' => [
            'App\Listeners\AddStoreListEmail',
        ],
        'App\Events\OrderSuccess' => [
            'App\Listeners\SendOrderSuccessEmail',
            'App\Listeners\SendOrderSuccessSMS',
        ],
        'App\Events\OrderConfirm' => [
            'App\Listeners\SendOrderConfirmEmail',
            'App\Listeners\SendOrderConfirmSMS',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}

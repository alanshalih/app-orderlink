<?php

namespace App\Notifications;

use App\Wacampaign;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class WaCampaignEmail extends Notification
{
    use Queueable;

    protected $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Wacampaign $data)
    {
        //
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
            return (new MailMessage)
                    ->subject($this->data->name.', Ini Detil WhatsApp Auto-Orderlink anda...')
                    ->line('Simpan detil ini baik-baik, anda bisa  menandai email ini sebagai email penting.')
                    ->line('link url anda : '.'http://wa.orderlink.in/no/'.$this->data->phone)
                    ->line('statistik url anda : '.'http://wa.orderlink.in/stat/'.$this->data->phone.'?token='.$this->data->token)
                    ->line('anda bisa mengedit isi text pesan anda dengan mengisi ulang form di http://wa.orderlink.in!');
                    ->line('Terima kasih sudah menggunakan aplikasi kami!');
     }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

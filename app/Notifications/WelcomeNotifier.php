<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class WelcomeNotifier extends Notification
{
    use Queueable;

    protected $token;
    protected $name;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(String $name, String $token)
    {
        //
        $this->token = $token;
        $this->name = $name;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject($this->name.', 1 Langkah Lagi...')
                    ->line('Selamat Datang di Orderlink!')
                    ->line('Lakukan Verifikasi Akun dengan menekan tombol di Bawah ini')
                    ->action('Verifikasi Akun', URL::to('/').'/verifikasi/'.$this->token)
                    ->line('Terima kasih sudah menggunakan aplikasi kami!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        'text' => Auth::user()->name.', Selamat Datang di Orderlink',
        'link' => '#'
        ];
    }
}

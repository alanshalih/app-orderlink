<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Googleid extends Model
{
    //
    protected $fillable = ['google_id','author_id','name'];
}

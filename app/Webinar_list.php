<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Webinar_list extends Model
{
    //
     protected $fillable = ['email','upline','firstname'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class C_campaign extends Model
{
    //
      protected $fillable = ['title','author_id', 'url', 'alias', 'pixel_id','google_id', 'hit','bitly','share_button','stuffing_url','pixel_event'];




	public function templates()
	{
	    return $this->belongsToMany('App\C_template')
	    	->withPivot('event', 'time', 'showOn', 'maksShow', 'redirectTo')
	    	->withTimestamps();
	}

    public function contentlinkProps(){
         return $this->hasMany('App\C_campaign_c_template');
    }

	 public function pixel()
    {
        return $this->belongsTo('App\Pixel');
    }

        public function google()
    {
        return $this->belongsTo('App\Googleid');
    }
    
}

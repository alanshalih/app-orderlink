<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIdaffPostbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('idaff_postbacks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice')->nullable();
            $table->string('amount')->nullable();
            $table->string('cname')->nullable();
            $table->string('cemail')->nullable();
            $table->string('cmphone')->nullable();
            $table->string('status')->nullable();
            $table->string('ipaddress')->nullable();
            $table->string('grand_total')->nullable();
            $table->text('user_agent')->nullable();
            $table->string('referer')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('idaff_postbacks');
    }
}

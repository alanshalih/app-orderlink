u<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderlinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderlinks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('author_id')->unsigned();
            $table->string('tipelink');
            $table->string('nomor');
            $table->string('isisms')->nullable();
            $table->string('alias')->unique();
            $table->integer('pixel_id')->unsigned()->nullable();
            $table->integer('google_id')->unsigned()->nullable();
            $table->integer('hit')->nullable();
            $table->string('desktop_url');
            $table->timestamps();

            $table->foreign('google_id')->references('id')->on('googleids');
            $table->foreign('pixel_id')->references('id')->on('pixels');
            $table->foreign('author_id')->references('id')->on('users') ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderlinks');
    }
}

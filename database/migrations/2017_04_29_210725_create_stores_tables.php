<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
             $table->string('avatar')->nullable();
            $table->string('domain')->unique();
            $table->integer('owner_id')->unsigned();
            $table->string('pixels')->nullable();
            $table->string('adwords')->nullable();
            $table->string('banks')->nullable();
            $table->integer('maxunique')->default(500);
            $table->integer('minunique')->default(0);
            $table->string('need_confirm_sms')->nullable();
            $table->string('order_confirmed_sms')->nullable();
            $table->string('order_paid_sms')->nullable();
            $table->string('payment_reminder_sms')->nullable();
            $table->string('order_canceled_sms')->nullable();
            $table->text('need_confirm_email')->nullable();
            $table->text('order_confirmed_email')->nullable();
            $table->text('order_paid_email')->nullable();
            $table->text('payment_reminder_email')->nullable();
            $table->text('order_canceled_email')->nullable();
            $table->string('email_sender')->nullable();
            $table->string('name_sender')->nullable();
            $table->integer('order_expired_in_hours')->nullable();
            $table->timestamps();

            $table->foreign('owner_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPixelsToOrderlink extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('orderlinks', function (Blueprint $table) {
        $table->string('pixels')->nullable();
        });

        Schema::table('pixelinks', function (Blueprint $table) {
        $table->string('pixels')->nullable();
        });

        Schema::table('c_campaigns', function (Blueprint $table) {
        $table->string('pixels')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('orderlinks', function (Blueprint $table) {
        $table->dropColumn('pixels');
        });

        Schema::table('pixelinks', function (Blueprint $table) {
        $table->dropColumn('pixels');
        });

        Schema::table('c_campaigns', function (Blueprint $table) {
         $table->dropColumn('pixels');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTitleToOrderlinkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::table('orderlinks', function (Blueprint $table) {
            $table->string('title')->nullable();
            $table->string('tags')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('orderlinks', function (Blueprint $table) {

            $table->dropColumn('title');
            $table->dropColumn('tags');

        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDomainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userdomains', function (Blueprint $table) {
            $table->increments('id');
            $table->string('domain');
            $table->integer('author_id')->unsigned();
            $table->string('status')->default('submitted');
            $table->timestamps();

            $table->foreign('author_id')->references('id')->on('users') ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userdomains');
    }
}

<?php

use Illuminate\Database\Seeder;

class GoogleScriptTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('googleids')->insert([
            'author_id' => 1,
            'google_id' => '200022016998409',
        ]);
    }
}

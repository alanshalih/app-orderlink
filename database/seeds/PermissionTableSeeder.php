
<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('permissions')->insert([
            'name' => 'pixelink',
            'display_name' => 'Pixelink Fitur',
        ]);

        DB::table('permissions')->insert([
            'name' => 'contentlink',
            'display_name' => 'ContentLink Fitur',
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 1,
            'role_id' => 1,
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 1,
            'role_id' => 2,
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 1,
            'role_id' => 3,
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 1,
            'role_id' => 4,
        ]);

         DB::table('permission_role')->insert([
            'permission_id' => 2,
            'role_id' => 1,
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 2,
            'role_id' => 2,
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 2,
            'role_id' => 3,
        ]);

        DB::table('permission_role')->insert([
            'permission_id' => 2,
            'role_id' => 4,
        ]);


    }
}

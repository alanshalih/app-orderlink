<?php

use Illuminate\Database\Seeder;

class PixelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('pixels')->insert([
            'author_id' => 1,
            'pixel_id' => '200022016998409',
        ]);
    }
}

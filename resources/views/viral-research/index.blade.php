@extends('layouts.app')
@section('title','Viral Search')
@section('content')

@if(count($mycontentlink) === 0)
<div class="m-t">
	<div class="col-md-8 offset-md-2">
		<div class="alert alert-info">
			<h4>PERHATIAN, BACA INI SEBELUM MENGGUNAKAN VIRAL RESEARCH</h4>
			<p>1. sebelum bisa melakukan pencarian, anda harus membuat minimal 1 buah template Contentlink. anda akan tahu alasannya kemudian...</p>
			<p>2. pencarian Viral Konten berdasarkan postingan dari Fanspage, jadi langkah pertama yang harus dilakukan adalah memasukan nama Fanspage yang mau di-kepoin (dicari tahu).</p>
			<p>3. hasil pencarian akan muncul daftar fanspage yang sesuai dengan keyword anda, klik Fanspage tersebut. maka otomatis akan keluar 100 postingan terakhir dari fanspage tersebut dan disorting berdasarkan jumlah share</p>
			<p>4. anda bisa memperdalam pencarian konten dengan menekan tombol <i class="fa fa-plus"></i></p>
			<p>5. secara default, gambar postingan tidak ditampilkan untuk menghemat kouta anda. tapi anda bisa mengaktifkannya dengan klik tombol checkbox (swipe)</p>
			<p>6. silakan mulai take action dengan bikin template Contentllink dulu klik tombol di bawah ini!</p>
			<div class="text-center">	<a href="/contentlink/template" class="btn info">Bikin Template Contentlink!</a></div>
	</div>
		</div>
	
</div>
@else
<social-viral-research :domains="{{$domains}}" :mycontentlink="{{$mycontentlink}}" :facebook="{{$facebook}}" :google="{{$google}}"></social-viral-research>
@endif
@endsection
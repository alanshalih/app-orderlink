@extends('layouts.app')

@section('content')

<!-- ############ PAGE START-->
<div class="row-col">
  <div class="col-sm-3 col-lg-2 b-r">
    <div class="p-y">
      <div class="nav-active-border left b-primary">
        <ul class="nav nav-sm">
          <li class="nav-item">
            <a class="nav-link block active" href="#" data-toggle="tab" data-target="#tab-1">Profile</a>
          </li>
          @role((['diamond','platinum','admin']))
          <li class="nav-item">
            <a class="nav-link block" href="#" data-toggle="tab" data-target="#tab-2">User Domain</a>
          </li>
          @endrole
          <li class="nav-item">
            <a class="nav-link block" href="#" data-toggle="tab" data-target="#tab-3">Bitly Access Token</a>
          </li>

        </ul>
      </div>
    </div>
  </div>
  <div class="col-sm-9 col-lg-10 light bg">
    <div class="tab-content pos-rlt">
      <div class="tab-pane active" id="tab-1">
        <div class="p-a-md b-b _600">Public profile</div>
        <form action="/setting" method="POST" role="form" class="p-a-md col-md-6">
          {{ csrf_field() }}
          {{ method_field('PUT') }}
          
          <div class="form-group">
            <label>Nama</label>
            <input type="text" value="{{$user->name}}" name="name" class="form-control">
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="email" value="{{$user->email}}" name="email" class="form-control">
          </div>

          <div class="form-group">
            <label>Password</label>
            <input type="password" name="password" placeholder="abaikan jika ga mau diubah" class="form-control">
          </div>

          <button type="submit" class="btn btn-info m-t">Update</button>
        </form>
      </div>
      <div class="tab-pane" id="tab-2">
        <div class="p-a-md b-b _600"> <h3>PANDUAN SETUP BRANDED DOMAIN</h3>
</div>
        <div class="padding">
         
<p>di orderlink, anda bisa menambahkan domain kesayangan anda. domain itu boleh berupa subdomain (cth : link.domainanda.com) 
</p>

<p>atau top level domain (TDL) seperti domain.com
</p>

<p>setup untuk branded domain TLD
</p>

<p>1. buka advance zone editor di cpanel anda
</p>
<p>2. pilih type record  : A
</p>
<p>3. name masukan   : linkanda.domainanda.com / domainanda.com
</p>
<p>4. TTL      : 14400
</p>
<p>5. Address    : 139.162.26.25
</p>
<p>setelah sukses, masukan domain anda di input form berikut ini</p>
        </div>
        <div class="row"> 
          <form role="form" action="/user-domain" method="POST" class="p-a-md col-md-6">
             {{ csrf_field() }}
            <div class="input-group">
              <input placeholder="domain.com or subdomain.domain.com" type="text" name="domain" class="form-control">
              <input type="hidden" name="author_id" value="{{$user->id}}">

              <span class="input-group-btn">
                <button class="btn info" type="submit"><i class="fa fa-plus"></i> Domain</button>
              </span>
            </div>
          </form></div>
            <div class="list-group m-b col-md-6">
              @foreach($domains as $domain)
              <span class="list-group-item">
                <span class="pull-right">  
                  <a class="nav-link" data-toggle="dropdown">
                    <i class="fa fa-fw fa-ellipsis-v"></i>
                  </a>
                  <div class="dropdown-menu pull-right">
                    <a  onclick="event.preventDefault();
                document.getElementById('delete-domain{{$domain->id}}').submit();"  class="dropdown-item" href="#">Hapus</a>
                  </div>
                </span>

                <span class="pull-right label {{$domain->status == 'submitted' ? 'info' : 'success'}}">{{$domain->status}}</span>
      
                {{$domain->domain}}
              </span>

              <form id="delete-domain{{$domain->id}}" action="/user-domain/{{$domain->id}}" method="POST" style="display: none;">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
          </form>
              @endforeach
            </div>

        </div>
              <div class="tab-pane" id="tab-3">
        <div class="p-a-md b-b _600">Bitly Access Token</div>
        <form action="/setting" method="POST" role="form" class="p-a-md col-md-6">
          {{ csrf_field() }}
          {{ method_field('PUT') }}
          <div class="form-group">
            <label>Bitly Access Token</label>
            <input type="text" name="bitly_access_token" value="{{$user->bitly_access_token}}" class="form-control">
          </div>

          <button type="submit" class="btn btn-info m-t">Update</button>
        </form>
      </div>

      </div>
    </div>

  </div>
</div>



@endsection

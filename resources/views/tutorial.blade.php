@extends('layouts.app')

@section('title','Dashboard')

@section('content')
<!-- ############ PAGE START-->
<div class="row-col">
    <div class="col-lg b-r">
        <div class="row no-gutter">
        <div class="box info">
            <div class="box-body">
               @if(count(Auth::user()->roles)==0)
                <p class="text-md profile-status pull-right"><a class="btn btn-fw white text-info">Upgrade ke Pro</a></p>
              @endif
              
              <div class="clear">
                <h3>{{Auth::user()->name}}, Ikuti Tutorial Orderlink disini!</h3>
                @if(count(Auth::user()->roles)>0)
                <small class="block text-muted">{{Auth::user()->roles[0]->display_name}}</small>
                @endif


                
              </div>
            </div>
          </div>
           
        </div>
        <div class="padding">
		<iframe width="560" height="315" src="https://www.youtube.com/embed/7jiSPMNr0kE?list=PLrcLolywcwtkCvzF7jqLkHTAHEobkfwsy" frameborder="0" allowfullscreen></iframe>            
        </div>
    </div>
    <div class="col-lg w-lg w-auto-md white bg">
        <div>

            <div class="p-a">
                <h6 class="text-muted m-a-0">Ada Pertanyaan?</h6>
            </div>
            <div class="streamline streamline-theme m-b">
            
              <div class="container">
              <!-- 	<form action="" method="POST">
              		<div class="form-group">
              		<label for="">Judul Pertanyaan</label>
              		<input type="text" class="form-control">

              	</div>
              	<div class="form-group">
              		<label for="">Isi Pertanyaan</label>
              		<textarea name="" id="" cols="30" class="form-control" rows="10"></textarea>

              	</div>
              	<button class="btn info" style="width: 100%" >Kirim Pertanyaan</button>
              	</form> -->
                <a href=""></a>
              </div>
              
            </div>
        </div>
    </div>
</div>

@endsection

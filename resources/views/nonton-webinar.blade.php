@extends('layouts.app')

@section('title','Dashboard')

@section('content')
<!-- ############ PAGE START-->
<div class="row-col">
    <div class="col-lg b-r">
        <div class="row no-gutter">
        <div class="box info">
            <div class="box-body">
 
              
              <div class="clear">
                <h3>{{Auth::user()->name}}, Temukan Insight Baru dalam Webinar ini!</h3>

                
              </div>
            </div>
          </div>
           
        </div>
        <div class="padding">
		<iframe width="560" height="315" src="https://www.youtube.com/embed/PDpRe5uBr7I" frameborder="0" allowfullscreen></iframe>     
        </div>
          <div class="padding">
		<iframe width="560" height="315" src="https://www.youtube.com/embed/3Z7kfqXQXH4" frameborder="0" allowfullscreen></iframe>  
        </div>
    </div>
    <div class="col-lg w-lg w-auto-md white bg">
        <div>

            <div class="p-a">
                <h6 class="text-muted m-a-0">Ada Pertanyaan?</h6>
            </div>
            <div class="streamline streamline-theme m-b">
            
              <div class="container">
              <!-- 	<form action="" method="POST">
              		<div class="form-group">
              		<label for="">Judul Pertanyaan</label>
              		<input type="text" class="form-control">

              	</div>
              	<div class="form-group">
              		<label for="">Isi Pertanyaan</label>
              		<textarea name="" id="" cols="30" class="form-control" rows="10"></textarea>

              	</div>
              	<button class="btn info" style="width: 100%" >Kirim Pertanyaan</button>
              	</form> -->
                <a href=""></a>
              </div>
              
            </div>
        </div>
    </div>
</div>

@endsection

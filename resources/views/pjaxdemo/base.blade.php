<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title', 'My Site')</title>
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>

    <nav class="container">
        <ul class="nav nav-tabs">
            <li>
                <a href="/">Home</a>
            </li>

            <li>
                <a href="/about">About</a>
            </li>
        </ul>
    </nav>

    <div class="container pjax-container" >
        @yield('content')
    </div>

    <h4 class="text-center">{{ time() }}</h4>

       <script src="/libs/jquery/dist/jquery.js"></script>
    <script src="/libs/jquery-pjax/jquery.pjax.js"></script>>
    <script>
        $(document).pjax('a', '.pjax-container');
    </script>
</body>
</html>
@extends('layouts.app')

@section('content')
@if (session('status'))
  <script>
  var sites = {!! json_encode(session('status')) !!};
  toastr.success(sites);
  var user = {!! Auth::user() !!};

  if(sites == 'Selamat Datang di Orderlink!'){
         $.post( "https://api.elasticemail.com/v2/contact/quickadd", { apikey: "b8d566e6-41bd-413d-8646-049b1cac94f6", emails: user.email , firstName : user.name, publicListID : "a8caf7d1-15e4-4fb4-a14e-dd12f8651ba0" });

         $.get( "/api/send/email");

  }



  </script>
@endif



<div class="container"  style="display: flex; flex-direction: column; align-items: center; justify-content: center; min-height: 600px">
                <div >
          <h3>Hallo, {{Auth::user()->name}}</h3>  
         	<h5>Verifikasi Email Anda sebelum menggunakan Aplikasi ini</h5>
         	<h5>Caranya cukup ikuti 1-2-3: </h5>
         	<h5>1. Buka Kotak Masuk Email Anda</h5>
         	<h5>2. Cari Email dari Orderlink Team</h5>
         	<h5>3. Klik Tombol Verifikasi Akun</h5>
			
			<p>jika email tidak ditemukan, silakan klik tombol di bawah ini</p>
         	<a  href="/resend">
              <span class="btn btn-sm info white">Kirim Ulang Email</span>
              </a>
            <a  href="/setting">
              <span class="btn btn-sm info white">Ganti Email</span>
              </a>


</div>
@endsection

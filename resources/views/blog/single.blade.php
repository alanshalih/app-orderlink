<!DOCTYPE html>

	<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
	<!--[if IE 7]><html class="lt-ie9 lt-ie8"> <![endif]-->
	<!--[if IE 8]><html class="lt-ie9"> <![endif]-->
	<!--[if gt IE 8]><!--> 
	
<html>

	<!--<![endif]-->
	<!-- Head -->
	<head>

		<!-- Meta -->
		<meta charset="utf-8">

		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">


		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


		<!-- Title -->
		<title>Ayam Goreng</title>

		<meta property="og:url" content="">

		<!-- Short-Cut Icon -->
		<link rel="shortcut icon" href="#" />

		<!-- Google Web Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600,700,900' rel='stylesheet' type='text/css'>
		
		<!-- Font Awesome Version - 4.6.3 -->
		<link href="/modal/font-awesome-4.6.3/css/font-awesome.min.css" rel="stylesheet" media="all">

		<!-- Bootstrap Style Sheet Version - 3.3.6 -->
		<link href="/modal/css/bootstrap.min.css" rel="stylesheet" media="all">


		<style>


				
		.modal.rbm_bottom_center {
		  top: auto;
		  overflow: visible; 
		}

		.modal.rbm_bottom_left {
		  top: auto;
		  overflow: visible; 
		}

		.modal.rbm_bottom_right {
		  top: auto;
		  overflow: visible; 
		}

		.modal.rbm_top_center {
		  bottom: auto;
		  overflow: visible; 
		}

		.modal.rbm_top_left {
		  bottom: auto;
		  overflow: visible; 
		}

		.modal.rbm_top_right {
		  bottom: auto;
		  overflow: visible; 
		}



		</style>

		<!-- Responsive Bootstrap Modal Popup Main Style Sheet -->
		<link href="/modal/css/responsive_bootstrap_modal_popup.css" rel="stylesheet" media="all">


	</head>

<body>


	<!--
	In this example we have used a link to Trigger the Modal.
	You can also use images, links, buttons etc to Trigger the Modal.
	-->
	  
    <header>
    	<div class="container">
    		<div class="row">
    			<h1 class="text-center">Judul Halaman</h1>
    		</div>
    		<div class="row">
    			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque id nihil doloribus illum minima, itaque est voluptatum dolorem quae commodi nobis suscipit ad. Maxime odit qui ullam consectetur! Quaerat, sed!</p>
    		</div>
    	</div>
    </header>
	
	<!-- End of Trigger Part -->
	
	<!-- Responsive Bootstrap Modal Popup -->




    <!-- End Responsive Bootstrap Modal Popup -->

	<!-- jQuery Version - 1.12.4 -->
	<script src="/modal/js/jquery-1.12.4.min.js"></script>

	<!-- Bootstrap JS File Version - 3.3.6 -->
	<script src="/modal/js/bootstrap.min.js"></script>

	<!-- Responsive Bootstrap Modal Popup Main JS File -->
	<script src="/modal/js/responsive_bootstrap_modal_popup_min.js"></script>

	<script src="/libs/exitModal/exitModal.js"></script>

	




</body>

</html>

 <!-- End -->
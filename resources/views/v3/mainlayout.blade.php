<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Orderlink v3</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <link rel="stylesheet" href="{{elixir('css/main.css')}}">

 <style>	


.vue-html5-editor>.toolbar>.dashboard button {
  border: none;
}

.vue-html5-editor>.toolbar>.dashboard button:hover {
  border: none;
}


 </style>

</head>
<body>
		
	<div id="appv3">
		<mainlayout></mainlayout>
	</div>
	<script type="text/javascript">
            window.Laravel = {
                csrfToken : "<?= csrf_token();  ?>"
            };
        </script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.1/socket.io.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.6.1/clipboard.min.js"></script>
	<script src="{{elixir('/js/appv3.js')}}"></script>
</body>

</html>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Document</title>

	<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet" type="text/css">

	<link href="https://unpkg.com/vuetify/dist/vuetify.min.css" rel="stylesheet" type="text/css">
	<style>
		table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

#invoice tr:nth-child(even) {
    background-color: #dddddd;
}

@media only screen and (max-width: 768px) {
    /* For mobile phones: */
     #bank {
        width: 100%;
    }
}
	</style>

	
</head>
<body>

	<div id="app">
		<v-container>
			<v-layout>
				<v-flex xs12 md6 offset-md3>
					
					<v-layout>
						<v-flex xs7 md9 class="info--text text--darken-4 pt-3">
							<strong class="display-1"><span>Invoice #@{{order.id}}</span></strong>
						</v-flex>
						<v-flex xs5 md3>
							<img :src="store.avatar" width="100%" alt="">
						</v-flex>
					</v-layout>
					<h5>{{$purchase->buyer->firstname}} {{$purchase->buyer->lastname}}</h5>
					<p>{{$purchase->buyer->address}}</p>
					<table id="invoice" class="mt-5">
						<tr>
							<th>No</th>
							<th>Produk</th>
							<th>Harga</th>
							<th>Jumlah</th>
							<th>Total</th>
						</tr>
						<tr v-for="(item, index) in order.products">
							<td>@{{index+1}}</td>
							<td>@{{item.title}}</td>
							<td>@{{item.price}}</td>
							<td>@{{item.pcs}}</td>
							<td>@{{item.pcs * item.price}}</td>
						</tr>
						<tr>
							<td colspan="4">Total Harga Produk</td>
							<td>@{{order.component_price.base_price}}</td>
						</tr>
						<tr>
							<td colspan="4">Biaya Kirim</td>
							<td>@{{order.component_price.courier_cost}}</td>
						</tr>
						<tr>
							<td colspan="4">Diskon</td>
							<td>@{{order.component_price.discount}}</td>
						</tr>
						<tr>
							<td colspan="4">Biaya Lain</td>
							<td>@{{order.component_price.another_cost}}</td>
						</tr>
						<tr>
							<td colspan="4">Angka Unik</td>
							<td>@{{order.component_price.unique}}</td>
						</tr>
						<tr style="background-color: #1a237e; color : white">
							<td colspan="4">Substotal</td>
							<td>@{{order.sell_price}}</td>
						</tr>
					</table>
					<h6 class="mt-5">Petunjuk Pembayaran</h6>
					<div v-if="false">{{\Carbon\Carbon::setLocale('id')}}</div>
					<p>Silakan Lakukan Pembayaran Sebelum <strong>{{ \Carbon\Carbon::parse($purchase->expired)->formatLocalized('%d-%m-%Y %H:%M') }} ({{ \Carbon\Carbon::parse($purchase->expired)->diffForHumans() }}). </strong></p>
					<v-card>
						<v-card-text class="text-xs-center">
						<h2 class="info--text">Rp.{{number_format($purchase->sell_price,0, '.', '.')}}</h2>
						<span>mohon transfer persis angka di atas (jangan dibulatkan)</span>
						</v-card-text>
					</v-card>

					<!-- <p class="mt-5">ke rekening berikut ini</p> -->

             <table>
				<tr v-if="store.banks.length > 0" v-for="(item, index) in store.banks">
					<td><span><img :src="logo[item.bank]" id="bank" alt=""></span></td>
					<td><span class="headline">@{{item.bank_account}}</span><br>
                    <span class="title">An. @{{item.bank_username}}</span></td>
				</tr>
             </table>


					



				</v-flex>

			</v-layout>
		</v-container>
	</div>

	<script src="https://unpkg.com/vue/dist/vue.js"></script>

	<script src="https://unpkg.com/vuetify/dist/vuetify.min.js"></script>

	<script>
		new Vue({
			mounted(){
				var order = {!! $purchase !!};

				var store = {!! $purchase->store !!};

				this.store = store;

				this.order = ((order));

				this.order.products = JSON.parse(order.products);

				this.order.component_price = JSON.parse(order.component_price);

				this.store.banks = JSON.parse(store.banks);
			},
			el : '#app',
			data : {
				order : '',
				store : '',
				logo : {
		          BCA : '/images/bca_logo.png',
		          MANDIRI : '/images/logo-bank-mandiri.png'
		        },
			},
			
		})
	</script>
</body>
</html>
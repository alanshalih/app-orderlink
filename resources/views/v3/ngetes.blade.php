
<!DOCTYPE html>
<html >
<head>
	<meta charset="UTF-8">
	<title>Responsive Charts with Chartist.js</title>


	<link rel='stylesheet prefetch' href='https://s3-us-west-2.amazonaws.com/s.cdpn.io/85781/chartist-0.0.2.min.css'>



</head>

<body>
	<div class="container">
		<div id="chartist-chart" class="ct-chart"></div>
	</div>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/snap.svg/0.1.0/snap.svg-min.js'></script>
	<script src='https://s3-us-west-2.amazonaws.com/s.cdpn.io/85781/chartist-0.0.2.min.js'></script>

	<script>
		var data = {
			labels: ['1', '2', '3', '4', '5', '6'],
			series: [
			{
				data: [1, 2, 3, 5, 8, 13]
			},
			{
				data: [0, 3, 5, 4, 3, 11]
			},
			{
				data: [3, 5, 9, 7, 4, 2]
			}
			]
		};

		var options = {
			chartPadding: 5,
			axisX: {
				offset: 10,
				labelInterpolationFnc: function(value) {
					return 'Week ' + value;
				}
			},
			axisY: {
				offset: 10
			}
		};

		var responsiveOptions = [
		['screen and (min-width: 641px) and (max-width: 1024px)', {
			showPoint: false,
			lineSmooth: false
		}],
		['screen and (max-width: 640px)', {
			showLine: false,
			axisX: {
				labelInterpolationFnc: function(value) {
					return 'W' + value;
				}
			}
		}]
		];

		var chart = Chartist('#chartist-chart', data, options, responsiveOptions);

	</script>

</body>
</html>

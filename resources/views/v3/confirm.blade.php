<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Document</title>

	<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet" type="text/css">

	<link href="https://unpkg.com/vuetify/dist/vuetify.min.css" rel="stylesheet" type="text/css">

	
</head>
<body>

	<div id="app">
		<v-container>
			<v-layout>
				<v-flex xs12 md6 offset-md3>
					<v-card horizontal class="mt-3 elevation-0">
						<v-card-column class="info white--text">
							<v-card-row>
								<v-spacer></v-spacer>
								<v-card-text class="text-xs-left">
									<strong class="display-1">Konfirmasi Order</strong>
								</v-card-text>
							</v-card-row>
						</v-card-column>
						<v-card-row style="display: flex; justify-content: center"><img :src="store.avatar" height="125px" alt=""></v-card-row>
					</v-card>

					<v-layout>
						<v-flex xs12 md9>
							<p class="title mt-5 pt-2">Produk yang dibeli</p>
							<v-list two-line>
								
								<v-list-item  v-for="(item, index) in order.products">
									<v-list-tile avatar>
										<v-list-tile-avatar>
											<img v-bind:src="item.thumbnail" />
										</v-list-tile-avatar>
										<v-list-tile-content>
											<v-list-tile-title v-html="item.title" />

										</v-list-tile-content>
										<v-list-tile-action>

											<v-text-field style="width : 8px"
											v-model="item.pcs"


											label="pcs"
											type="number"
											></v-text-field>
										</v-list-tile-action>
										<v-list-tile-action>

											<v-btn floating small error @click.native.stop="removeitem(index)">
												<v-icon light >close</v-icon>
											</v-btn>
										</v-list-tile-action>
									</v-list-tile>
								</v-list-item>
							</v-list>
							<v-divider class="mt-4"></v-divider>
							<form :action="'/order/'+order.id+'/confirm'" method="POST">
							 {{ csrf_field() }}
							<v-text-field
							label="Alamat Pengiriman"
							v-model="buyer.address"
							name="address"
							multi-line
							></v-text-field>
							<input type="hidden" name="products" v-model="products">
							<p class="text-xs-center"><i class="material-icons">arrow_downward</i><i class="material-icons">arrow_downward</i> KLIK <i class="material-icons">arrow_downward</i><i class="material-icons">arrow_downward</i></p>
							<v-btn type="submit" block primary light>Ya, Data Sudah Benar!</v-btn>
							</form>
						</v-flex>
					</v-layout>



				</v-flex>

			</v-layout>
		</v-container>
	</div>

	<script src="https://unpkg.com/vue/dist/vue.js"></script>

	<script src="https://unpkg.com/vuetify/dist/vuetify.min.js"></script>

	<script>
		new Vue({
			mounted(){
				var order = {!! $purchase !!};

				var store = {!! $purchase->store !!};

				var buyer = {!! $purchase->buyer !!};

				this.order = ((order));

				this.buyer = buyer;

				this.store = store;

				this.order.products = JSON.parse(order.products);
			},
			el : '#app',
			data : {
				order : '',
				buyer : '',
				store : '',
			},
			computed: {
				products : function(){
					return JSON.stringify(this.order.products);
				}
			},
			methods : {
				removeitem : function (index){
					this.order.products.splice(index,1);
				},

			}
		})
	</script>
</body>
</html>
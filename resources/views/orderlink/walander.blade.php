<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>1 Klik WhatsApp Orderlink</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <meta name="csrf-token" content="{{ csrf_token() }}">
  
  
  <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>
<link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800'>
<link rel='stylesheet prefetch' href='https://decorator.io/tools/modulr.min.css'>
<link rel='stylesheet prefetch' href='https://decorator.io/tools/tooltip.min.css'>


     

<meta property="og:title" content="1 Klik WhatsApp Auto-Order"/>
<meta property="og:image" content="http://res.cloudinary.com/resensi-digital/image/upload/v1489447613/wa_tqiole.jpg"/>
<meta property="og:site_name" content="WA.Orderlink"/>
  <meta property="og:url" content="http://wa.orderlink.in">
<meta property="og:description" content="Orderan Mudah, Rejeki Lancar!"/>


<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '157095107992587'); // Insert your pixel ID here.
bq('track', 'PageView');


</script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '200022016998409'); // Insert your pixel ID here.
fbq('track', 'PageView');

fbq('init', '157095107992587'); // Insert your pixel ID here.
fbq('track', 'PageView');

fbq('init', '926629954099014'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=200022016998409&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->



     <style>

html,
body {
  height: 100%;
  overflow-x: hidden;
}

.wa-background{
  background-color : #075e54
}

.wa-text{
  color : #075e54
}

.width-70 {
  width: 70%;
}

.margin-top-10 {
  margin-top: 10%;
}

.margin-top-15 {
  margin: 15% auto 5% auto;
}

.margin-top-15 h2,
h5 {
  line-height: 1.5;
}

@media (max-width: 800px) {
  .margin-top-15,
  .margin-top-10 {
    margin: 0 auto 5% auto;
  }
}

#credit{
  width: 100%;
  text-align: center;
}
     </style>

  
</head>

<body>
  <!-- Content Row Start -->
 <!-- Content Row Start -->
<div id="app" class="row height-100">

  <!-- Left Section Start -->
  <div class="col-8 wa-background height-100 blue tablet-height-auto">

    <!-- Logo  Start -->
    <div class="padding-25">
    </div>
    <!-- Logo End -->
    <!-- Intro Start -->
    <div class="margin-top-15 width-70">
       <p class="text-center">telah digunakan oleh {{$counter}} orang Pebisnis Online Seperti anda</p>
      <h2 class="text-white text-center mobile">Sekarang Customer Anda bisa Order di WhatsApp dalam 1 Klik Tanpa Capek Nyatat Nomor dan Nulis Pesan </h2>

      <!-- Intro Buttons Start -->
      <div class="button-center padding-top-20 hover-href-text-green">
        <a class="btn l round border white margin-right mobile-full-width mobile-margin-bottom" href="http://ffsandi.co/wa/j2v3KZ7r">
          <i class="fa fa-lg fa-whatsapp"></i> Coba Sekarang
        </a>
        

      </div>
      
   
      <!-- Intro Buttons End -->
    </div>


    <!-- Intro End -->
       <div id="credit">Powered by Orderlink.in</div>
      

  </div>
  <!-- Left Section End -->

  <!-- Right Section Start -->
  <div class="col-4 height-100 padding-40 tablet-height-auto mobile-padding">

    
    <!-- Register Intro Start -->
    <div v-show="!success" id="wizard-title">  
    <div v-show="next" id="first-step" class="padding-40 mobile-padding margin-top-10">
      <h3 class="text-center wa-text margin-bottom mobile">1 Langkah lagi</h3>
      <h5 class="text-center text-18">
        Silakan Isi Email Aktif & Nama untuk bisa mulai menggunakan link auto-order WhatsApp
      </h5>
    </div>
     <div v-show="!next" id="second-step" class="padding-40 mobile-padding margin-top-10">
      <h3 class="text-center wa-text margin-bottom mobile">Udah Nyoba?</h3>
      <h5 class="text-center text-18">
        Sekarang anda bisa bikin link seperti itu GRATIS dengan mengisi form di bawah ini
      </h5>
    </div>
    </div>
    <div id="sucess-title" v-show="success">
         <div  id="second-step" class="padding-40 mobile-padding margin-top-10">
      <h3 class="text-center wa-text margin-bottom mobile">Selamat link Auto-Order anda selesai dibuat!</h3>
      <h5 class="text-center text-18">
        inilah detil OrderLink anda
      </h5>
    </div>
    </div>

    <!-- Register Intro End -->

    <!-- Register Form Start -->
    <form v-show="!success" class="form step-form wire border blue padding-20 -padding-top mobile-padding">
<div v-show="next == false" id="first-step">
      <div class="group gutter-bottom-10">
        <span class="text-green">masukan nomor hp anda</span>
        <input v-model="phone" class="l border-silver focus-border-green text-black" type="text" placeholder="cth : 6287815579530 | wajib didahului 62">
      </div>
      
      <div class="group gutter-bottom-10">
        <span class="text-green">isi pesan yang dikirim pelanggan</span>
        <textarea class="l border-silver focus-border-green text-black" v-model="message" placeholder="cth : ORDER ORDERLINK an. Maulana Shalihin"></textarea>
      </div>




  </div>

      <div v-show="next == true" id="second-step">
      <div class="group gutter-bottom-10">
        <span class="text-green">masukan email anda</span>
        <input v-model="email" class="l border-silver focus-border-green text-black" type="email" placeholder="cth : maulanashalihin@gmail.com">
      </div>
         <div class="group gutter-bottom-10">
        <span class="text-green">masukan nama anda</span>
        <input v-model="name" class="l border-silver focus-border-green text-black" type="text" placeholder="cth : maulana">
      </div>
          
  </div>
       <div v-show="konfirm == true" style="padding-top : 20px; color : red">*Pastikan <strong>email</strong> yg Anda masukan aktif & sudah benar (tidak ada Salah ketik). Karena link akan dikirimkan via email. Jika sudah baner, silakan pencet tombol Bikin Sekarang!</div>

      <div class="group">

        <!-- Register Button Start -->
        <button v-show="next" @click.prevent="backStep()" class="btn rounded-2 text500 hover-fill-green hover-text-white text-green border-green">Back</button>
        <!-- Register Button End -->

        <!-- Social Login Start -->
        <div class="pull-right">
           <button v-show="!next" @click.prevent="nextStep()" class="btn rounded-2 text500 hover-fill-green hover-text-white text-green border-green">Next</button>
            <button v-show="next" @click.prevent="submit()" class="btn rounded-2 text500 hover-fill-green hover-text-white text-green border-green">Bikin Sekarang!</button>
        </div>
        <!-- Social Login End -->

      </div>
    </form>

     

    <div v-show="success">
 <div class="row">   <div class="col-3"><strong>link url</strong></div>
      <div class="col-12">http://wa.orderlink.in/@{{no}}/@{{this.phone}}</div></div>
      <br>
   <div class="row">
        <div class="col-3"><strong>statistik url</strong></div>
      <div class="col-12">http://wa.orderlink.in/@{{stat}}/@{{this.phone}}?token=@{{token}}</div></div>

      <div v-show="no == '***'" style="padding-top : 20px">beberapa bagian dari link di atas di sensor, kami sudah mengirimkan bagian yang hilang di inbox email anda. silakan cek inbox anda, tandai sebagai email penting untuk memudahkan pencarian. jika email tidak ditemukan, silakan cek folder promosi atau spam. jika belum ada bisa pencet tombol kirim ulang di bawah ini atau hubungi CS kami via <a target="_blank" href="https://www.facebook.com/OrderLink-Web-CS-396588787389915/">Messenger - klik disini</a></div>

      <div style="padding-top : 20px">
      <button v-show="next" @click.prevent="gantiEmail()" class=" btn rounded-2 text500 hover-fill-green hover-text-white text-green border-green">Ganti Email</button> <button v-show="next" @click.prevent="gantiEmail()" class="btn rounded-2 text500 hover-fill-green hover-text-white text-green border-green">Kirim Ulang Email</button></div>
      
    
    </div>
    <!-- Register Form End -->

  </div>
  <!-- Right Section End -->

</div>

<div class="row" style="padding : 50px;">
<div class="container">
  
  <br>
    <h3 style="text-align : center">DISCLAIMER: </h3>
    <br>
<p style="text-align : center">
id: semua merk dan logo adalah milik dari pemiliknya masing-masing. produk dan layanan yang ditawarkan di situs ini tidak terkait, afiliasi, didukung, atau disponsori oleh perusahaan yang tertulis di halaman ini.</p>

<p style="text-align : center; margin-top : 30px">en: all trademarks and logos are the property of their respective owners. this site and the products and services offered on this site are not associated, affiliated, endorsed, or sponsored by any business listed on this page nor have they been reviewed tested or certified by any other company listed on this page.</p>
</div>
</div>
<!-- Content Row End -->
  <script src='https://cdnjs.cloudflare.com/ajax/libs/vue/2.1.10/vue.min.js'></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>



    <script>
    var app = new Vue({
  el: '#app',
  data: {
    next : false,
    success : false,
    konfirm : false,
    no : '***',
    stat : '***',
    phone : '628',
    message : '',
    email : '',
    name : '',
    token : ''
  },
  mounted : function (){

    this.token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');


    window.axios.defaults.headers.common = {
      'X-CSRF-TOKEN': this.token,
      'X-Requested-With': 'XMLHttpRequest'
    };


  },
 methods: {
    nextStep: function () {
      this.next = true;

      var self = this;

      axios.post('/wacampaign/signup', {
        phone: this.phone,
        message : this.message
      }).then(function (response) {

        self.email = (response.data.email);

        if(response.data.token){
          self.token =  response.data.token;
          self.success = true;
          self.no = 'no';
          self.stat = 'stat';
        }

        self.name = response.data.name;
      });



    },
   backStep : function () {
     this.next = false;
   },
   submit : function () {
        if(this.konfirm){

          axios.post('/wacampaign/signup', {
          email: this.email,
          name : this.name,
          token : this.token,
          phone : this.phone
        });   

             this.success=true;

             axios.post('https://api.elasticemail.com/v2/contact/quickadd', { apikey: "b8d566e6-41bd-413d-8646-049b1cac94f6", emails: this.email , firstName : this.name, publicListID : "6789cc11-7f59-4e56-9b69-0378d54e68fc" }); 

 

     

        }else{
          this.konfirm = true;
        }
    



   },
   gantiEmail : function () {
     this.success=false;
     this.next = true;
   },

  }
});





    </script>
    <script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 928823537;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/928823537/?guid=ON&amp;script=0"/>
</div>
</noscript>
<link rel="stylesheet" href="/css/notyf.min.css">

<script src="/js/notyf.min.js">   </script>

<script>  
</script>

<script>  
var notyf = new Notyf();


</script>


<script>


var counter = 0;
var users = {!! json_encode($users) !!};



function random(){
  return Math.random()*20000;
}


function showAlert(){

  setTimeout(function() {

  notyf.confirm(users[counter].name+' telah membuat WA Auto-Order dengan nomor '+users[counter].phone+'!');

    counter++;

   if(counter >= (users.length)){
        counter = 0;
   }


  showAlert();

  }, random());

}

showAlert();






</script>




</body>
</html>

<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="/stack/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="/stack/css/theme.css" rel="stylesheet" type="text/css" media="all" />
        <link href="/stack/css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700" rel="stylesheet">

    </head>
    <body data-smooth-scroll-offset="77">
        <div class="nav-container"> </div>
        <div class="main-container">
            <section class="switchable bg--secondary">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-5">
                            <div class="switchable__text">
                                <div class="switchable__text">
                                    <h2>WhatsApp Orderlink</h2>
                                    <p class="lead">WhatsApp Auto-Order adalah bagian kecil dari fitur-fitur Dahsyat Orderlink.in. Selamat Mencoba!<br><br>Jika anda menyukainya, silakan bagikan info berharga ini kepada sahabat anda.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-md-pull-1 col-xs-12">
                            <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                                <h4>Click Counter</h4> <span class="h1"><span class="pricing__dollar">{{$stat ?? 0}}/</span>{{$wa->limit_klik}}*</span>
                                <p>*jika melewati batas maksimum di atas, anda akan mendapatkan peringatan yang muncul pada link anda. watermark pada link akan dihilangkan pada Orderlink versi Pro.</p> 
                                <a class="btn btn--primary" href="http://orderlink.in" target="_blank"> <span class="btn__text">
                            Aktifkan Orderlink Pro!</span> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="cover imagebg height-70 text-center" data-overlay="6">
                <div class="background-image-holder"><img alt="background" src="/img/insurance-3.jpg"></div>
                <div class="container pos-vertical-center">
                    <div class="row">
                        <div class="col-sm-10 col-md-8">
                            <h1>Ingin dapat lebih banyak batas klik? Yuk sama2 berbagi!</h1>
                            <p class="lead">setiap 1 teman dan sahabat anda yang mendaftar di WhatsApp Orderlink dengan mengeklik link unik anda di bawah ini. anda berhak mendapatkan 100 credit klik tambahan. copy linknya, dan paste ke semua sosial media yang anda miliki.</p>
                            <div class="boxed boxed--lg bg--white text-left">
                  
                                    <div class="col-sm-8"> <input id="foo" type="text" value="http://wa.orderlink.in/share/{{$wa->id}}"> </div>
                                    <div class="col-sm-4"> <button data-clipboard-target="#foo" type="submit" class="btn btn--primary type--uppercase">COPY</button> </div>


                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
       <script src="/stack/js/jquery-3.1.1.min.js"></script>
        <script src="/stack/js/parallax.js"></script>
        <script src="/stack/js/countdown.min.js"></script>
        <script src="/stack/js/smooth-scroll.min.js"></script>
        <script src="/stack/js/scripts.js"></script>
              <script src="https://cdn.jsdelivr.net/clipboard.js/1.5.13/clipboard.min.js"></script>
                   <link rel="stylesheet" href="/css/toastr.min.css">
                       <script src="/libs/toastr/toastr.min.js"></script>
              <script>
                   var clipboard = new Clipboard('.btn');

    clipboard.on('success', function(e) {
        toastr.success('Link telah dicopy');
    });

    clipboard.on('error', function(e) {
        console.log(e);
    });

              </script>
    </body>

</html>
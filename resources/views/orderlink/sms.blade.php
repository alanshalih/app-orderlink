@extends('orderlink.template')
@section('headmeta')
<title>
	SMS ke {{$getcampaign->nomor}}
</title>
<meta property="og:title" content="SMS ke {{$getcampaign->nomor}}" />
<meta property="og:image" content="http://res.cloudinary.com/ffsandi/image/upload/c_scale,w_420/v1486002342/sms_stusov.png" />
@endsection
@section('content')
  <script>

 var max = 1000;
 var init = 0;
 var add = 200;


     setTimeout(function(){
      
        if('{{$getcampaign->isisms}}'.length>0){
            if('{{$platform}}' == 'iPhone'){
              window.location = 'sms:{{$getcampaign->nomor}}';
            }else{
              window.location = 'sms:{{$getcampaign->nomor}}?body={{$getcampaign->isisms}}';
            }
           
         }else{
            window.location = 'sms:{{$getcampaign->nomor}}';
         }
       
    },max);
  </script>

  
@endsection
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Contact-Card</title>
  
  
  
      <style>
      	@import url(https://fonts.googleapis.com/css?family=Oswald);

body {
  margin: 0 auto;
  background-color: #999;
}

.container {
  display: flex;
  width: 100vw;
  height: 100vh;
  padding-top: 30vh;
  justify-content: center;
  align-content: center;
 }

.contact-box {
  display: flex;
  flex-direction: column;
  width: 600px;
  height: 300px;
  background-color: white;
  justify-content: center;
 
}


.header {
  flex-direction: row;
  margin-top:-60px;
  height: 80px;
  background-color: #075e54;
  justify-content: center;
}

.header h3{
  text-align: center;
  font-family: arial;
  color: white;
}

.mail {
  margin-top: 20px;
  margin-bottom: 50px;
  font-family: Oswald;
  font-size: 1.5rem;
  align-self: center;
  border-bottom: 2px solid black;
}

.linkedin {
  margin-top: -40px;
  font-family: Oswald;
  font-size: 1rem;
  align-self: center;
}

a {
  text-decoration: none;
  color: black;
  
}

p{
  font-family: arial;
  font-size: 10px;
  padding-left: 10px;
}

a:hover {
  color: #075e54;
  
}


.contact-box:hover {
  animation: shake 1s cubic-bezier(0.455, 0.03, 0.515, 0.955) both;
  transform: translate3d(0, 0, 0);
  backface-visibility: hidden;
  perspective: 1000px;
}

@keyframes shake {
  10%, 100% {
    transform: translate3d(0, -1px, 0);
  }
  
  20%, 80% {
    transform: translate3d(0, 2px, 0);
  }

  30%, 50%, 70% {
    transform: translate3d(0, -4px, 0);
  }

  40%, 60% {
    transform: translate3d(0, 4px, 0);
  }
}
      </style>
  
</head>

<body>
  <div class="container">
  <div class="contact-box">
    <div class="header">
      <h3>Silakan simpan kontak berikut, lalu buka app whatsapp di smartphone Anda untuk menghubungi:</h3>
    </div> 
    <div class="mail">
      <h1><a href="https://web.whatsapp.com/send?text={{$wa->message}}&phone={{$wa->phone}}">{{$wa->name}}</a></h1>
    </div>
    <div class="linkedin">
      <a href="https://web.whatsapp.com/send?text={{$wa->message}}&phone={{$wa->phone}}" target="blank"><h2><img src="/images/wa.png" width="20px" alt="" />   {{$wa->phone}}</h2></a>
    </div>
    <p>*klik pada nomor untuk membuka WA versi desktop</p>
  </div>
 </div>
  
  
</body>
</html>

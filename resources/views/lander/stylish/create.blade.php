<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Stylish Portfolio - Start Bootstrap Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="/stylish/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/stylish/css/stylish-portfolio.css?2" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/stylish/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
      <!-- Responsive Bootstrap Modal Popup Main Style Sheet -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">


    <link rel="stylesheet" href="/libs/pageBuilder/style.css?6">



</head>

<body>

  <div class="editbutton"  > 
    
 
    <a href="#" onclick="editMode(event)" tooltip="Edit Mode" class="buttons sm-btn  "><i id="edit-icon" style="padding-top: 3px"  class="fa  fa-pencil fa-fw fa-2x"></i></a>

    <a href="/icon-helper" target="_blank" class="buttons sm-btn" tooltip="Icon Helper"><i style="padding-top: 7px" class="fa fa-question fa-fw fa-2x "></i></a>

    <a href="#" data-toggle="modal" data-target="#icon-helper" class="buttons sm-btn" tooltip="SEO"><i style="padding-top: 7px" class="fa fa-cloud fa-fw fa-2x"></i></a>
    
    <a href="/home" class="buttons sm-btn" tooltip="Back to Home"><i style="padding-top: 7px" class="fa fa-home fa-fw fa-2x "></i></a>
<!-- 
    <a class="buttons" tooltip="Share" href="#"></a> -->
     <a  class="buttons md-btn " tooltip="Pengaturan"><i style="padding-top: 13px" class="fa fa-gears fa-fw fa-2x"></i></a>

  </div>

    <div class="click2edit">
    <div id="top" class="header">
        <div class="text-vertical-center" >
          
            <h1>Start Bootstrap</h1>
            <h3>Free Bootstrap Themes &amp; Templates</h3>
            <br>
            <a href="#about" class="btn btn-dark btn-lg">Find Out More</a>
        </div>
    </div>
    </div>


 
      
    <!-- About -->
    <div class="click2edit">
    <section id="about" class="about ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 >Stylish Portfolio is the perfect theme for your next project!</h2>
                    <p class="lead">This theme features some wonderful photography courtesy of <a target="_blank" href="http://join.deathtothestockphoto.com/">Death to the Stock Photo</a>.</p>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>

    </div>
    <!-- end of about -->
  


    <!-- Services -->
    <!-- The circle icons use Font Awesome's stacked icon classes. For more information, visit http://fontawesome.io/examples/ --> 
     <div class="click2edit">
    <section id="services" class="services bg-primary ">
        <div class="container ">
            <div class="row text-center">
                <div class="col-lg-10 col-lg-offset-1" >
                    <h2>Our Services</h2>
                    <hr class="small">
                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-cloud fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>Service Name</strong>
                                </h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                <a href="#" class="btn btn-light">Learn More</a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-compass fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>Service Name</strong>
                                </h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                <a href="#" class="btn btn-light">Learn More</a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-flask fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>Service Name</strong>
                                </h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                <a href="#" class="btn btn-light">Learn More</a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-shield fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>Service Name</strong>
                                </h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                <a href="#" class="btn btn-light">Learn More</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.col-lg-10 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>

    </div>
    <!-- end of service -->

  

    <!-- Portfolio --> 
      <div class="click2edit">
    <section id="portfolio" class="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 text-center">
                    <h2>Our Work</h2>
                    <hr class="small">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="portfolio-item">
                                
                                    <img class="img-portfolio img-responsive" src="/stylish/img/portfolio-1.jpg">
                                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portfolio-item">
                                
                                    <img class="img-portfolio img-responsive" src="/stylish/img/portfolio-2.jpg">
                                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portfolio-item">
                                
                                    <img class="img-portfolio img-responsive" src="/stylish/img/portfolio-3.jpg">
                                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="portfolio-item">
                                
                                    <img class="img-portfolio img-responsive" src="/stylish/img/portfolio-4.jpg">
                                
                            </div>
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                    <a href="#" class="btn btn-dark">View More Items</a>
                </div>
                <!-- /.col-lg-10 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>

    </div>
    <!-- end of portfolio -->
       

    <!-- Call to Action -->
     <div class="click2edit">
    <aside class="call-to-action bg-primary">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3>The buttons below are impossible to resist.</h3>
                    <a href="#" class="btn btn-lg btn-light">Click Me!</a>
                    <a href="#" class="btn btn-lg btn-dark">Look at Me!</a>
                </div>
            </div>
        </div>
    </aside>
    </div>
    <!-- end of call to action -->

        



    <!-- Footer -->
        <div class="click2edit">
    <footer>

        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 text-center">
                    <h4><strong>Start Bootstrap</strong>
                    </h4>
                    <p>3481 Melrose Place
                        <br>Beverly Hills, CA 90210</p>
                    <ul class="list-unstyled">
                        <li><i class="fa fa-phone fa-fw"></i> (123) 456-7890</li>
                        <li><i class="fa fa-envelope-o fa-fw"></i> <a href="mailto:name@example.com">name@example.com</a>
                        </li>
                    </ul>
                    <br>
                    <ul class="list-inline">
                        <li>
                            <a href="#"><i class="fa fa-facebook fa-fw fa-3x"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-twitter fa-fw fa-3x"></i></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-dribbble fa-fw fa-3x"></i></a>
                        </li>
                    </ul>
                    <hr class="small">
                    <p class="text-muted">Copyright &copy; Your Website 2014</p>
                </div>
            </div>
        </div>
        <a id="to-top" href="#top" class="btn btn-dark btn-lg"><i class="fa fa-chevron-up fa-fw fa-1x"></i></a>
    </footer>
    </div>

<!-- jQuery -->
<script src="/stylish/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/stylish/js/bootstrap.min.js"></script>

   


<!-- codemirror: todo: extract codemirror to bower -->
<!-- codemirror: todo: extract codemirror to bower -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.21.0/codemirror.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/theme/monokai.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.21.0/codemirror.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/mode/xml/xml.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/2.36.0/formatting.min.js"></script>

<!-- summernote plugin -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.js"></script>
<!-- jsColor plugin -->
<script src="/libs/jsColor/jscolor.js?3"></script>


    <!-- Custom Theme JavaScript -->
<script src="/libs/pageBuilder/pagebuilder.js?166"></script>



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Ganti Gambar</h4>
      </div>
      <div class="modal-body">
         <div class="form-group">
              <label for="bookId">Image</label>
              <input type="text" oninput="changeImage($('#bookId').val())" class="form-control" id="bookId" value="">
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button"   class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-background" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Ganti Gambar</h4>
      </div>
      <div class="modal-body">
         <div class="form-group">
              <label for="gambar">Image</label>
              <input type="text" class="form-control" oninput="updateBackgroundImage(value)" id="gambar" value="">
            </div>

            <div class="form-group">
                <label class="ui-switch info m-t-xs m-r">
                  <input type="checkbox" id="paralax" onclick="isParalax()"  >
                  <i></i>
                </label>
            </div>

             <div class="form-group">
              <label for="color">Background Color</label>
              <input type="text" class="jscolor form-control" id="color"  onchange="updateBackgroundColor(this.jscolor)" >
            </div>


     
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="closeModalWithoutSave()">Tutup Tanpa Disimpan</button>
        <button type="button" data-dismiss="modal"   class="btn btn-primary">Simpan Perubahan</button>
      </div>
    </div>
  </div>
</div>

<!-- .modal -->
<div id="icon-helper" class="modal fade animate" data-backdrop="true">
  <div class="modal-dialog" id="animate">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal</h5>
      </div>
      <div class="modal-body text-center p-lg">
        <p>Are you sure to execute this action?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn dark-white p-x-md" data-dismiss="modal">No</button>
        <button type="button" class="btn danger p-x-md" data-dismiss="modal">Yes</button>
      </div>
    </div><!-- /.modal-content -->
  </div>
</div>
<!-- / .modal -->




</body>

</html>

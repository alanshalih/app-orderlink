@extends('layouts.app')

@section('content')
@if (session('status'))
<script>
  var sites = {!! json_encode(session('status')) !!};
  toastr.success(sites);
</script>
@endif


<div class="padding">
  @if(count($data) == 0)
    <div class="col-md-6">
      <h3>Panduan Penggunaan Page Builder</h3>
      <br>
      <p>1. Klik Tombol <i class="fa fa-plus"></i> untuk memulai!</p>
      <p>2. untuk mengedit gambar, silakan klik pada gambar</p>
      <img class="img-responsive" src="http://res.cloudinary.com/resensi-digital/image/upload/v1485699007/ganti_gambar_rhp3rm.jpg" alt="">

      <p class="m-a">3. untuk mengedit, silakan aktifkan mode edit</p>
      <img class="img-responsive" src="http://res.cloudinary.com/resensi-digital/image/upload/v1485699006/edit_mode_n2m6zg.jpg" alt="">
      </div>
      @endif
  <div class="row">
     <!-- @if(!($data)) -->
  
     <!-- @endif -->
     @foreach($data as $list)
     <div class="col-xs-6 col-sm-4 col-md-3">
      <div class="box p-a-xs">
       <a href="#"><img src="{{$list->thumbnail}}" alt="" class="img-responsive"></a>
        <div class="p-a-sm">

          <h6>{{$list->title}}</h6>
          <div class="text-ellipsis" style="text-align: center">
            <div class="m-b">
              <div class="btn-group">
                <a href="/page/{{$list->id}}/edit" data-turbolinks="false" class="btn btn-sm info">Edit</a>
                <a href="/p/{{$list->url}}" target="_blank" class="btn btn-sm info">Tampilkan</a>
                <a href="#" onclick="event.preventDefault();
                                                     document.getElementById('delete-{{$list->id}}').submit();"  class="btn btn-sm info">Hapus</a>
          <form id="delete-{{$list->id}}" action="/page/{{$list->id}}" method="POST" style="display: none;">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
          </form>



              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endforeach      


</div>
<!-- <div>
  <div class="btn-group m-r">
    <button type="button" class="btn btn-sm white"><i class="fa fa-angle-left"></i></button>
    <button type="button" class="btn btn-sm white"><i class="fa fa-angle-right"></i></button>
  </div>
  Showing <strong>8</strong> of 25
</div> -->
</div>

    <a href="/page/create" class="md-btn md-fab m-b-sm blue" style="position: fixed; right: 15px; bottom: 10px;" data-turbolinks="false"><i class="fa fa-plus"></i></a>

<!-- Responsive Bootstrap Modal Popup -->





@endsection


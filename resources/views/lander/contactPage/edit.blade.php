@extends('lander.builder')
@section('save', 'updatePage()')
@section('content')
{!!$data->content!!}


@endsection
@section('script')

<script>
    function updatePage(){
    var text = '';
    $('.click2edit').each(function( index ) {
      text=text+'<div class="click2edit">'+( $( this ).html() )+'</div>';
    });

      $.ajax({
    url: '/page/'+'{{$data->id}}',
    type: 'PUT',
    data: { content: text }
    })
      .done(function( msg ) {
        updateThumbnail();
        
      });
  }

  function updateSEOAttr(){
    var data = {
      title : $('#title').val(),
      description : $('#description').val(),
      url : $('#url').val(),
      head : $('#head').val(),
      body : $('#body').val()
    }

      $.ajax({
    url: '/page/'+'{{$data->id}}',
    type: 'PUT',
    data: data
    })
      .done(function( msg ) {
        
      });
  }

  function updateThumbnail(){
    html2canvas(document.body, {
  onrendered: function(canvas) {
    var lowQuality = canvas.toDataURL("image/jpeg", 0.1);

         $.ajax({
    url: '/page/'+'{{$data->id}}',
    type: 'PUT',
    data: {img : lowQuality}
    })
      .done(function( msg ) {
        
      });
  },
  height: 700
});
  }
  updateThumbnail();

</script>
@endsection
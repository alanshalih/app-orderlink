<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Contact Page Creator</title>

  <!-- Bootstrap Core CSS -->
  <link href="/stylish/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom CSS -->
  <!-- <link href="/stylish/css/stylish-portfolio.css?2" rel="stylesheet"> -->

  <!-- Custom Fonts -->
  <link href="/stylish/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
  <!-- Responsive Bootstrap Modal Popup Main Style Sheet -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">





  <link rel="stylesheet" href="/libs/pageBuilder/style.css?7">
  <link rel="stylesheet" href="/css/ContactPage/style.css">



</head>

<body >


  @yield('content')

  <div class="editbutton"  > 



    <a href="#" onclick="editMode(event)" tooltip="Edit Mode" class="buttons sm-btn  "><i id="edit-icon" style="padding-top: 3px"  class="fa  fa-pencil fa-fw fa-2x"></i></a>

    <a href="/icon-helper" target="_blank" class="buttons menu2 sm-btn" tooltip="Icon Helper"><i style="padding-top: 7px" class="fa fa-question fa-fw fa-2x "></i></a>

    <a href="#" data-toggle="modal" data-target="#seo-attr" class="buttons menu2 sm-btn" tooltip="SEO"><i style="padding-top: 7px" class="fa fa-cloud fa-fw fa-2x"></i></a>

    <a href="#"  tooltip="Simpan" class="buttons menu2 sm-btn" onclick="@yield('save')"><i id="export" style="padding-top: 3px"  class="fa  fa-save fa-fw fa-2x"></i></a>
    
    <a href="/page" class="buttons sm-btn menu2" tooltip="Back to List Page"><i style="padding-top: 7px" class="fa fa-home fa-fw fa-2x "></i></a>
<!-- 
  <a class="buttons" tooltip="Share" href="#"></a> -->
  <a  class="buttons md-btn " tooltip="Pengaturan"><i style="padding-top: 13px" class="fa fa-gears fa-fw fa-2x"></i></a>

</div>


<!-- jQuery -->
<script src="/stylish/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/stylish/js/bootstrap.min.js"></script>






<!-- codemirror: todo: extract codemirror to bower -->
<!-- codemirror: todo: extract codemirror to bower -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.21.0/codemirror.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/theme/monokai.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.21.0/codemirror.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/3.20.0/mode/xml/xml.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/2.36.0/formatting.min.js"></script>

<!-- summernote plugin -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.js"></script>
<!-- jsColor plugin -->
<script src="/libs/jsColor/jscolor.js?3"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="/libs/pageBuilder/pagebuilder.js?189"></script>

  @yield('script')



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Ganti Gambar</h4>
      </div>
      <div class="modal-body">
       <div class="form-group">
        <label for="bookId">Image</label>
        <input type="text" oninput="changeImage($('#bookId').val())" class="form-control" id="bookId" value="">
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <button type="button"   class="btn btn-primary">Save changes</button>
    </div>
  </div>
</div>
</div>

<div class="modal fade" id="modal-background" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Ganti Gambar</h4>
      </div>
      <div class="modal-body">
       <div class="form-group">
        <label for="gambar">Image</label>
        <input type="text" class="form-control" oninput="updateBackgroundImage(value)" id="gambar" value="">
      </div>

      <div class="form-group">
        <label class="ui-switch info m-t-xs m-r">
          <input type="checkbox" id="paralax" onclick="isParalax()"  >
          <i></i>
        </label>
      </div>

      <div class="form-group">
        <label for="color">Background Color</label>
        <input type="text" class="jscolor form-control" id="color"  onchange="updateBackgroundColor(this.jscolor)" >
      </div>



    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" onclick="closeModalWithoutSave()">Tutup Tanpa Disimpan</button>
      <button type="button" data-dismiss="modal"   class="btn btn-primary">Simpan Perubahan</button>
    </div>
  </div>
</div>
</div>

<!-- .modal -->
<div id="seo-attr" class="modal fade animate" data-backdrop="true">
  <div class="modal-dialog" id="animate">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">SEO Stuff</h5>
      </div>
      <div class="modal-body p-lg">
        <div class="form-group">
          <label for="title">title</label>
          <input type="text" id="title" class="form-control" value="{{$data->title ?? ''}}">
        </div>
        <div class="form-group">
          <label for="description">deskripsi</label>
          <input type="text" id="description" class="form-control" value="{{$data->description ?? ''}}">
        </div>
         <div class="form-group">
          <label for="url">url</label>
          <input type="text" id="url" class="form-control" value="{{$data->url ?? ''}}">
        </div>
        <div class="form-group">
          <label for="head">sisipkan script sebelum <&#47;head></label>
          <textarea  id="head" class="form-control" value="{{$data->head ?? ''}}"></textarea>
        </div>
        <div class="form-group">
          <label for="body">sisipkan script sebelum <&#47;body></label>
          <textarea id="body" class="form-control" value="{{$data->body ?? ''}}"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn danger p-x-md" onclick="updateSEOAttr()" data-dismiss="modal">Simpan</button>
      </div>
    </div><!-- /.modal-content -->
  </div>
</div>
<!-- / .modal -->
<form id="simpan-form" action="/page" method="POST" style="display: none;">
  {{ csrf_field() }}
  <input type="text" id="simpan-data" name="data">
  <input type="text" id="simpan-data" name="title" value="A Page by {{Auth::user()->name}}">
</form>



</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Contact Page Creator</title>

  <!-- Bootstrap Core CSS -->
  <link href="/stylish/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom CSS -->
  <!-- <link href="/stylish/css/stylish-portfolio.css?2" rel="stylesheet"> -->

  <!-- Custom Fonts -->
  <link href="/stylish/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
  <!-- Responsive Bootstrap Modal Popup Main Style Sheet -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

  <link rel="stylesheet" href="/css/ContactPage/style.css">



</head>

<body >
{!!$getcampaign->content!!}

</body>

</html>

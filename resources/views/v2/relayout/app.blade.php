<!DOCTYPE html>
<html>
<head>
       <link rel="apple-touch-icon" sizes="76x76" href="/v2/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
      <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Orderlink - Tracking and List Building Tool</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="/v2/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <!-- <link href="/v2/css/material-dashboard.css?2" rel="stylesheet" /> -->
    <link rel="stylesheet" href="{{elixir('css/app.css')}}">
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <!-- <link href="/v2/css/app.css?12" rel="stylesheet" /> -->
	

    <!--     Fonts and icons     -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-icons/3.0.1/iconfont/material-icons.min.css">
    <!-- <style src="https://unpkg.com/vue-multiselect@2.0.0-beta.14/dist/vue-multiselect.min.css"></style> -->
    <!-- <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" /> -->
        <style> 

    @media screen and (min-width: 1200px) {
/*        #pill1{
                width: 100%;
    height: 64vh;

  position: relative;
  overflow: auto;
        }*/

        .timeline{
                        width: 100%;
    height: 85vh;

  position: relative;
  overflow: auto;
        }

        html, body {margin: 0; height: 100%; overflow: hidden}
    }

    @media screen and (max-width: 768px) {
            .small-text{
                font-size: 12px;
            }
    }
   
    </style>
</head>
<body>
<div  class="wrapper">
        <div class="sidebar" data-active-color="orange" data-background-color="black" data-image="/v2/img/sidebar-1.jpg">
            <div class="logo">
                <a href="http://www.creative-tim.com" class="simple-text">
                    ORDERLINK

                </a>

            </div>
            <div class="logo logo-mini">
                <a href="http://www.creative-tim.com" class="simple-text">
                    OL
                </a>
            </div>
            
            <div class="sidebar-wrapper">
                <div class="user">
                    <div class="photo">
                        <img src="/v2/img/faces/avatar.jpg" />
                    </div>
                    <div class="info">
                        <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                            Maulana Shalihin
                            <b class="caret"></b>
                        </a>
                        <div class="collapse" id="collapseExample">
                            <ul class="nav">
                                <li>
                                    <a href="#">My Profile</a>
                                </li>
                                <li>
                                    <a href="#">Edit Profile</a>
                                </li>
                                <li>
                                    <a href="#">Settings</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <ul class="nav">
                    <li class="">
                        <a href="/home">
                            <i class="fa fa-exchange"></i>
                            <p>Ke Orderlink v1</p>
                        </a>
                         
                    </li>
                    <li class="active">
                        <a href="/v2/orderlink">
                            <i class="fa fa-shopping-cart"></i>
                            <p>Orderlink</p>
                        </a>
                         
                    </li>
                </ul>
            </div>

        </div>

        
        <div class="main-panel">

            <nav class="navbar navbar-inverse navbar-absolute" style="border-radius: 0">
                <div class="container-fluid">
                    <div class="navbar-minimize">
                        <button id="minimizeSidebar" class="btn btn-round btn-warning btn-just-icon">
                            <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                            <i class="material-icons visible-on-sidebar-mini">view_list</i>
                        </button>
                    </div>
                    <div class="navbar-header ">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> Orderlink </a>
                    </div>
                    <!-- 
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">dashboard</i>
                                    <p class="hidden-lg hidden-md">Dashboard</p>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">notifications</i>
                                    <span class="notification">5</span>
                                    <p class="hidden-lg hidden-md">
                                        Notifications
                                        <b class="caret"></b>
                                    </p>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="#">Mike John responded to your email</a>
                                    </li>
                                    <li>
                                        <a href="#">You have 5 new tasks</a>
                                    </li>
                                    <li>
                                        <a href="#">You're now friend with Andrew</a>
                                    </li>
                                    <li>
                                        <a href="#">Another Notification</a>
                                    </li>
                                    <li>
                                        <a href="#">Another One</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">person</i>
                                    <p class="hidden-lg hidden-md">Profile</p>
                                </a>
                            </li>
                            <li class="separator hidden-lg hidden-md"></li>
                        </ul>
               
                    </div> -->
                </div>
            </nav>
           <div id="appv2">
               @yield('content')
           </div>
          
        </div>
        <div class="floating">
        	<button class="btn btn-primary btn-round btn-fab " data-toggle="modal" data-target="#v2ModalNew" style="position: fixed; bottom: 4vh; right: 4vw;">
                                        <i class="material-icons">add</i>
                                    </button>
        </div>
    </div>

<script src="/v2/js/jquery-3.1.1.min.js" type="text/javascript"></script>
<!-- <script src="/v2/js/jquery-ui.min.js" type="text/javascript"></script> -->
<script src="/v2/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/v2/js/material.min.js" type="text/javascript"></script>
<script src="/v2/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<!-- Forms Validations Plugin -->
<!-- <script src="/v2/js/jquery.validate.min.js"></script> -->
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="/v2/js/moment.min.js"></script>
<!--  Charts Plugin -->
<script src="/v2/js/chartist.min.js"></script>
<!--  Plugin for the Wizard -->
<!-- <script src="/v2/js/jquery.bootstrap-wizard.js"></script> -->
<!--  Notifications Plugin    -->
<script src="/v2/js/bootstrap-notify.js"></script>
<!-- DateTimePicker Plugin -->
<script src="/v2/js/bootstrap-datetimepicker.js"></script>
<!-- Vector Map plugin -->
<!-- <script src="/v2/js/jquery-jvectormap.js"></script> -->
<!-- Sliders Plugin -->
<!-- <script src="/v2/js/nouislider.min.js"></script> -->
<!--  Google Maps Plugin    -->
<!-- <script src="https://maps.googleapis.com/maps/api/js"></script> -->
<!-- Select Plugin -->
<script src="/v2/js/jquery.select-bootstrap.js"></script>
<!--  DataTables.net Plugin    -->
<!-- <script src="/v2/js/jquery.datatables.js"></script> -->
<!-- Sweet Alert 2 plugin -->
<script src="/v2/js/sweetalert2.js"></script>
<!--    Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<!-- <script src="/v2/js/jasny-bootstrap.min.js"></script> -->
<!--  Full Calendar Plugin    -->
<!-- <script src="/v2/js/fullcalendar.min.js"></script> -->
<!-- TagsInput Plugin -->
<script src="/v2/js/jquery.tagsinput.js"></script>
<!-- Material Dashboard javascript methods -->
<script src="/v2/js/material-dashboard.js"></script>

<script src="{{elixir('js/appv2.js')}}"></script>

<script>    
// if($( window ).width() > 1200){
//     $('.timeline').perfectScrollbar();
//     $('#pill1').perfectScrollbar();
//   }




</script>

@yield('create-button')
</body>
</html>
@extends('v2.relayout.app')

@section('content')

     <div class="container">
                    <h3>Manage Listings</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="orange">
                                    <i class="material-icons">contacts</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">Horizontal d</h4>
                                    <form  action="/v2r/products" method="POST">
                                    {{ csrf_field() }}
                                        <div class="col-md-6">  
                                        <div class="form-group label-floating">
                                            <label class="control-label">Nama Produk</label>
                                            <input type="text" name="title" class="form-control">
                                        </div>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Harga</label>
                                            <input type="number" name="price" class="form-control">
                                        </div>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Berat (gram)</label>
                                            <input name="weight" type="number" value="1000" class="form-control">
                                        </div>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Thumbnail</label>
                                            <input type="text" name="thumbnail" class="form-control">
                                        </div>
                                        
                                       <!-- <legend>Switches</legend> -->
                                       <!-- <br> -->
                                           <!--  <div class="togglebutton">
                                                <label>
                                                    <input type="checkbox" checked> Tampilkan di Marketplace
                                                </label>
                                            </div> -->

                                        <v2-productlocation :user="{{$user}}"></v2-productlocation>
										
                                        </div>
                                        <div class="col-md-6">  
                                        <div class="form-group label-floating">
                                            <label class="control-label">Deskripsi Singkat (optional)</label>
                                            <input name="short_description" type="text" class="form-control">
                                        </div>
                                        
                                        <div class="form-group label-floating">
                                            <label class="control-label">Deskripsi Produk (optional)</label>
                                            <textarea name="long_description" class="form-control" id="" cols="30" rows="15"></textarea>
                                        </div>
                                        
                                            <div class="pull-right">
                                                <div class="form-group form-button">
                                                    <button type="submit" class="btn btn-fill btn-primary">Submit Produk</button>
                                                </div>
                                            </div>
                                        </div>

                                        <input type="hidden" name="status" value="submit">
                                        <input type="hidden" name="author_id" value="{{Auth::id()}}">
                                        
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                     
                    </div>

                </div>
@endsection
@extends('v2.relayout.app')

@section('content')


 <div class="container-fluid">
   <br>
        <div class="row"> @foreach($products as $product)
       <div class="col-md-4">
          
            <div class="card card-product">
                                <div class="card-image" data-header-animation="true">
                                    <a href="#pablo">
                                        <img class="img" src="{{$product->thumbnail ?? '/v2/img/image_placeholder.jpg'}}">
                                    </a>
                                </div>
                                <div class="card-content">
                                    <div class="card-actions">
                                        <button type="button" class="btn btn-danger btn-simple fix-broken-card">
                                            <i class="material-icons">build</i> Fix Header!
                                        </button>
                                        <button type="button" class="btn btn-default btn-simple" rel="tooltip" data-placement="bottom" title="View">
                                            <i class="material-icons">art_track</i>
                                        </button>
                                        <button type="button" class="btn btn-success btn-simple" rel="tooltip" data-placement="bottom" title="Edit">
                                            <i class="material-icons">edit</i>
                                        </button>
                                        <button type="button" class="btn btn-danger btn-simple" rel="tooltip" data-placement="bottom" title="Remove">
                                            <i class="material-icons">close</i>
                                        </button>
                                    </div>
                                    <h4 class="card-title">
                                        <a href="#pablo">{{$product->title}}</a>
                                    </h4>
                                    <div class="card-description">
                                        {{$product->short_description}}
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="price">
                                        <h4>Rp.{{$product->price}}</h4>
                                    </div>
                                    <div class="stats pull-right">
                                        <!-- <p class="category"><i class="material-icons">place</i> {{$product->province_id}}</p> -->
                                    </div>
                                </div>
                            </div>
          
       </div> @endforeach
    </div>
   </div>
@endsection
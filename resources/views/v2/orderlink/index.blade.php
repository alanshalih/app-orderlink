@extends('v2.layouts.app')
@section('create-button')
<button  id="button-trigger-new" class="btn btn-just-icon btn-round btn-primary" data-toggle="modal"  data-target="#create-new"  >
 <i class="fa fa-plus"></i>
</button>
@endsection
@section('content')
 <orderlink :all="{{$all}}" :pixels="{{$pixels}}" :googles="{{$googles}}" :domains="{{$domains}}"></orderlink>
@endsection
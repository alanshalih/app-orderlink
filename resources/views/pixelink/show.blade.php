@extends('layouts.app')
@section('title','Pixelink')
@section('content')
@if (session('status'))
<script>
  var sites = {!! json_encode(session('status')) !!};
  toastr.success(sites);

</script>
@endif
{{Carbon\Carbon::setLocale('id')}}
<div class="app-body-inner" >
  <div class="row-col">
    <div class="col-xs-5 w-xl modal fade aside aside-sm black" id="list">
      <div class="row-col lt">
        <!-- header -->
        <div class="p-a">
          <form>
            <div class="input-group">
              <input type="text" class="form-control form-control-sm" placeholder="Search" required="">
              <span class="input-group-btn">
                <button class="btn btn-default btn-sm no-shadow" type="button"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
        </div>
        <!-- / -->
        <!-- flex content -->
        <div class="row-row">
          <div class="row-body scrollable hover">
            <div class="row-inner">
              <!-- left content -->
              <div class="list inset">
                @foreach ($grabLink as $link)
                @if($link->id === $getOneLink->id)
                <div class="list-item info">
                 @else
                 <div class="list-item">
                   @endif
                   <div class="list-left">
                    <span class="w-40 avatar circle indigo">
                     @if(($link->title))
                     {{strtoupper($link->title[0])}}
                     @else
                     {{strtoupper($link->url[0])}}
                     @endif
                   </span>
                 </div>
                 <div class="list-body">
                  <span class="pull-right text-xs text-muted">{{\Carbon\Carbon::createFromTimeStamp(strtotime($link->created_at))->diffForHumans()}}</span>
                  <div class="item-title">
                   @if(($link->title))
                   <a href="/pixelink/{{$link->id}}?page={{$grabLink->currentPage()}}" class="_500">{{$link->title}}</a>
                   @else
                   <a href="/pixelink/{{$link->id}}?page={{$grabLink->currentPage()}}" class="_500">{{$link->url}}</a>
                   @endif
                 </div>
                 <small class="block text-muted text-ellipsis">
                  {{$link->alias}}
                  <span class="pull-right text-xs text-muted"><i class="fa fa-bar-chart"></i> {{Redis::ZSCORE("pixelink", $link->id) ?? 0}}</span>
                </small>
              </div>
            </div>
            @endforeach
          </div>
          <!-- / -->
        </div>
      </div>
    </div>
    <!-- / -->
    <!-- footer -->
    <div class="p-x-md p-y">
      <div class="btn-group pull-right">
        <a href="{{$grabLink->previousPageUrl()}}" class="btn btn-xs white"><i class="fa fa-fw fa-angle-left"></i></a>
        <a href="{{$grabLink->nextPageUrl()}}" class="btn btn-xs white"><i class="fa fa-fw fa-angle-right"></i></a>
      </div>
      <span class="text-sm text-muted">menampilkan {{$grabLink->count()}} dari {{$grabLink->total()}}</span>
    </div>
    <!-- / -->
  </div>
</div>
<div class="col-xs-7 bg" id="detail">
  <div class="row-col">
    <!-- header -->
    <div class="white b-b bg">
      <div class="navbar">
        <!-- nabar right -->
        <ul class="nav navbar-nav pull-right m-l">
          <a class="nav-link " data-toggle="modal" data-target="#modal-new">
            <span class="btn btn-sm info white">Bikin Pixelink Baru <i class="fa fa-plus"></i></span>
          </a>
        </ul>
        <!-- / navbar right -->
        <a data-toggle="modal" data-target="#list" data-ui-modal class="navbar-item pull-left hidden-md-up">
          <span class="btn btn-sm btn-icon white">
            <i class="fa fa-list"></i>
          </span>
        </a>
        <!-- link and dropdown -->
        <ul class="nav navbar-nav">

              <li class="nav-item b-l p-l">
                <a class="nav-link text-muted no-border" data-toggle="modal"  data-target="#modal-edit" data-placement="bottom" title="Edit">
                <span class="nav-text"><i class="fa fa-fw fa-edit"></i>edit</span>
                </a>
              </li>

              <li class="nav-item">
                <a onclick="event.preventDefault();
                document.getElementById('delete-link').submit();" class="nav-link text-muted" data-toggle="tooltip" data-placement="bottom" title="Delete">
                <span class="nav-text"><i class="fa fa-fw fa-trash-o"></i></span>
              </a>

            </li>

          </ul>
          <form id="delete-link" action="/pixelink/{{$getOneLink->id}}" method="POST" style="display: none;">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
          </form>


          <!-- / link and dropdown -->
        </div>
      </div>
      <!-- / -->
      <!-- flex content -->
      <div class="row-row">
        <div class="row-body">
          <div class="row-inner">
            <!-- mail content -->
            <div class="padding">

              @if(($getOneLink->title))
              <h2 class="m-b _600">{{$getOneLink->title}}</h2>
              @else
              <h2 class="m-b _600">{{$getOneLink->url}}</h2>
              @endif
              
              <h6 class="text-info">

              <a id="alias" class="m-r" href="{{$getOneLink->alias}}" target="_blank">{{$getOneLink->alias}}</a>  
              <div class="hidden-sm-up"><br></div>
              <button class="btn btn-xs info" data-clipboard-action="copy" data-clipboard-target="a#alias"><i class="fa fa-copy"></i> Copy 

              </button> <!-- <a href="https://www.facebook.com/sharer/sharer.php?u={{$getOneLink->alias}}" target="_blank" class="btn btn-xs info">Share 
              <i class="fa fa-facebook"></i>
            </a>  -->
            <a href="#" data-toggle="modal" data-target="#share">
            <span class="btn btn-xs info white"><i class="fa fa-facebook"></i> share</span>
          </a>
            </h6>
          <p style="margin: 0; padding: 0">link asli  <a href="{{$getOneLink->url}}">{{$getOneLink->url}}</a></p>
      

          <div class="p-y b-t">

           <h3 class="text-info"><i class="fa fa-bar-chart"></i>  {{Redis::ZSCORE("pixelink", $getOneLink->id) ?? 0}} KLIK</h3>

           <span class="text-xs text-muted">dibikin {{\Carbon\Carbon::createFromTimeStamp(strtotime($getOneLink->created_at))->diffForHumans()}}</span>
         </div>
         <div class="b-b b-primary nav-active-info">
          <ul class="nav nav-tabs">
            <li class="nav-item">
              <a class="nav-link active" href="#" data-toggle="tab" data-target="#tab4">Statistik</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#" data-toggle="tab" data-target="#tab5">Advanced</a>
            </li>
          </ul>
        </div>
        <div class="tab-content p-a m-b-md">
          <div class="tab-pane animated fadeIn text-muted active" id="tab4">
                     <div>
        
                    <hit-chart :Klik="{{$totalHit}}" :Unik="{{$unikHit}}" :Campaign_id="{{$getOneLink->id}}" model="pixelink"></hit-chart>
                 

                    <div class="box">
                      <div class="box-header b-b">
                        <h3>Browser & Platform Statistik</h3>
                      </div>
                      <div>
                        <div class="row-col">
                          <div class="col-md-6">
<platform-chart :data="{{$browserChart}}" id="Browser"></platform-chart>

                       </div>
                         <div class="col-md-6">
<platform-chart :data="{{$platformChart}}" id="Platform"></platform-chart>

                       </div>
                     </div>
                   </div>
                      
                   </div>


        </div>
          </div>
          <div class="tab-pane animated fadeIn text-muted" id="tab5">
            <advance-tab :domains="{{$cloakdomain}}" :getcampaign="{{$getOneLink}}"></advance-tab>

          </div>
        </div>


        <div>
          
        </div>
      </div>
      <!-- / -->
    </div>
  </div>
</div>
<!-- / -->
</div>
</div>
</div>
</div>

<div class="modal fade " id="modal-new" data-backdrop="true">
  <div class="modal-right w-xl white b-l">
    <div class="row-col">
      <a data-dismiss="modal" class="pull-right text-muted text-lg p-a-sm m-r-sm">&times;</a>
      <div class="p-a b-b">
        <span class="h5">Bikin Baru</span>
      </div>
      <div class="row-row">
        <div class="row-body">
          <div class="row-inner">
            <div class="list-group no-radius no-borders">


              <form action="/pixelink" method="POST" class="form-horizontal">
                {{ csrf_field() }}
                <create-pixelink-form :domains="{{$domains}}">
                  @if(count($pixel)>0)
                  <pixel-input      
                  :pixel="{{$pixel}}" 
                  :selectedp="{{$pixel[0]->id}}"
                  eventselected="fbq('track', 'ViewContent');">
                </pixel-input>

                @else
                <pixel-input-default></pixel-input-default>
                @endif


                @if(count($google)>0)
                <google-input 
                :google="{{$google}}" :selectedg="{{$google[0]->id}}"  
                ></google-input>
                @else
                <google-input-default></google-input-default>
                @endif
              </create-pixelink-form>


            </form>

          </div>
        </div>
      </div>
    </div>

  </div>
</div>
</div>
<div class="modal fade " id="modal-edit" data-backdrop="true">
  <div class="modal-right w-xl white b-l">
    <div class="row-col">
      <a data-dismiss="modal" class="pull-right text-muted text-lg p-a-sm m-r-sm">&times;</a>
      <div class="p-a b-b">
        <span class="h5">Edit Campaign</span>
      </div>
      <div class="row-row">
        <div class="row-body">
          <div class="row-inner">
            <div class="list-group no-radius no-borders">


              <form action="/pixelink/{{$getOneLink->id}}" method="POST" class="form-horizontal">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <edit-pixelink :domains="{{$domains}}" :pixelink="{{$getOneLink}}">
                 @if(count($pixel)>0)
                 @if(isset($getOneLink->pixel_id))
                 @if(isset($getOneLink->pixel_event))
                 <pixel-input      
                 :pixel="{{$pixel}}" 
                 :selectedp="{{$getOneLink->pixel_id}}"
                 eventselected="{{$getOneLink->pixel_event}}">
               </pixel-input>
               @else
               <pixel-input      
                 :pixel="{{$pixel}}" 
                 :selectedp="{{$getOneLink->pixel_id}}"
                 eventselected="fbq('track', 'ViewContent');">
               </pixel-input>
               @endif
               @else
               <pixel-input      
                 :pixel="{{$pixel}}" 
                 :selectedp="{{$pixel[0]->id}}"
                 eventselected="fbq('track', 'ViewContent');">
               </pixel-input>
               @endif
               @else
               <pixel-input-default></pixel-input-default>
               @endif

               @if(count($google)>0)
                @if(isset($getOneLink->google_id))
               <google-input 
               :google="{{$google}}" :selectedg="{{$getOneLink->google_id}}"   
               ></google-input>
                @else
               <google-input 
               :google="{{$google}}" :selectedg="{{$google[0]->id}}"    
               ></google-input>
               @endif
               @else
               <google-input-default></google-input-default>
               @endif
              </edit-pixelink>


            </form>

          </div>
        </div>
      </div>
    </div>

  </div>
</div>
</div>


<div class="modal fade " id="share" data-backdrop="true">
  <div class="modal-right w-xl white b-l">
    <div class="row-col">
      <a data-dismiss="modal" class="pull-right text-muted text-lg p-a-sm m-r-sm">&times;</a>
      <div class="p-a b-b">
        <span class="h5">Share</span>
      </div>
      <div class="row-row">
        <div class="row-body">
          <div class="row-inner">
            <div class="list-group no-radius no-borders">

                <facebook-share just_token="{{Auth::user()->bitly_access_token}}" url="{{$getOneLink->url}}" alias="{{$getOneLink->alias}}"></facebook-share>  

          </div>
        </div>
      </div>
    </div>

  </div>
</div>
</div>





@endsection
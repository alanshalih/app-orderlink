<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>{{$getcampaign->title}}</title>
	@if(isset($getcampaign->cloak_link))
	@if(!($getcampaign->cloak_link))
	<meta property="og:url" content="{{$getcampaign->url}}">
	@endif
	@else
	<meta property="og:url" content="{{$getcampaign->url}}">
	@endif
	<!-- <link rel="stylesheet" href="/css/loader/css/style.css"> -->

	<!-- Facebook Pixel Code -->
	     @if(isset($getcampaign->pixel->pixel_id))
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '{{$getcampaign->pixel->pixel_id}}');
	fbq('track', 'PageView');


	</script>

		@if(isset($getcampaign->pixel_event))
	<script>
	{!! $getcampaign->pixel_event !!}


	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id={{$getcampaign->pixel->pixel_id}}&ev=PageView&noscript=1"
	/></noscript>
	@else
		<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id={{$getcampaign->pixel->pixel_id}}&ev=PageView&noscript=1"
	/></noscript>
	@endif

	@endif

	<!-- End Facebook Pixel Code -->
	<style>
		
		* {margin: 0; padding: 0;}

.loader-fb {
  top: calc(45% - 5px);
  left: calc(50% - 5px);
  position: absolute !important;
}

#progress-wrap{
	  top: calc(58%);
   left: calc(50% - 5px);
  position: absolute !important;
 
}

.load-text{
	font-family: arial;
	margin-left: -50px;
}

/* Loader with three blocks */
.loader-fb, .loader-fb:before, .loader-fb:after {
  position: relative;
  display: inline-block;
  width: 20px;
  height: 50px;
  background-color: rgba(215,230,240,0.9);
  border: 1px solid rgb(215,230,240);
  z-index: 100000;
  content: ' ';
  margin-left: -5px;
  margin-right: -9px;
}
.loader-fb:before {
  top: -11px;
  left: -100%;
  animation: loading-fb .8s cubic-bezier(.4,.5,.6,1) infinite;
}
.loader-fb {
  animation: loading-fb-main .8s cubic-bezier(.4,.5,.6,1) .2s infinite;
}
.loader-fb:after {
  top: -11px;
  right: -100%;
  margin-top: 50%;
  animation: loading-fb .8s cubic-bezier(.4,.5,.6,1) .4s infinite;
}
@keyframes loading-fb {
  from {
    transform: scaleY(1.4);
    background-color: rgba(55,114,171,0.9);
    border: 1px solid rgb(55,114,171);
  }
}
@keyframes loading-fb-main {
  from {
    padding-top: 10px;
    padding-bottom: 10px;
    margin-top: -10px;
    background-color: rgba(55,114,171,0.9);
    border: 1px solid rgb(55,114,171);
  }
}

	</style>

</head>
<body>
  <div class="loader-fb"></div>  

<div id="progress-wrap" >
 <div class="load-text">
 	 <span>Loading...</span>
  <span id="progress">100%</span>
 </div>
</div>
  
<div class="text">
  <!-- <span>...mohon tunggu...</span>
  <span id="progress">100%</span> -->
</div>
@if(isset($getcampaign->google->google_id))
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = {{$getcampaign->google->google_id}};
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/{{$getcampaign->google->google_id}}/?guid=ON&amp;script=0"/>
	</div>
	</noscript>
@endif
	<script>
	
	var counter = document.getElementById("progress");

	function changeText(data){
	  counter.innerHTML = data;
	}

	var max = 1000;
	var init = 0;
	var add = 200;

	   var getcampaign = {!! json_encode($getcampaign) !!};

	setTimeout(function(){
    	window.location = getcampaign.url;
    },max)


	function loop(){
	  setTimeout(function(){ 
	  init=init+add;
	   
	  changeText((init/(max*2))*100+'%');
	   if(init<(max*2)){
	     loop();
	   }
	}, add);
	}

	loop();


	</script>

	
</body>
</html>
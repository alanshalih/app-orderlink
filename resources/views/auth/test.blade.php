@extends('auth.layouts')

@section('content')

  <div class="form">
  <img src="https://res.cloudinary.com/ffsandi/image/upload/v1484557360/OrderLink/nama_program_di_menu.png" width="60%" alt="Orderlink Logo">
  <p>Silakan Masuk, Awali dengan Bismillah.</p>
  <br>

  <a href="/redirect" class="btn btn-lg btn-primary"><i class="fa fa-facebook" style="padding-right: 10px; border-right: solid 1px white"></i>    <span style="margin-left: 10px">Login with Facebook</span></a>
 <span style="margin-top: 20px">- OR -</span>

   <form class="form-horizontal" role="form" method="POST" action="/signin">
                        {{ csrf_field() }}
    <input type="email"  placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
              @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
      
          <input type="password"  placeholder="password"  name="password" required>

              @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
       <div class="m-b-md">        
            <label class="md-check">
              <input type="checkbox" name="remember"><i class="primary"></i> Remember me
            </label>
          </div>
      <button type="submit" class="login">Sign in</button>
    </form>
       <div class="m-y">
          <a href="{{ url('/password/reset') }}" class="_600">Kelupaan Password?</a>
        </div>
  </div>

  


@endsection



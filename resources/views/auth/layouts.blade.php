
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Orderlink - All in One Link Solution</title>
  <meta name="description" content="Kini List Building, Link Tracking & Retargeting semakin mudah" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- for ios 7 style, multi-resolution icon of 152x152 -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
  <link rel="apple-touch-icon" href="images/logo.png">
  <meta name="apple-mobile-web-app-title" content="Flatkit">
  <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="shortcut icon" sizes="196x196" href="images/logo.png">
  
  <!-- style -->
  <link rel="stylesheet" href="/css/animate.css/animate.min.css" type="text/css" />

  <link rel="stylesheet" href="/css/glyphicons/glyphicons.css" type="text/css" />
  <link rel="stylesheet" href="/css/font-awesome/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="/css/bootstrap/dist/css/bootstrap.min.css" type="text/css" />  
          <link rel="stylesheet" href="/css/Auth/style.css?7">
    <link rel="stylesheet" href="/libs/button-loader/buttonLoader.css?id=1.0">


</head>
<body>
  <div class="box">
   <div class="qoute hidden-md-down">
      <img class="img-responsive" src="https://res.cloudinary.com/ffsandi/image/upload/v1484572375/OrderLink/login_image_orderlink.png" alt="" />
      <br>
      <p>Kini List Building, Link Tracking & Retargeting semakin mudah </p>
    <span class="fadeIn wait-1s">- Orderlink.in -</span>
  </div>
        @yield('content')
  </div>




<!-- build:js scripts/app.min.js -->
<!-- jQuery -->
  <script src="/libs/jquery/dist/jquery-3.1.1.min.js"></script>

  <script src="/libs/button-loader/jquery.buttonLoader.js?id=1/0"></script>
    <script src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>


  <script>

 
    $(document).on('click', '.signup', function() { 

    var btn = $(this);
            $(btn).buttonLoader('start');
   

    });
     $(document).on('click', '.login', function() { 

    

  


  </script>

  @yield('footer')
  
  

<!-- endbuild -->
</body>
</html>

@extends('auth.layouts')

@section('content')
<!-- ############ LAYOUT START-->


  <div class="form">
          @if (session('status'))
            <div class="alert alert-danger">
                {{ session('status') }}
            </div>
        @endif
    <img src="https://res.cloudinary.com/ffsandi/image/upload/v1484557360/OrderLink/nama_program_di_menu.png" width="60%" alt="Orderlink Logo">
    <br>

<!--       <a href="/redirect" class="btn btn-lg btn-primary"><i class="fa fa-facebook" style="padding-right: 10px; border-right: solid 1px white"></i>    <span style="margin-left: 10px">Sign up with Facebook</span></a>
>>>>>>> dev-master
 <span style="margin-top: 20px">- OR -</span> -->
  <div class="alert alert-danger" id="errors" style="display: none"></div>
    <form name="form" method="POST" action="/register/v2">
                        {{ csrf_field() }}



                        <input id="lisensi" type="text" placeholder="Kode Lisensi Anda" onchange  name="lisensi"  required >
     
                    <input id="name" type="text" placeholder="Nama Panggilan"  name="name" value="{{ old('name') }}" required >

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif

                                       <input id="email" type="email" placeholder="Email" class="md-input" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif


      
           <input id="password" type="password" placeholder="Password" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                                       <input id="password-confirm" type="password" class="md-input" name="password_confirmation" placeholder="Konfirmasi Password" required>
      <!--  <div class="m-b-md text-sm">
            <span class="text-muted">By clicking Sign Up, I agree to the</span> 
            <a href="#">Terms of service</a> 
            <span class="text-muted">and</span> 
            <a href="#">Policy Privacy.</a>
          </div> -->
          <div class="hidden">
           
          </div>
          <input type="hidden" value="{{$status}}" name="status">
          <input type="hidden" name="result" id="result">
      <button id="submit-btn" type="submit" class="signup">Sign Up</button>
    </form>
       <div class="text-center">
          <div>Sudah Punya Akun? <a href="/signin" class="text-primary _600">Sign in</a></div>

        </div>
   
  </div>





<!-- ############ LAYOUT END-->

@endsection

@section('footer')
    <script>
  function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};


$('#lisensi').keyup(debounce(function(){
        var $this=$(this);
        ( $this.val() );

        $.post( "https://orderlink.in/check/license", { key: $this.val(), email: "{{$request->email}}", string: "{{$status}}" }, function( data ) {
    var data  = jQuery.parseJSON(data);
    $('#email').val(data.email);
    $('#result').val(data.result);
    if(data.content){
      $('#errors').html(data.content).show();
      // $('#submit-btn').hide();
    }
    else{
      // $('#submit-btn').show();
      $('#errors').hide();
    }
    

});
        
    },500));

$( "form" ).submit(function( event ) {
  if ( $( "#result" ).val() === "true" ) {
    // $( "span" ).text( "Validated..." ).show();
    return;
  }
 
  // $( "span" ).text( "Not valid!" ).show().fadeOut( 1000 );
  event.preventDefault();
});

    
  </script>
@endsection
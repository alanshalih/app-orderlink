@extends('auth.layouts')

@section('content')
<!-- ############ LAYOUT START-->


  <div class="form">
          @if (session('status'))
            <div class="alert alert-danger">
                {{ session('status') }}
            </div>
        @endif
    <img src="https://res.cloudinary.com/ffsandi/image/upload/v1484557360/OrderLink/nama_program_di_menu.png" width="60%" alt="Orderlink Logo">
    <br>

<!--       <a href="/redirect" class="btn btn-lg btn-primary"><i class="fa fa-facebook" style="padding-right: 10px; border-right: solid 1px white"></i>    <span style="margin-left: 10px">Sign up with Facebook</span></a>
>>>>>>> dev-master
 <span style="margin-top: 20px">- OR -</span> -->
    <form name="form" method="POST" action="/register">
                        {{ csrf_field() }}
     
                    <input id="name" type="text" placeholder="Nama Panggilan"  name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif

                                       <input id="email" type="email" placeholder="Email" class="md-input" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif


      
           <input id="password" type="password" placeholder="Password" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                                       <input id="password-confirm" type="password" class="md-input" name="password_confirmation" placeholder="Konfirmasi Password" required>
      <!--  <div class="m-b-md text-sm">
            <span class="text-muted">By clicking Sign Up, I agree to the</span> 
            <a href="#">Terms of service</a> 
            <span class="text-muted">and</span> 
            <a href="#">Policy Privacy.</a>
          </div> -->
          <div class="hidden">
            @if(isset($upline))
            <input type="hidden" name="upline" value="{{$upline->id}}">
            @endif
          </div>
      <button type="submit" class="signup">Sign Up</button>
    </form>
       <div class="text-center">
          <div>Sudah Punya Akun? <a href="/signin" class="text-primary _600">Sign in</a></div>

        </div>
        @if(isset($upline))

        <span>anda direferensikan oleh : {{$upline->name}}</span>
        @endif
  </div>



<!-- ############ LAYOUT END-->

@endsection
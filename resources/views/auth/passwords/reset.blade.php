@extends('auth.layouts')
@section('content')

                <div class="form">
                    <form class="form-horizontal" role="form" method="POST" action="/password/reset">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

    

                                <input id="email" type="email" placeholder="email" name="email" value="{{ $email or old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif



         
                                <input id="password" type="password" placeholder="password baru"  name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

         
                                <input id="password-confirm" type="password" placeholder="konfirmasi password"  name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
         

                 
                                <button type="submit" class="login">
                                    Reset Password
                                </button>
        
                        </div>
                    </form>
                </div>
@endsection

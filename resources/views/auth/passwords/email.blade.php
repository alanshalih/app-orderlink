@extends('auth.layouts')

<!-- Main Content -->
@section('content')

  <div class="form">
       @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
   <h2>Lupa Password...</h2>
 

   
                    <form class="form-horizontal" role="form" method="POST" action="/password/email">
                        {{ csrf_field() }}
       <input id="email" type="email" placeholder="Masukan Email Anda" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

      <button type="submit" class="login">Reset Password</button>
    </form>

  </div>


@endsection

@extends('layouts.app')
@section('title','Contentlink')
@section('content')
 @if (session('status'))
  <script>
  var sites = {!! json_encode(session('status')) !!};
  toastr.success(sites);

  </script>
@endif
<div class="app-body-inner">
  <div class="row-col">
    <div class="col-xs-5 w-xl modal fade aside aside-sm black" id="list">
      <div class="row-col lt">
        <!-- header -->
        <div class="p-a">
          <form>
            <div class="input-group">
              <input type="text" class="form-control form-control-sm" placeholder="Search" required="">
              <span class="input-group-btn">
              <button class="btn btn-default btn-sm no-shadow" type="button"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
        </div>
        <!-- / -->
        <!-- flex content -->
        <div class="row-row" style="display: flex;align-items: center; justify-content: center">
    
        </div>
        <!-- / -->
        <!-- footer -->
        <div class="p-x-md p-y">
          <div class="btn-group pull-right">
            <a href="#" class="btn btn-xs white"><i class="fa fa-fw fa-angle-left"></i></a>
            <a href="#" class="btn btn-xs white"><i class="fa fa-fw fa-angle-right"></i></a>
          </div>
          <span class="text-sm text-muted">Total: 0</span>
        </div>
        <!-- / -->
      </div>
    </div>
    <div class="col-xs-7 bg" id="detail">
      <div class="row-col">
        <!-- header -->
        <div class="white b-b bg">
          <div class="navbar">
            <!-- nabar right -->
            <ul class="nav navbar-nav pull-right m-l">
              @if(count($mycontentlink)>0)
              <a class="nav-link " data-toggle="modal" data-target="#create-new">
              <span class="btn btn-sm info white">Bikin Campaign Baru <i class="fa fa-plus"></i></span>
              </a>
              @else
              <a class="nav-link " href="/contentlink/template">
              <span class="btn btn-sm info white">Bikin Campaign Baru <i class="fa fa-plus"></i></span>
              </a>
              @endif
            </ul>
            <!-- / navbar right -->
            <a data-toggle="modal" data-target="#list" data-ui-modal class="navbar-item pull-left hidden-md-up">
            <span class="btn btn-sm btn-icon white">
            <i class="fa fa-list"></i>
            </span>
            </a>
            <!-- link and dropdown -->
          
    

                                     
            <!-- / link and dropdown -->
          </div>
        </div>
        <!-- / -->
        <!-- flex content -->
        <div class="row-row">
          <div class="row-body">
            <div class="row-inner">
              <!-- mail content -->
              <div class="row-row" style="display: flex; flex-direction: column; align-items: center; justify-content: center">
              <div class="alert alert-info text-center"><h6>bikin contentlink campaign pertama anda dengan klik</h6>  
              @if(count($mycontentlink)>0)
              <a  data-toggle="modal" data-target="#create-new">
              <span class="btn btn-sm info white">Bikin Campaign Baru <i class="fa fa-plus"></i></span>
              </a>
              @else
              <a  href="/contentlink/template">
              <span class="btn btn-sm info white">Bikin Campaign Baru <i class="fa fa-plus"></i></span>
              </a>
              @endif</div>
            

        </div>
            </div>
          </div>
        </div>
        <!-- / -->
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="create-new" data-backdrop="true">
  <div class="modal-right w-xl white b-l">
      <div class="row-col">
        <a data-dismiss="modal" class="pull-right text-muted text-lg p-a-sm m-r-sm">&times;</a>
        <div class="p-a b-b">
          <span class="h5">Contentlink</span>
        </div>
        <div class="row-row">
          <div class="row-body">
            <div class="row-inner">
              <div class="list-group no-radius no-borders">

                  <form action="/contentlink/campaign" method="POST" class="form-horizontal">
    {{ csrf_field() }}
        
      <contentlink-campaign :mycontentlink="{{$mycontentlink}}" :domains="{{$domains}}" :first="true">
           @if(count($pixel)>0)
          <pixel-input      
          :pixel="{{$pixel}}" 
          :selectedp="{{$pixel[0]->id}}">
          </pixel-input>
          @else
            <pixel-input-default></pixel-input-default>
          @endif
          
          @if(count($google)>0)
             <google-input 
            :google="{{$google}}" :selectedg="{{$google[0]->id}}"  
            ></google-input>
          @else
            <google-input-default></google-input-default>
          @endif
      </contentlink-campaign>
    </form>

              </div>
            </div>
          </div>
        </div>
      
      </div>
  </div>
</div>
@endsection
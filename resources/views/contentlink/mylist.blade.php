@extends('layouts.app')

@section('content')
@if (session('status'))
<script>
  var sites = {!! json_encode(session('status')) !!};
  toastr.success(sites);
</script>
@endif


<div class="padding">
  <div class="row">
   @foreach($userCL as $list)
   <div class="col-xs-6 col-sm-4 col-md-3">
    <div class="box p-a-xs">
      <a href="#"><img src="{{$list->cover}}" alt="" class="img-responsive"></a>
      <div class="p-a-sm">

        <h6>{{$list->title}}</h6>
        <div class="text-ellipsis" style="text-align: center">
          <div class="m-b">
            <div class="btn-group">
              <button onclick="getModal({{$list->id}})" type="button" class="btn btn-sm info active">Tampilkan</button>
              <a href="/contentlink/{{$list->id}}/edit" class="btn btn-sm info">Edit</a>
              <button onclick="event.preventDefault();
              document.getElementById('delete-mylist{{$list->id}}').submit();" type="button" class="btn btn-sm info">Hapus</button>
            </div>
            <form id="delete-mylist{{$list->id}}" action="/contentlink/{{$list->id}}" method="POST" style="display: none;">
              {{ csrf_field() }}
              {{ method_field('DELETE') }}
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endforeach      
</div>
<!-- <div>
  <div class="btn-group m-r">
    <button type="button" class="btn btn-sm white"><i class="fa fa-angle-left"></i></button>
    <button type="button" class="btn btn-sm white"><i class="fa fa-angle-right"></i></button>
  </div>
  Showing <strong>8</strong> of 25
</div> -->
</div>

<!-- Responsive Bootstrap Modal Popup -->


<div id="" class="preview modal" data-backdrop="true">

  <!-- Modal Dialog-->
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content demo">


    </div> <!-- /Modal content-->
  </div> <!-- /Modal Dialog-->
</div> <!-- .modal -->


@endsection

@section('script')
@parent

<!-- Responsive Bootstrap Modal Popup Main Style Sheet -->

<script>

  function getModal(id){
  	$.get("/contentlink/"+id, function(data){
     $('.demo').html( data.template );
     $('.preview').attr('class',data.modalClass);
     $('.preview').attr('id', data.modalId);
     $('.preview').modal('show');
   });
  }

</script>
@endsection
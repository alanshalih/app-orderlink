<!DOCTYPE html>

	<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
	<!--[if IE 7]><html class="lt-ie9 lt-ie8"> <![endif]-->
	<!--[if IE 8]><html class="lt-ie9"> <![endif]-->
	<!--[if gt IE 8]><!--> 
	
<html>

	<!--<![endif]-->
	<!-- Head -->
	<head>

		<!-- Meta -->
		<meta charset="utf-8">

		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">


		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


		<!-- Title -->
		<title>{{$getcampaign->title}}</title>

		<meta property="og:url" content="{{$getcampaign->url}}">

		<!-- Short-Cut Icon -->
		<link rel="shortcut icon" href="#" />

		<!-- Google Web Fonts -->
		<!-- <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600,700,900' rel='stylesheet' type='text/css'> -->
		
		<!-- Font Awesome Version - 4.6.3 -->
		<!-- <link href="/modal/font-awesome-4.6.3/css/font-awesome.min.css" rel="stylesheet" media="all"> -->

		<!-- Bootstrap Style Sheet Version - 3.3.6 -->
		<link href="/modal/css/bootstrap.min.css" rel="stylesheet" media="all">

		<link rel="stylesheet" href="/css/loader/css/style.css?1">

			<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '{{$getcampaign->pixel->pixel_id}}');
	fbq('track', 'PageView');

		</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id={{$getcampaign->pixel->pixel_id}}&ev=PageView&noscript=1"
		/></noscript>
		<!-- DO NOT MODIFY -->


		<style>


				
		.modal.rbm_bottom_center {
		  top: auto;
		  overflow: visible; 
		}

		.modal.rbm_bottom_left {
		  top: auto;
		  overflow: visible; 
		}

		.modal.rbm_bottom_right {
		  top: auto;
		  overflow: visible; 
		}

		.modal.rbm_top_center {
		  bottom: auto;
		  overflow: visible; 
		}

		.modal.rbm_top_left {
		  bottom: auto;
		  overflow: visible; 
		}

		.modal.rbm_top_right {
		  bottom: auto;
		  overflow: visible; 
		}



		</style>

		<!-- Responsive Bootstrap Modal Popup Main Style Sheet -->
		<link href="/modal/css/responsive_bootstrap_modal_popup.css" rel="stylesheet" media="all">


	</head>

<body >

<!-- loader -->
<div class="loader">
	
	<div class="keep-center">
<div class="logo-wrapper">
<div class="logo">
  <img src="http://res.cloudinary.com/resensi-digital/image/upload/c_scale,q_10,w_100/v1483669297/Orderlink_Logo_Box_trans_logo_aja_qkfkbt.png" width="100px" alt="" />  
</div>
<div id="progress-wrap">
  <span>...mohon tunggu...</span>
  <span id="progress">0%</span>
  </div>

</div>
</div>
<div class="text">
<span>Powered by <strong>Orderlink.in</strong></span>
</div>
</div>



<div id="placeholder"></div>


	<!--
	In this example we have used a link to Trigger the Modal.
	You can also use images, links, buttons etc to Trigger the Modal.
	-->
	  <div class="navbar " role="navigation">
        <div class="container">
           
        </div>
    </div>
	
	<!-- End of Trigger Part -->
	
	<!-- Responsive Bootstrap Modal Popup -->
	@foreach($getcampaign->templates as $template)

	<div id="{{$template->modalId}}" class="{{$template->modalClass}}" role="dialog" data-backdrop="{{$template->data_backdrop}}">

		<!-- Modal Dialog-->
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">

				{!! $template->template !!}

				

			</div> <!-- /Modal content-->
		</div> <!-- /Modal Dialog-->
	</div> <!-- .modal -->
	@endforeach



    <!-- End Responsive Bootstrap Modal Popup -->

	<!-- jQuery Version - 1.12.4 -->
	<script src="/modal/js/jquery-1.12.4.min.js"></script>

	<!-- Bootstrap JS File Version - 3.3.6 -->
	<script src="/modal/js/bootstrap.min.js"></script>

	<!-- Responsive Bootstrap Modal Popup Main JS File -->
	<script src="/modal/js/responsive_bootstrap_modal_popup_min.js"></script>

	<script src="/libs/exitModal/exitModal.js"></script>

	<script>
		var templates = {!! json_encode($getcampaign->templates) !!};
		
		function letShowModal(){

		templates.forEach(function(item, index){

			if(item.pivot.event == 'onload')
			{
		
				  setTimeout(function(){ $('#'+item.modalId).modal({show:true}); }, item.pivot.time*1000);
				  console.log('hallo');
			
					
			}
			else
			{

	        $(document).ready(function(){

	            var timer;

	            var exitModalParams = {
	                numberToShown: 5,
	               
	                callbackOnModalShown: function() {
                    timer = setTimeout(function(){
                        var click = $('#'+item.modalId).data('exitModal').modalClick;

                        if(!click)
                        window.location.href = item.pivot.redirectTo;
                    }, 4000)
                
                	},
	                callbackOnModalHide: function() {
	                    clearTimeout(timer);
	                }
	            }

	            setTimeout(function(){ $('#'+item.modalId).exitModal(exitModalParams);
	                if($('#'+item.modalId).data('exit-modal')) {
	                    $(".destroyed-state").hide();
	                    $(".initialized-state").show();
	                } }, 1000);
		        });
			
			}


		});

	
		}
	




    </script>

    <script>

    var getcampaign = {!! json_encode($getcampaign) !!};


    $(document).ready( function(){
	   $('#placeholder').html('<iframe onload="letShowModal()"  id="website" src="'+getcampaign.url+'" style="position:absolute;border:0;width:100%;height:100%;"></iframe>');

	   hideLoader();
	})

		var counter = document.getElementById("progress");

	function changeText(data){
	  counter.innerHTML = data;
	}

	function hideLoader(){
		$('.loader').hide();
	}


	var counter = document.getElementById("progress");

	function changeText(data){
	  counter.innerHTML = data;
	}

	var max = 1500;
	var init = 0;
	var add = 150;




	function loop(){
	  setTimeout(function(){ 
	  init=init+add;
	   
	  changeText((init/(max))*100+'%');
	   if(init<(max)){
	     loop();
	   }else{
	   	hideLoader();
	   }
	}, add);
	}

	loop();








    </script>



</body>

</html>

 <!-- End -->
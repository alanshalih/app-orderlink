
<div class="padding">
  <div class="row">
    <div id="modal-preview">
    <div  class="col-xs-12 col-sm-12 col-md-9 " :class="[  contentlink.border_radius, contentlink.color_scheme, contentlink.background_color, contentlink.backdrop_color, contentlink.shadow]">
     <div class="modal-dialog modal-lg"> 
      <div class="modal-content template box-shadow-md m-b">
        <!-- Close Button -->
       {!! $contentlink->template !!}
      <!-- end modal content -->
      </div>
    </div>
  </div>
  <div class="row">

    <div class="col-md-12">
      
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12 col-md-9">
      <div class="box">
        <div class="box-header">
    @include('contentlink.template.button')
          <h1>BIKIN SUKA-SUKA</h1>
        </div>
        <div class="box-divider m-a-0"></div>
        <div class="box-body">
          <form role="form">
            <div class="md-form-group">
              <input v-model="contentlink.title" class="md-input">
              <label>Judul</label>
            </div>
   
            
            <div class="md-form-group">
              <input oninput="changeImage(value, $('img#main-image'))" value="/modal/images/rbm_single_commerce_01.png" class="md-input">
              <label>Image</label>
            </div>
            <div class="md-form-group">
              <input type="number" min="0" max="5" oninput="changeRating(value, $('li#star-rating'))" value="3.5" class="md-input">
              <label>Rating (skala 5)</label>
            </div>



             <div class="form-group row">
              <label class="col-sm-2 form-control-label">Tampilkan Kuantitas</label>
              <div class="col-sm-10">
                <label class="ui-switch info m-t-xs m-r">
                  <input type="checkbox" v-model="contentlink.kuantitas" onclick="toogleElement(checked, $('.rbm_form_num'))"  >
                  <i></i>
                </label>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Tampilkan Warna</label>
              <div class="col-sm-10">
                <label class="ui-switch info m-t-xs m-r">
                  <input type="checkbox" v-model="contentlink.warna"  onclick="toogleElement(checked, $('.rbm_form_color'))" >
                  <i></i>
                </label>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Tampilkan Ukuran</label>
              <div class="col-sm-10">
                <label class="ui-switch info m-t-xs m-r">
                  <input type="checkbox" v-model="contentlink.ukuran"  onclick="toogleElement(checked, $('.rbm_form_size'))"  >
                  <i></i>
                </label>
              </div>
            </div>
            <div class="md-form-group">
                     <input oninput="changeText(value, $('button.call_to_action'))" v-model="contentlink.cta" class="md-input">
                     <label>Call to Action Teks</label>
                  </div>
            <div class="md-form-group">
              <input v-model="contentlink.action" class="md-input">
              <label>Action URL</label>
            </div>

                <div class="md-form-group">
                     <select v-model="contentlink.background_color" class="md-input">
                        <option :value="cs.class" v-for="cs in properties.background_color">@{{cs.name}}</option>
                     </select>
                     <label>Warna Background</label>
                  </div>


            

            <div>

            </div>
           
          </form>
        </div>
      </div>
      @include('contentlink.template.properties')
    </div>
  </div>
</div>


  
  
  <!-- End of Trigger Part -->
  
  <!-- Responsive Bootstrap Modal Popup -->
  <div :id="contentlink.id" class="modal preview fade rbm_modal   rbm_animate   pauseVideoM onlinePauseVideoMt "  :class="[contentlink.color_scheme, contentlink.background_color, contentlink.backdrop_color, contentlink.transparency, contentlink.modalSize, contentlink.position, contentlink.border_radius, contentlink.animation, contentlink.animation_duration, contentlink.animation_time_function, contentlink.shadow, contentlink.no_backdrop]" role="dialog" :data-backdrop="contentlink.data_backdrop">

    <!-- Modal Dialog-->
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content demo">

      </div> <!-- /Modal content-->
    </div> <!-- /Modal Dialog-->
  </div> <!-- .modal -->

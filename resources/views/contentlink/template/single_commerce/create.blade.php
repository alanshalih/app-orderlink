@extends('layouts.app')

@section('content')

<div class="ecommerce modal">
  @{{ contentlink }}
</div>


<div class="padding">
  <div class="row">
    <div >
    <div  class="col-xs-12 col-sm-12 col-md-9 " :class="[  contentlink.border_radius, contentlink.color_scheme, contentlink.background_color, contentlink.backdrop_color, contentlink.shadow]">
     <div class="modal-dialog modal-lg"> 
      <div id="modal-preview" class="modal-content template box-shadow-md m-b">
        <!-- Close Button -->
       <div id="editorWrapper">
        <a href="#" data-dismiss="modal" class="rbm_btn_x_out_shtr rbm_sngl_cmrce_close">close</a>
        
        <!-- .row -->
        <div class="row">

          <!-- Image Box -->
          <div class="col-xs-12 col-sm-5 col-md-5">
            <div class="rbm_sngl_cmrce_img">
              <img id="main-image" src="/modal/images/rbm_single_commerce_01.png"  alt="rbm_single_commerce_01">
            </div>
          </div>
          <!-- /Image Box -->

          <!-- Description Box -->
          <div class="col-xs-12 col-sm-7 col-md-7">
            
            <!-- Content -->
            <div class="rbm_sngl_cmrce_txt">
              <!-- Name -->
              <h1 id="title">BEST OF MY PRODUCT</h1>
              <!-- Rating Stars -->
              <ul>

                <li id="star-rating">  <span class="fa fa-star "></span><span class="fa fa-star "></span><span class="fa fa-star "></span><span class="fa fa-star-half-o "></span><span class="fa fa-star-o "></span></li>
              </ul>
              <!-- Price -->
              <span class="price text-white">Rp. 10.000,00</span>
              <!-- Detail -->
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta consectetur id iure consequatur, accusamus asperiores, doloribus illum voluptatem aperiam incidunt quos rem culpa. Possimus architecto dolores impedit dolorem voluptatum inventore!</p>
              <!-- Form -->
              <form id="order_form" :action="contentlink.action" method="GET">
                <div class="rbm_form_num" >
                  <label>kuantitas</label>
                  <input type="number" value="1" name="quantity" min="1" max="20">
                </div>
                <div class="rbm_form_color" >
                  <label>warna</label>
                  <select name="colors">
                    <option value="red">red</option>
                    <option value="green">green</option>
                    <option value="blue">blue</option>
                  </select>
                </div>
                <div class="rbm_form_size" >
                  <label>ukuran</label>
                  <select name="size">
                    <option value="18">18</option>
                    <option value="25">25</option>
                    <option value="36">36</option>
                  </select>
                </div>
                <div class="rbm_form_cmrce_btn">
                  <button type="submit" class="rbm_btn_x_out_shtr call_to_action">BELI SEKARANG</button>
                </div>


              </form><!-- /Form -->
            </div> <!-- /.rbm_sngl_cmrce_txt -->
          
          </div>
          <!-- /Description Box -->
        
        
        </div> <!-- /.row -->


      </div></div>
      </div>
      <!-- end modal content -->
      </div>
    </div>
  </div>
  <div class="row">

    <div class="col-md-12">
      
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12 col-md-9">
      <div class="box">
        <div class="box-header">
    @include('contentlink.template.button')
          <h1>BIKIN SUKA-SUKA</h1>
        </div>
        <div class="box-divider m-a-0"></div>
        <div class="box-body">
          <form role="form">
            <div class="md-form-group">
              <input v-model="contentlink.title" class="md-input">
              <label>Judul</label>
            </div>
   
            
            <div class="md-form-group">
              <input oninput="changeImage(value, $('img#main-image'))" v-model="contentlink.image1" class="md-input">
              <label>Image</label>
            </div>
            <div class="md-form-group">
              <input type="number" min="0" max="5" oninput="changeRating(value, $('li#star-rating'))" value="3.5" class="md-input">
              <label>Rating (skala 5)</label>
            </div>



             <div class="form-group row">
              <label class="col-sm-2 form-control-label">Tampilkan Kuantitas</label>
              <div class="col-sm-10">
                <label class="ui-switch info m-t-xs m-r">
                  <input type="checkbox" v-model="contentlink.kuantitas" onclick="toogleElement(checked, $('.rbm_form_num'))"  >
                  <i></i>
                </label>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Tampilkan Warna</label>
              <div class="col-sm-10">
                <label class="ui-switch info m-t-xs m-r">
                  <input type="checkbox" v-model="contentlink.warna"  onclick="toogleElement(checked, $('.rbm_form_color'))" >
                  <i></i>
                </label>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Tampilkan Ukuran</label>
              <div class="col-sm-10">
                <label class="ui-switch info m-t-xs m-r">
                  <input type="checkbox" v-model="contentlink.ukuran"  onclick="toogleElement(checked, $('.rbm_form_size'))"  >
                  <i></i>
                </label>
              </div>
            </div>
            <div class="md-form-group">
                     <input oninput="changeText(value, $('button.call_to_action'))" v-model="contentlink.cta" class="md-input">
                     <label>Call to Action Teks</label>
                  </div>
                  
            <div class="md-form-group">
              <input oninput="changeFormActionUrl(value,$('#order_form'))" v-model="contentlink.action" class="md-input">
              <label>Action URL</label>
            </div>

                <div class="md-form-group">
                     <select v-model="contentlink.background_color" class="md-input">
                        <option :value="cs.class" v-for="cs in properties.background_color">@{{cs.name}}</option>
                     </select>
                     <label>Warna Background</label>
                  </div>


            

            <div>

            </div>
           
          </form>
        </div>
      </div>
      @include('contentlink.template.properties')
    </div>
  </div>
</div>


  
  
  <!-- End of Trigger Part -->
  
  <!-- Responsive Bootstrap Modal Popup -->
  <div :id="contentlink.id" class="modal preview fade rbm_modal   rbm_animate   pauseVideoM onlinePauseVideoMt "  :class="[contentlink.color_scheme, contentlink.background_color, contentlink.backdrop_color, contentlink.transparency, contentlink.modalSize, contentlink.position, contentlink.border_radius, contentlink.animation, contentlink.animation_duration, contentlink.animation_time_function, contentlink.shadow, contentlink.no_backdrop]" role="dialog" :data-backdrop="contentlink.data_backdrop">

    <!-- Modal Dialog-->
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content demo">

      </div> <!-- /Modal content-->
    </div> <!-- /Modal Dialog-->
  </div> <!-- .modal -->

  
@endsection

@section('script')
    @parent

    <script>
        var contentlinkID = null;
    </script>

@include('contentlink.template.single_commerce.properties');

@include('contentlink.template.function-script')

@include('contentlink.template.create_script');

    
    
@endsection
@extends('layouts.app')

@section('content')

<div class="ecommerce modal">
  {{ $contentlink->data}}
</div>

<div class="saveecommerce modal">
  @{{ contentlink }}
</div>


<div v-html="contentlink.formHTML"  class="form-data modal">
    
</div>
<div class="padding">
   <div class="row">
   <div >
      <div  class="col-xs-12 col-sm-12 col-md-9" :class="[ contentlink.modalSize, contentlink.border_radius, contentlink.color_scheme, contentlink.background_color, contentlink.backdrop_color, contentlink.shadow]">
         <div class="modal-dialog">
            <div  id="modal-preview" class="modal-content template box-shadow-md m-b">
               <!-- Close Button -->
               <div id="editorWrapper">
               	{!!$contentlink->template!!}
               </div>
            </div>
         </div>
         <!-- end modal content -->
      </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-12">
      </div>
</div>
<div class="row">
      <div class="col-sm-12 col-md-9">
         <div class="box">
            <div class="box-header">
                 @include('contentlink.template.button')
               <h1>BIKIN SUKA-SUKA</h1>
            </div>
            <div class="box-divider m-a-0"></div>
            <div class="box-body" id="modal-content-manipulation">

                  <div class="md-form-group">
                     <input v-model="contentlink.title" class="md-input">
                     <label>Judul</label>
                  </div>


             
                  <div class="md-form-group">
                     <select class="md-input" v-model="contentlink.pakaiAutoresponder"  onchange="isUsingAutoresponder(value == 'tidak', $('#native-email'),$('#pakaiAutoresponder'))">
                        <option value="tidak">Tidak, Simpan di Orderlink</option>
                        <option value="ya">Ya, Pakai</option>
                     </select>
                     <label>Apakah Anda Pakai Autoresponder?</label>
                  </div>

                  
                  <div v-show="contentlink.pakaiAutoresponder == 'ya'" class="md-form-group" id="htmlform" >
                     <textarea name="" v-model="contentlink.formHTML" cols="30" rows="10" class="md-input"></textarea>
                     <button onclick="htmlParsing()" class="btn btn-outline b-info text-info">Render</button>
                     <label>Autoresponder HTML Form</label>
                  </div>
             

                  <div class="md-form-group">
                     <select class="md-input" v-model="contentlink.afterSubmit" onchange="changeValue(value, $('#afterSubmit'))">
                        <option value="tetap">Tetap di Halaman Tersebut</option>
                        <option value="ikut">Ikut Pengaturan Autoresponder</option>
                        <option value="redirect">Dialihkan ke Alamat Baru</option>
                     </select>
                     <label>Setelah Submit Email, Pengujung akan?</label>
                  </div>

                  <div v-show="contentlink.afterSubmit == 'redirect'" class="md-form-group">
                     <input v-model="contentlink.linkText" oninput="changeValue(value, $('#afterSubmitProps'))" class="md-input">
                     <label>Dialihkan ke?</label>
                  </div>

                    <div class="md-form-group">
                     <input v-model="contentlink.cta" oninput="changeText(value, $('button.call_to_action'))" class="md-input">
                     <label>Call to Action Text</label>
                  </div>

                   <div v-show="contentlink.afterSubmit == 'tetap'" class="md-form-group">
                     <input v-model="contentlink.afterSubsProps" oninput="changeValue(value, $('#afterSubmitProps'))" class="md-input">
                     <label>Notifikasi Setelah Submit (anda bisa mengucapkan terima kasih)</label>
                  </div>

                  <div class="md-form-group">
                     <select v-model="contentlink.background_color" class="md-input">
                        <option :value="cs.class" v-for="cs in properties.background_color">@{{cs.name}}</option>
                     </select>
                     <label>Warna Background</label>
                  </div>

                  <div>
                  </div>
            </div>
         </div>
         @include('contentlink.template.properties')
      </div>
   </div>
</div>
<!-- End of Trigger Part -->
<!-- Responsive Bootstrap Modal Popup -->
<div :id="contentlink.id" class="modal preview fade rbm_modal   rbm_shadow_none rbm_animate   pauseVideoM onlinePauseVideoMt "  :class="[contentlink.color_scheme, contentlink.background_color, contentlink.backdrop_color, contentlink.transparency, contentlink.modalSize, contentlink.position, contentlink.border_radius, contentlink.animation, contentlink.animation_duration, contentlink.animation_time_function, contentlink.shadow, contentlink.no_backdrop]" role="dialog" :data-backdrop="contentlink.data_backdrop">
   <!-- Modal Dialog-->
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content demo">
      
      </div>
      <!-- /Modal content-->
   </div>
   <!-- /Modal Dialog-->
</div>
<!-- .modal -->

@endsection
@section('script')
@parent

<script>
   var contentlinkID = {!! json_encode($contentlink) !!};
</script>


@include('contentlink.template.newsletter_1.properties')

@include('contentlink.template.edit_script');
@endsection
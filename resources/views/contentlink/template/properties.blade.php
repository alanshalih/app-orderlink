<div id="properties" class="box">
            <div class="box-header">
                @include('contentlink.template.button')

               <h1>PROPERTIES</h1>
            </div>
            <div class="box-divider m-a-0"></div>
            <div class="box-body">
               <form role="form">
                  <div class="row">
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="md-form-group">
                           <select v-model="contentlink.color_scheme" class="md-input">
                              <option :value="cs.class" v-for="cs in properties.color_scheme">@{{cs.name}}</option>
                           </select>
                           <label>Skema Warna</label>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="md-form-group">
                           <select v-model="contentlink.shadow" class="md-input">
                              <option :value="cs.class" v-for="cs in properties.shadow">@{{cs.name}}</option>
                           </select>
                           <label>Shadow</label>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="md-form-group">
                           <select v-model="contentlink.position" class="md-input">
                              <option :value="cs.class" v-for="cs in properties.position">@{{cs.name}}</option>
                           </select>
                           <label>Posisi</label>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="md-form-group">
                           <select v-model="contentlink.backdrop_color" class="md-input">
                              <option :value="cs.class" v-for="cs in properties.backdrop_color">@{{cs.name}}</option>
                           </select>
                           <label>Warna Backdrop</label>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="md-form-group">
                           <select v-model="transparency" class="md-input">
                              <option :value="cs.class" v-for="cs in properties.transparency">@{{cs.name}}</option>
                           </select>
                           <label>Transparansi Backdrop</label>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="md-form-group">
                           <select v-model="contentlink.animation" class="md-input">
                              <option :value="cs.class" v-for="cs in properties.animation">@{{cs.name}}</option>
                           </select>
                           <label>Animasi</label>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="md-form-group">
                           <select v-model="contentlink.animation_duration" class="md-input">
                              <option :value="cs.class" v-for="cs in properties.animation_duration">@{{cs.name}}</option>
                           </select>
                           <label>Durasi Animasi</label>
                        </div>
                     </div>
                     <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="md-form-group">
                           <select v-model="contentlink.animation_time_function" class="md-input">
                              <option :value="cs.class" v-for="cs in properties.animation_time_function">@{{cs.name}}</option>
                           </select>
                           <label>Animasi Waktu</label>
                        </div>
                     </div>
                  </div>
          
               </form>
            </div>
         </div>
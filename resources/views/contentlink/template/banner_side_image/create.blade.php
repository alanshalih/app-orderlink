@extends('layouts.app')

@section('content')
<div class="ecommerce modal">
  @{{ contentlink | json}}
</div>

<div class="padding">
 <div class="row" >
   <div>
    <div   class="col-xs-12 col-sm-12 col-md-9" :class="[ contentlink.modalSize, contentlink.border_radius, contentlink.color_scheme, contentlink.background_color, contentlink.backdrop_color, contentlink.shadow]">
     <div class="modal-dialog">
      <div  id="modal-preview" class="modal-content template box-shadow-md m-b">

       <div id="editorWrapper">

         <!-- Modal-content -->

         <!-- Close Button -->
         <a href="#" class="rbm_btn_x_out_shtr rbm_bnr_close rbm_bnr_cl_btn" data-dismiss="modal">
          <span class="fa fa-times"></span>
        </a>

        <!-- Large Banner Content -->
        <div class="rbm_bnr_content">
        <a id="call_to_action_url" :href="contentlink.action"><img style="  width: 100%;display: block;"  id="background_banner" src="/modal/images/rbm_banner_side_lg_01.jpg" alt="rbm_banner_side_lg_01"></a>
        </div> <!-- /.rbm_bnr_content -->


      </div>
    </div>
  </div>
  <!-- end modal -->
</div>
</div>
</div>

<div class="row">
  <div class="col-md-12 separator" >

  </div>
</div>
<div class="row">
  <div class="col-sm-12 col-md-9">
   <div class="box">
    <div class="box-header">
     @include('contentlink.template.button')
     <h1>BIKIN SUKA-SUKA</h1>
   </div>
   <div class="box-divider m-a-0"></div>
   <div class="box-body" id="modal-content-manipulation">
    
    <div class="md-form-group">
     
      

    </div>
    <div class="md-form-group">
     <input v-model="contentlink.title" class="md-input">
     <label>Judul</label>
   </div>

   <div  class="md-form-group">
     <input v-model="contentlink.action" oninput="changeActionUrl(value, $('#call_to_action_url'))" class="md-input">
     <label>Call to Action URL</label>
   </div>


   <div  class="md-form-group">
     <input oninput="changeImage(value, $('#background_banner'))" v-model="contentlink.image1" class="md-input">
     <label>Background Image (ukuran 469x469)</label>
   </div>


   <div class="md-form-group">
     <select v-model="contentlink.background_color" class="md-input">
      <option :value="cs.class" v-for="cs in properties.background_color">@{{cs.name}}</option>
    </select>
    <label>Warna Background</label>
  </div>



  <div>
  </div>
</div>
</div>
@include('contentlink.template.properties')
</div>
</div>
</div>
<!-- End of Trigger Part -->
<!-- Responsive Bootstrap Modal Popup -->
<div :id="contentlink.id" class="modal preview fade rbm_modal   rbm_shadow_none rbm_animate   pauseVideoM onlinePauseVideoMt "  :class="[contentlink.color_scheme, contentlink.background_color, contentlink.backdrop_color, contentlink.transparency, contentlink.modalSize, contentlink.position, contentlink.border_radius, contentlink.animation, contentlink.animation_duration, contentlink.animation_time_function, contentlink.shadow,  contentlink.no_backdrop]" role="dialog" :data-backdrop="contentlink.data_backdrop">
 <!-- Modal Dialog-->
 <div class="modal-dialog">
  <!-- Modal content-->
  <div class="modal-content demo">
    
  </div>
  <!-- /Modal content-->
</div>
<!-- /Modal Dialog-->
</div>
<!-- .modal -->
@endsection
@section('script')
@parent
<!-- Responsive Bootstrap Modal Popup Main Style Sheet -->

<script>
 var contentlinkID = null;
</script>

<!-- <script src="{{elixir('/js/domToImage.js')}}"></script> -->


@include('contentlink.template.banner_side_image.properties')

@include('contentlink.template.function-script')

@include('contentlink.template.create_script');

@endsection
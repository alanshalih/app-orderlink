@extends('layouts.app')

@section('content')
<div class="padding">
   <div class="alert alert-info"><strong>Coming Soon!</strong> Puluhan Option Form, Product Display dan Banner baru Siap Pakai!</div>
   <div class="row">
         <div class="col-xs-6 col-sm-4 col-md-3">
            <div class="box p-a-xs">
               <a href="/create/?t=single_commerce"><img src="/images/single commerce1.JPG" alt="" class="img-responsive"></a>
               <div class="p-a-sm">
                  <h6>Ecommerce Single Product</h6>
                  <div class="text-ellipsis" style="text-align: center">
                     <div class="m-b">
                        <div class="btn-group">
                           <a href="#single_commerce"  data-toggle="modal" class="btn btn-sm info active">Demo</a>
                           <a href="/contentlink/create/?t=single_commerce" class="btn btn-sm info active">Pakai ini</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-xs-6 col-sm-4 col-md-3">
            <div class="box p-a-xs">
               <a href="/create/?t=newsletter_1"><img src="http://res.cloudinary.com/resensi-digital/image/upload/v1485646114/newletter_dgtmbr.jpg" alt="" class="img-responsive"></a>
               <div class="p-a-sm">
                  <h6>Newsletter 1</h6>
                  <div class="text-ellipsis" style="text-align: center">
                     <div class="m-b">
                        <div class="btn-group">
                           <a href="#newsletter_1"  data-toggle="modal" class="btn btn-sm info active">Demo</a>
                           <a href="/contentlink/create/?t=newsletter_1" class="btn btn-sm info active">Pakai ini</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

	     <div class="col-xs-6 col-sm-4 col-md-3">
	         <div class="box p-a-xs">
	            <a href="/contentlink/create/?t=banner_1"><img src="/images/banner_1.JPG" alt="" class="img-responsive"></a>
	            <div class="p-a-sm">
	               <h6>Banner Text 1</h6>
	               <div class="text-ellipsis" style="text-align: center">
	                  <div class="m-b">
	                     <div class="btn-group">
	                        <a href="#banner_1"  data-toggle="modal" class="btn btn-sm info active">Demo</a>
	                        <a href="/contentlink/create/?t=banner_1" class="btn btn-sm info active">Pakai ini</a>
	                     </div>
	                  </div>
	               </div>
	            </div>
	         </div>
	      </div>
         <div class="col-xs-6 col-sm-4 col-md-3">
            <div class="box p-a-xs">
               <a href="/contentlink/create/?t=banner_2"><img src="http://res.cloudinary.com/resensi-digital/image/upload/v1485646024/top_ibyor1.jpg" alt="" class="img-responsive"></a>
               <div class="p-a-sm">
                  <h6>Banner Text 2</h6>
                  <div class="text-ellipsis" style="text-align: center">
                     <div class="m-b">
                        <div class="btn-group">
                           <a href="#banner_2"  data-toggle="modal" class="btn btn-sm info active">Demo</a>
                           <a href="/contentlink/create/?t=banner_2" class="btn btn-sm info active">Pakai ini</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-xs-6 col-sm-4 col-md-3">
            <div class="box p-a-xs">
               <a href="/contentlink/create/?t=banner_side_1"><img src="/images/banner_side_1.JPG" alt="" class="img-responsive"></a>
               <div class="p-a-sm">
                  <h6>Banner Side 1</h6>
                  <div class="text-ellipsis" style="text-align: center">
                     <div class="m-b">
                        <div class="btn-group">
                           <a href="#banner_side_1"  data-toggle="modal" class="btn btn-sm info active">Demo</a>
                           <a href="/contentlink/create/?t=banner_side_1" class="btn btn-sm info active">Pakai ini</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
          <div class="col-xs-6 col-sm-4 col-md-3">
            <div class="box p-a-xs">
               <a href="/contentlink/create/?t=banner_image_1"><img src="http://res.cloudinary.com/resensi-digital/image/upload/v1485572243/orderlink_eqt7f0.jpg" alt="" class="img-responsive"></a>
               <div class="p-a-sm">
                  <h6>Banner Image 1 (469x469)</h6>
                  <div class="text-ellipsis" style="text-align: center">
                     <div class="m-b">
                        <div class="btn-group">
                           <a href="#banner_image_1"  data-toggle="modal" class="btn btn-sm info active">Demo</a>
                           <a href="/contentlink/create/?t=banner_image_1" class="btn btn-sm info active">Pakai ini</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-xs-6 col-sm-4 col-md-3">
            <div class="box p-a-xs">
               <a href="/contentlink/create/?t=banner_image_2"><img src="http://res.cloudinary.com/resensi-digital/image/upload/v1485571921/20_vl8jt1.jpg" alt="" class="img-responsive"></a>
               <div class="p-a-sm">
                  <h6>Banner Image 2 (1460x180)</h6>
                  <div class="text-ellipsis" style="text-align: center">
                     <div class="m-b">
                        <div class="btn-group">
                           <a href="#banner_image_2"  data-toggle="modal" class="btn btn-sm info active">Demo</a>
                           <a href="/contentlink/create/?t=banner_image_2" class="btn btn-sm info active">Pakai ini</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-xs-6 col-sm-4 col-md-3">
            <div class="box p-a-xs">
               <a href="/contentlink/create/?t=banner_side_image"><img src="http://res.cloudinary.com/resensi-digital/image/upload/v1485645920/sidebar_pbzfcs.jpg" alt="" class="img-responsive"></a>
               <div class="p-a-sm">
                  <h6>Banner Image Side (300x110)</h6>
                  <div class="text-ellipsis" style="text-align: center">
                     <div class="m-b">
                        <div class="btn-group">
                           <a href="#banner_side_image"  data-toggle="modal" class="btn btn-sm info active">Demo</a>
                           <a href="/contentlink/create/?t=banner_side_image" class="btn btn-sm info active">Pakai ini</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>


<!-- 	</div>
	   <div>
      <div class="btn-group m-r">
         <button type="button" class="btn btn-sm white"><i class="fa fa-angle-left"></i></button>
         <button type="button" class="btn btn-sm white"><i class="fa fa-angle-right"></i></button>
      </div>
      Showing <strong>8</strong> of 25
   </div> -->
</div>
<!-- Responsive Bootstrap Modal Popup -->
<div id="single_commerce" class="modal preview fade rbm_modal   rbm_animate   pauseVideoM onlinePauseVideoMt  rbm_blue rbm_bg_white rbm_bd_black rbm_bd_semi_trnsp rbm_size_sngl_cmrce rbm_center rbm_none_radius rbmFadeInUp rbm_duration_md rbm_ease rbm_shadow_none" data-backdrop="true">
   <!-- Modal Dialog-->
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <a href="#" data-dismiss="modal" class="rbm_btn_x_out_shtr rbm_sngl_cmrce_close">close</a>  
         <div class="row">
            <div class="col-xs-12 col-sm-5 col-md-5">
               <div class="rbm_sngl_cmrce_img"><img src="http://res.cloudinary.com/resensi-digital/image/upload/c_crop,h_600,w_600/v1480980799/Best-Digital-Watches-for-Men_vzejoo.jpg" alt="rbm_single_commerce_01"></div>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-7">
               <div class="rbm_sngl_cmrce_txt">
                  <h1 id="title">DIGITAL WATCH PREMIUM</h1>
                  <ul>
                     <li><span class="fa fa-star "></span></li>
                     <li><span class="fa fa-star "></span></li>
                     <li><span class="fa fa-star "></span></li>
                     <li><span class="fa fa-star-half-o"></span></li>
                     <li><span class="fa fa-star-o"></span></li>
                  </ul>
                  <h2>Rp.499.000,00</h2>
                  <p>bikin gaya lo lebih asyik pake jam canggih ini!</p>
                  <form action="/tracklink" method="GET">
                     <div class="rbm_form_num"><label>kuantitas</label> <input type="number" value="1" name="quantity" min="1" max="20"></div>
                     <div class="rbm_form_color">
                        <label>warna</label> 
                        <select name="colors">
                           <option value="red">red</option>
                           <option value="green">green</option>
                           <option value="blue">blue</option>
                        </select>
                     </div>
                     <div class="rbm_form_size" style="display: none;">
                        <label>ukuran</label> 
                        <select name="size">
                           <option value="18">18</option>
                           <option value="25">25</option>
                           <option value="36">36</option>
                        </select>
                     </div>
                     <div class="rbm_form_cmrce_btn"><button type="submit" class="rbm_btn_x_out_shtr">Beli Sekarang</button></div>
                  </form>
               </div>
            </div>
         </div>
      </div>
      <!-- /Modal content-->
   </div>
   <!-- /Modal Dialog-->
</div>
<!-- .modal -->
<div id="newsletter_1" class="modal preview fade rbm_modal   rbm_shadow_none rbm_animate   pauseVideoM onlinePauseVideoMt  rbm_blue rbm_bg_white rbm_bd_black rbm_bd_semi_trnsp rbm_form_general rbm_size_subscribe rbm_center rbm_none_radius rbmFadeInUp rbm_duration_md rbm_ease rbm_shadow_none" data-backdrop="true">
   <!-- Modal Dialog-->
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="rbm_subscribe_close"><a href="#" data-dismiss="modal" class="rbm_btn_x_out_shtr"><span class="fa fa-times"></span></a></div>
         <div class="rbm_subscribe">
            <div class="rbm_subscribe_txt">
               <h1>Pengen Semangat Terus?</h1>
               <p>yuk langganan motivaksi bisnis dari saya, saya akan mengirimkan kepada anda tulisan terbaru saya langsung ke email anda setiap minggunya.</p>
            </div>
            <form action="/track" method="POST">
               <input type="hidden" name="_token" value="oTcwTSRAc33kQnpAP46iu36yodVxBuPTQf8qa4mV"> 
               <div class="rbm_input_txt"><input type="email" name="email" placeholder="Masukan Email Anda"></div>
               <input type="hidden" name="afterSubmit" value="tetap"> <input type="hidden" name="pakaiAutoresponder" value="tidak"> <input type="hidden" name="afterSubmitProps" value="Terima Kasih"> <input type="hidden" name="author_id" value="1"> <input type="hidden" name="template_id" value="4">
               <div class="rbm_form_submit"><button type="submit" class="submit rbm_btn_x_out_shtr">Berlangganan</button></div>
            </form>
         </div>
      </div>
      <!-- /Modal content-->
   </div>
   <!-- /Modal Dialog-->
</div>
<!-- .modal -->
<div id="banner_1" class="modal preview fade rbm_modal   rbm_shadow_none rbm_animate   pauseVideoM onlinePauseVideoMt  rbm_blue rbm_bg_black rbm_bd_black rbm_bd_semi_trnsp rbm_size_banner_box_lg rbm_center rbm_none_radius rbmFadeInUp rbm_duration_md rbm_ease rbm_shadow_none" data-backdrop="true">
   <!-- Modal Dialog-->
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <a href="#" data-dismiss="modal" class="rbm_btn_x_out_shtr rbm_bnr_close rbm_bnr_cl_btn"><span class="fa fa-times"></span></a>  
         <div class="rbm_bnr_content">
            <div class="rbm_bnr_img rbm_bnr_box_lg_img"><img src="http://img00.deviantart.net/fc3f/i/2015/104/1/0/blissful_sky_1_1_stock_background_by_lcbailey-d8d20u0.jpg" alt="rbm_banner_box_lg_01"></div>
            <div class="rbm_bnr_txt rbm_bnr_box_lg_txt">
               <img src="/images/logo-orderlink.png" alt="company_logo">  
               <h2 style="color: white">PROMO DIMANAPUN GRATIS</h2>
               <p>gunakan banner ini untuk sarana promo anda!</p>
               <a href="http://www.google.co.id" class="rbm_btn_x_out_shtr rbm_bnr_btn">Klik disini</a> <span>http://orderlink.in</span>
            </div>
         </div>
      </div>
      <!-- /Modal content-->
   </div>
   <!-- /Modal Dialog-->
</div>

<div id="banner_image_1" class="modal preview fade rbm_modal   rbm_shadow_none rbm_animate   pauseVideoM onlinePauseVideoMt  rbm_blue  rbm_bd_black rbm_bd_semi_trnsp rbm_size_banner_box_lg rbm_center rbm_none_radius rbmFadeInUp rbm_duration_md rbm_ease rbm_shadow_none" data-backdrop="true">
   <!-- Modal Dialog-->
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <a href="#" data-dismiss="modal" class="rbm_btn_x_out_shtr rbm_bnr_close rbm_bnr_cl_btn"><span class="fa fa-times"></span></a>  
         <div class="rbm_bnr_content">
            <div class="rbm_bnr_img rbm_bnr_box_lg_img"><img src="http://res.cloudinary.com/resensi-digital/image/upload/v1485572243/orderlink_eqt7f0.jpg" alt="rbm_banner_box_lg_01"></div>

         </div>
      </div>
      <!-- /Modal content-->
   </div>
   <!-- /Modal Dialog-->
</div>

<div id="banner_2" class="modal fade rbm_modal rbm_size_banner_lg rbm_top_right rbm_bd_semi_trnsp rbm_bd_black rbm_bg_black rbm_blue rbm_none_radius rbm_shadow_none rbm_animate rbm_duration_md rbmFadeInDown rbm_easeOutQuint" data-backdrop="true">
   <!-- Modal Dialog-->
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
       
            <!-- Close Button -->
            <a href="#" class="rbm_btn_x_out_shtr rbm_bnr_close rbm_bnr_cl_btn" data-dismiss="modal">
               <span class="fa fa-times"></span>
            </a>
               
               <!-- Large Banner Content -->
               <div class="rbm_bnr_content">
                  <div class="rbm_bnr_img rbm_bnr_lg_img">
                     <img src="/modal/images/rbm_banner_lg_01.jpg" alt="rbm_banner_lg_01">
                  </div>
                  <div class="rbm_bnr_txt rbm_bnr_lg_txt">
                     <h1>bikin iklan di website apapun</h1>
                     <p>Gunakan Banner ini untuk sarana promosi anda.</p>
                     <a href="#" class="rbm_btn_x_out_shtr rbm_bnr_btn">pelajari</a>
                     <span>orderlink.in</span>
                  </div>
               </div> <!-- /.rbm_bnr_content -->


      </div>
      <!-- /Modal content-->
   </div>
   <!-- /Modal Dialog-->
</div>

<div id="banner_image_2" class="modal fade rbm_modal rbm_size_banner_lg rbm_top_right rbm_bd_semi_trnsp rbm_bd_black  rbm_blue rbm_none_radius rbm_shadow_none rbm_animate rbm_duration_md rbmFadeInDown rbm_easeOutQuint" data-backdrop="true">
   <!-- Modal Dialog-->
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
       
            <!-- Close Button -->
            <a href="#" class="rbm_btn_x_out_shtr rbm_bnr_close rbm_bnr_cl_btn" data-dismiss="modal">
               <span class="fa fa-times"></span>
            </a>
               
               <!-- Large Banner Content -->
               <div class="rbm_bnr_content">
                  <div class="rbm_bnr_img rbm_bnr_lg_img">
                     <img src="http://res.cloudinary.com/resensi-digital/image/upload/v1485571921/20_vl8jt1.jpg" alt="rbm_banner_lg_01">
                  </div>

               </div> <!-- /.rbm_bnr_content -->


      </div>
      <!-- /Modal content-->
   </div>
   <!-- /Modal Dialog-->
</div>
<!-- .modal -->


   <!-- Responsive Bootstrap Modal Popup -->
   <div id="banner_side_1" class="modal fade rbm_modal rbm_size_banner_side_lg rbm_left rbm_bd_semi_trnsp rbm_bd_black rbm_bg_black rbm_blue rbm_none_radius rbm_shadow_none rbm_animate rbm_duration_md rbmFadeInLeft rbm_easeOutQuint" role="dialog">

      <!-- Modal Dialog-->
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">

            <!-- Close Button -->
            <a href="#" class="rbm_btn_x_out_shtr rbm_bnr_close rbm_bnr_cl_btn" data-dismiss="modal">
               <span class="fa fa-times"></span>
            </a>

               <!-- Large Banner Content -->
               <div class="rbm_bnr_content">
                  <div class="rbm_bnr_img rbm_bnr_side_lg_img">
                     <img src="/modal/images/rbm_banner_side_lg_01.jpg" alt="rbm_banner_side_lg_01">
                  </div>
                  <div class="rbm_bnr_txt rbm_bnr_side_lg_txt">
                     <img src="/images/logo-orderlink.png" alt="company_logo">
                     <h1>PASANG IKLAN DI WEB MANAPUN GRATIS</h1>
                     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam nibh.</p>
                     <a href="#" class="rbm_btn_x_out_shtr rbm_bnr_btn">learn more</a>
                     <span>orderlink.in</span>
                  </div>
               </div> <!-- /.rbm_bnr_content -->

         </div> <!-- /Modal content-->
      </div> <!-- /Modal Dialog-->
   </div> <!-- .modal -->

    <div id="banner_side_image" class="modal fade rbm_modal rbm_size_banner_side_lg rbm_left rbm_bd_semi_trnsp rbm_bd_black rbm_blue rbm_none_radius rbm_shadow_none rbm_animate rbm_duration_md rbmFadeInLeft rbm_easeOutQuint" role="dialog">

      <!-- Modal Dialog-->
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">

            <!-- Close Button -->
            <a href="#" class="rbm_btn_x_out_shtr rbm_bnr_close rbm_bnr_cl_btn" data-dismiss="modal">
               <span class="fa fa-times"></span>
            </a>

               <!-- Large Banner Content -->
               <div class="rbm_bnr_content">
                  <div class="rbm_bnr_img rbm_bnr_side_lg_img">
                     <img src="http://res.cloudinary.com/resensi-digital/image/upload/v1485572050/Untitled_design_bitl3w.jpg" alt="rbm_banner_side_lg_01">
                  </div>
 
               </div> <!-- /.rbm_bnr_content -->

         </div> <!-- /Modal content-->
      </div> <!-- /Modal Dialog-->
   </div> <!-- .modal -->
    <!-- End Responsive Bootstrap Modal Popup -->

@endsection
<script>
var isEditMode = true;




function editMode(t){



    $(t).toggleClass('b-info b-success').toggleClass('text-info text-success')
    .children().toggleClass('fa-pencil fa-check');
    	 
    	 if(isEditMode){
    	 	   $(function() {
		    $('#editorWrapper').summernote({
		      height: 300,
		      airMode: true,
		      popover: {


  air: [
    ['color', ['color']],
    ['font', ['bold','italic', 'underline','strikethrough', 'clear']],
    ['para', ['style','fontname','fontsize', 'paragraph', 'undo','redo']],
    ['insert', ['link']]
  ]
}
		    });
		  });
                $(t).siblings().hide();
                $('#alert-tombol').toggleClass('info danger').html('klik tombol <i class="fa fa-check"></i> setelah selesai edit text pada banner');
                $('#properties').hide();
                $('.box-body').hide();
    	 	}else{
    	 		 $('#editorWrapper').summernote('destroy');
                  $(t).siblings().show();
                  $('#alert-tombol').toggleClass('info danger').html('klik tombol <i class="fa fa-pencil"></i> untuk mengedit text pada banner');
                  $('#properties').show();
                  $('.box-body').show();
                  saveTemplate();
    	 	}

            isEditMode=!isEditMode;

}

function changeImage(value, dom){

    console.log(dom);
    console.log(value);    
    dom.attr('src', value);
}

function changeRating(value, dom){

    dom.children().remove();

    var star = Math.floor(value);
    var starO = 5 - Math.ceil(value);
    var starHalf = 5 - star - starO;

    for (i = 0; i < star; i++) { 
    dom.append('<span class="fa fa-star ">');
    }

    for (i = 0; i < starHalf; i++) { 
    dom.append('<span class="fa fa-star-half-o ">');
    }

    for (i = 0; i < starO; i++) { 
    dom.append('<span class="fa fa-star-o ">');
    }


}

function toogleElement(check, dom){

    console.log(check);
    console.log(dom);

    dom.toggle( check );
}

function changeText(value, dom){
    console.log(value);
    dom.html(value);
}

function isUsingAutoresponder(value, dom, dom2){

    dom.toggle(value);
    value==true ? dom2.val('tidak') : dom2.val('ya');
}

function changeValue(value, dom){
    dom.val(value);
}

function changeActionUrl(value, dom){
    dom.attr('href', '/track/link/?to='+value);
}

function changeFormActionUrl(value, dom){
    dom.attr('action', value);
}


</script>
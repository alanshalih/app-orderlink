<!DOCTYPE html>

	<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
	<!--[if IE 7]><html class="lt-ie9 lt-ie8"> <![endif]-->
	<!--[if IE 8]><html class="lt-ie9"> <![endif]-->
	<!--[if gt IE 8]><!--> 
	
<html>

	<!--<![endif]-->
	<!-- Head -->
	<head>

		<!-- Meta -->
		<meta charset="utf-8">

		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">


		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


		<!-- Title -->
		<title>{{$getcampaign->title}}</title>

		<meta property="og:url" content="{{$getcampaign->url}}">

		<!-- Short-Cut Icon -->
		<link rel="shortcut icon" href="#" />



		<!-- Bootstrap Style Sheet Version - 3.3.6 -->
		<link href="/modal/css/bootstrap.min.css" rel="stylesheet" media="all">

		
  <link rel="stylesheet" href="/css/font-awesome/css/font-awesome.min.css" type="text/css" />


			<!-- Facebook Pixel Code -->
			     @if(isset($getcampaign->pixel->pixel_id))
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '{{$getcampaign->pixel->pixel_id}}');
		fbq('track', 'PageView');
		{!! $getcampaign->pixel_event !!}

		</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id={{$getcampaign->pixel->pixel_id}}&ev=PageView&noscript=1"
		/></noscript>
		<!-- DO NOT MODIFY -->
		@endif


		<style>


				
		.modal.rbm_bottom_center {
		  top: auto;
		  overflow: visible; 
		}

		.modal.rbm_bottom_left {
		  top: auto;
		  overflow: visible; 
		}

		.modal.rbm_bottom_right {
		  top: auto;
		  overflow: visible; 
		}

		.modal.rbm_top_center {
		  bottom: auto;
		  overflow: visible; 
		}

		.modal.rbm_top_left {
		  bottom: auto;
		  overflow: visible; 
		}

		.modal.rbm_top_right {
		  bottom: auto;
		  overflow: visible; 
		}

* {
  box-sizing: border-box;
}

.openshare-list {
  bottom: 0;
  left: 0;
  margin: 0;
  padding: 0;
  position: fixed;
  width: 100%;
  z-index: 999;
}

.openshare-list__item {
  display: block;
  float: left;
  width: 80%;
}

.openshare-list__item--count {
  background: #fff;
  color: #000;
  display: block;
  float: left;
  font-size: 12px;
  font-weight: normal;
  padding: 5px;
  text-align: center;
  width: 20%;
}

.openshare-list__count {
  display: block;
  font-size: 12px;
  font-weight: bold;
  line-height: 1.6;
  min-height: 20px;
  text-align: center;
}

.openshare-list__link, .openshare-list__link--twitter, .openshare-list__link--linkedin, .openshare-list__link--facebook, .openshare-list__link--google {
  transition: .3s;
  color: #fff;
  display: block;
  padding: 15px 15px 10px;
  text-align: center;
  text-decoration: none;
}
.openshare-list__link:hover, .openshare-list__link--twitter:hover, .openshare-list__link--linkedin:hover, .openshare-list__link--facebook:hover, .openshare-list__link--google:hover {
  border-bottom: 0;
}

.openshare-list__link--twitter {
  background: #55acee;
}
.openshare-list__link--twitter:hover {
  background: #3ea1ec;
}

.openshare-list__link--linkedin {
  background: #0077b5;
}
.openshare-list__link--linkedin:hover {
  background: #00669c;
}

.openshare-list__link--facebook {
  background: #3b5998;
}
.openshare-list__link--facebook:hover {
  background: #344e86;
}

.openshare-list__link--google {
  background: #dc4e41;
}
.openshare-list__link--google:hover {
  background: #d83a2b;
}

.openshare-list__icon {
  font-size: 1em;
}

@media (min-width: 768px) {
  .openshare-list {
    transform: translateY(-50%);
    bottom: auto;
    top: 50%;
    width: auto;
  }

  .openshare-list__item,
  .openshare-list__item--count {
    float: none;
    width: auto;
  }

  .openshare-list__count {
    min-height: 0;
  }

  .openshare-list__link, .openshare-list__link--twitter, .openshare-list__link--linkedin, .openshare-list__link--facebook, .openshare-list__link--google {
    padding: 25px 25px 20px;
  }
}
/**
 * This work is licensed under the Creative Commons
 * Attribution 3.0 Unported License. To view a copy
 * of this license, visit http://creativecommons.org/licenses/by/3.0/.
 *
 * Author: Girish Sharma <scrapmachines@gmail.com>
 */

/* Demo specific styles begin */


/* Demo specific styles end */

/* Loader with three blocks */
		* {margin: 0; padding: 0;}

.loader-fb {
  top: calc(45% - 5px);
  left: calc(50% - 5px);
  position: absolute !important;
}

#progress-wrap{
	  top: calc(58%);
   left: calc(50% - 5px);
  position: absolute !important;
 
}

.load-text{
	font-family: arial;
	margin-left: -50px;
}


.loader-fb, .loader-fb:before, .loader-fb:after {
  position: relative;
  display: inline-block;
  width: 20px;
  height: 50px;
  background-color: rgba(215,230,240,0.9);
  border: 1px solid rgb(215,230,240);
  z-index: 100000;
  content: ' ';
  margin-left: -5px;
  margin-right: -9px;
}
.loader-fb:before {
  top: -11px;
  left: -100%;
  animation: loading-fb .8s cubic-bezier(.4,.5,.6,1) infinite;
}
.loader-fb {
  animation: loading-fb-main .8s cubic-bezier(.4,.5,.6,1) .2s infinite;
}
.loader-fb:after {
  top: -11px;
  right: -100%;
  margin-top: 50%;
  animation: loading-fb .8s cubic-bezier(.4,.5,.6,1) .4s infinite;
}
@keyframes loading-fb {
  from {
    transform: scaleY(1.4);
    background-color: rgba(55,114,171,0.9);
    border: 1px solid rgb(55,114,171);
  }
}
@keyframes loading-fb-main {
  from {
    padding-top: 10px;
    padding-bottom: 10px;
    margin-top: -10px;
    background-color: rgba(55,114,171,0.9);
    border: 1px solid rgb(55,114,171);
  }
}

.rbm_sngl_cmrce_txt > .price {
color : white;
	}

		</style>

		<!-- Responsive Bootstrap Modal Popup Main Style Sheet -->
		<link href="/modal/css/responsive_bootstrap_modal_popup.css" rel="stylesheet" media="all">


	</head>

<body >

<!-- loader -->
<div class="loader">
<div class="loader-fb"></div>  

<div id="progress-wrap" >
 <div class="load-text">
 	 <span>Loading...</span>
  <span id="progress">100%</span>
 </div>
</div>
</div>



<div id="placeholder"></div>
<div id="stuffing"></div>


	<!--
	In this example we have used a link to Trigger the Modal.
	You can also use images, links, buttons etc to Trigger the Modal.
	-->
	  <div class="navbar " role="navigation">
        <div class="container">
           
        </div>
    </div>
	
	<!-- End of Trigger Part -->
	
	<!-- Responsive Bootstrap Modal Popup -->
	@foreach($getcampaign->templates as $template)

	<div id="{{$template->modalId}}" class="{{$template->modalClass}}" role="dialog" data-backdrop="{{$template->data_backdrop}}">

		<!-- Modal Dialog-->
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">

				{!! $template->template !!}

				

			</div> <!-- /Modal content-->
		</div> <!-- /Modal Dialog-->
	</div> <!-- .modal -->
	@endforeach


<ul class="openshare-list" style="display: none">
  <li class="openshare-list__item--count">
    <span class="openshare-list__count">
			    </span> shares
  </li>
  <li class="openshare-list__item">
    <a href="https://web.facebook.com/sharer/sharer.php?u={{$getcampaign->alias}}" class="openshare-list__link--facebook"   onclick="window.open(this.href, 'mywin',
'left=0,top=0,width=558,height=600,toolbar=1,resizable=0');return false;"  >



      <span class="socicon-facebook" style="color:white"><i class="fa fa-facebook"></i> 
      	<span id="cta-text"></span>
	 </span>
    </a>
  </li>
</ul>



    <!-- End Responsive Bootstrap Modal Popup -->

	<!-- jQuery Version - 1.12.4 -->
	<script src="/modal/js/jquery-1.12.4.min.js"></script>

	<!-- Bootstrap JS File Version - 3.3.6 -->
	<script src="/modal/js/bootstrap.min.js"></script>

	<!-- Responsive Bootstrap Modal Popup Main JS File -->
	<script src="/modal/js/responsive_bootstrap_modal_popup_min.js"></script>

	<script src="/libs/exitModal/exitModal.js"></script>

	<script>

		
		var templates = {!! json_encode($getcampaign->templates) !!};
		
		function letShowModal(){

		templates.forEach(function(item, index){

			if(item.pivot.event == 'onload')
			{
		
				  setTimeout(function(){ $('#'+item.modalId).modal({show:true}); }, item.pivot.time*1000);

			
					
			}
			else
			{

	        $(document).ready(function(){

	            var timer;

	            var exitModalParams = {
	                numberToShown: 5,
	               
	                callbackOnModalShown: function() {
                    timer = setTimeout(function(){
                        var click = $('#'+item.modalId).data('exitModal').modalClick;

                        if(!click)
                        window.location.href = item.pivot.redirectTo;
                    }, 4000)
                
                	},
	                callbackOnModalHide: function() {
	                    clearTimeout(timer);
	                }
	            }

	            setTimeout(function(){ $('#'+item.modalId).exitModal(exitModalParams);
	                if($('#'+item.modalId).data('exit-modal')) {
	                    $(".destroyed-state").hide();
	                    $(".initialized-state").show();
	                } }, 1000);
		        });
			
			}


		});

		if('{{$getcampaign->share_button}}'){
			var width = $(window).width(), height = $(window).height();

			if(width < 768){
				$("#cta-text").html(" Share on Facebook");
			}

			$(".openshare-list").css('display', 'block');
		}

	
		

		
			

		$.get( "https://graph.facebook.com?id={{$getcampaign->url}}", function( data ) {
			  console.log(data);
			  $(".openshare-list__count").html(data.share.share_count)
			});

		}
	




    </script>
@if(isset($getcampaign->google->google_id))

  <script type="text/javascript">
  /* <![CDATA[ */
  var google_conversion_id = {{$getcampaign->google->google_id}};
  var google_custom_params = window.google_tag_params;
  var google_remarketing_only = true;
  /* ]]> */
  </script>
  <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
  </script>
  <noscript>
  <div style="display:inline;">
  <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/{{$getcampaign->google->google_id}}/?guid=ON&amp;script=0"/>
  </div>
  </noscript>
  @endif


    <script>

    var getcampaign = {!! json_encode($getcampaign) !!};




    $(document).ready( function(){
	   $('#placeholder').html('<iframe onload="letShowModal()"  id="website" src="'+getcampaign.url+'" style="position:absolute;border:0;width:100%;height:100%;"></iframe>');

	   hideLoader();

	   	if('{{$getcampaign->stuffing_url}}'){
			  $('#stuffing').html('<iframe onload="letShowModal()"  id="website" src="'+getcampaign.stuffing_url+'" style="position:absolute;border:0;width:1px;height:1px;opacity:0;"></iframe>');
		}


	})

		var counter = document.getElementById("progress");

	function changeText(data){
	  counter.innerHTML = data;
	}

	function hideLoader(){
		$('.loader').hide();
	}


	var counter = document.getElementById("progress");

	function changeText(data){
	  counter.innerHTML = data;
	}

	var max = 1500;
	var init = 0;
	var add = 150;




	function loop(){
	  setTimeout(function(){ 
	  init=init+add;
	   
	  changeText((init/(max))*100+'%');
	   if(init<(max)){
	     loop();
	   }else{
	   	hideLoader();
	   }
	}, add);
	}

	loop();








    </script>



</body>

</html>

 <!-- End -->
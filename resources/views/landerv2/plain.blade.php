
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Starter Template for Bootstrap</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="/froala/css/froala_editor.css">
  <link rel="stylesheet" href="/froala/css/froala_style.css">
  <link rel="stylesheet" href="/froala/css/plugins/code_view.css">
  <link rel="stylesheet" href="/froala/css/plugins/colors.css">
  <link rel="stylesheet" href="/froala/css/plugins/emoticons.css">
  <link rel="stylesheet" href="/froala/css/plugins/image_manager.css">
  <link rel="stylesheet" href="/froala/css/plugins/image.css">
  <link rel="stylesheet" href="/froala/css/plugins/line_breaker.css">
  <link rel="stylesheet" href="/froala/css/plugins/quick_insert.css">
  <link rel="stylesheet" href="/froala/css/plugins/table.css">
  <link rel="stylesheet" href="/froala/css/plugins/file.css">
  <link rel="stylesheet" href="/froala/css/plugins/char_counter.css">
  <link rel="stylesheet" href="/froala/css/plugins/video.css">
  <link rel="stylesheet" href="/froala/css/plugins/emoticons.css">
  <link rel="stylesheet" href="/froala/css/plugins/fullscreen.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">

  <style>	
  .o-block{
  position: relative;
}
#overlay-button{
  position: absolute;
  top: 0px;
  right: 3px;
  z-index: 2;
}

#overlay-button > .btn {
  margin: 6px 3px;
}



</style>

    <!-- Custom styles for this template -->
  </head>

  <body>

    <nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse fixed-top">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="#">Orderlink Builder</a>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="#">Back</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#" data-toggle="modal" data-target="#insertblock">Tambah Blok</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" onclick="lookScript()" href="#">SEO</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" onclick="lookScript()" href="#">Simpan</a>
          </li>
         
        </ul>
    
      </div>
    </nav>
    <div style="margin-top:32px;">halo</div>
   <div id="block-wrapper">	
		
   </div>
    <!-- modal to insert block -->
    <div class="modal fade" id="insertblock">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title">Modal title</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      		<div class="row">
	      			
	      		</div>
				<div class="row">	
				<div class="col-md-3"><a href="#" onclick="insertTemplate(1)">	<img src="img/work-6.jpg" class="img-thumbnail"></a></div>
				<div class="col-md-3"><a href="#" onclick="insertTemplate(2)">	<img src="img/work-6.jpg" class="img-thumbnail"></a></div>
				</div>
	      </div>
	      <div class="modal-footer">
	        
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
  <script type="text/javascript" src="/froala/js/froala_editor.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/align.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/code_beautifier.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/code_view.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/colors.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/emoticons.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/draggable.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/font_size.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/font_family.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/image.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/image_manager.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/line_breaker.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/quick_insert.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/link.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/lists.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/paragraph_format.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/paragraph_style.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/video.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/table.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/url.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/emoticons.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/file.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/entities.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/inline_style.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/save.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/fullscreen.min.js"></script>
   <script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>



<script>	
var tempBlock = '';
		function insertTemplate(id){

		   $('div#block-wrapper').froalaEditor('destroy');

			if(id==1){
				$( "#block-wrapper" ).append( ` <div class="o-block">   <div class="jumbotron">
      <div class="container">
        <h1 class="display-3">Hello, world!</h1>
        <p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>
      </div>
    </div> </div>` );
			}else if(id == 2){
				$("#block-wrapper").append(`<section class="imagebg image--light cover bg--secondary">
                <div class="background-image-holder hidden-xs">
                    <img alt="background" src="img/promo-1.jpg" />
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-5">
                            <div>
                                <h1>Element Index</h1>
                                <p class="lead">
                                    Easily create your own blocks using Stack's modular and customizable range of elements.
                                </p>
                                <hr class="short">
                                <p>
                                    Do you have an idea or suggestion for elements that
                                    <br /> could improve Stack?
                                    <a href="mailto:ask@mrare.co?subject=Stack Suggestion">Contact Us &rarr;</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>`);
			}

		  $('#block-wrapper').froalaEditor({
	        toolbarInline: true,
	        paragraphStyles: {
	        class2: 'Class 2'
	      },


	      });

		   blockHover();

			
		}

		 
function hapusBlock(){
    tempBlock.remove();
  }
    function moveUp(){
    tempBlock.prev().insertAfter(tempBlock);
  }

   function moveDown(){
    tempBlock.next().insertBefore(tempBlock);
  }

  function blockHover(){
 $( '.o-block' ).hover( 
    function(){
      tempBlock = $(this);
      console.log(tempBlock);

      var button = '<div id="overlay-button"><a  onclick="moveUp()" class="btn btn-outlined btn-primary text-white"><i class="fa fa-arrow-up"></i></a><a onclick="moveDown()"  class="btn btn-outlined btn-primary text-white"><i class="fa fa-arrow-down"></i></a><a onclick="clone()" class="btn btn-outlined btn-primary text-white"><i class="fa fa-copy"></i></a><a onclick="changeBackground()" class="btn btn-outlined btn-primary text-white"><i class="fa fa-gear"></i></a><a onclick="hapusBlock()" class="btn btn-outlined btn-primary text-white"><i class="fa fa-trash"></i></a></div>'
        

        if($( this ).children().length == 1){
          $(this).prepend(button);
        }
        

        
  }, function(){

    // $(this).children().first().remove();
        if($( this ).children().length == 2){
          $(this).children().first().remove();
        }
  

  } );
  }

  function clone(){
    // console.log(tempBlock);
     if(tempBlock.children().length == 2){
          tempBlock.children().first().remove();
          tempBlock.clone().insertBefore( tempBlock );
          blockHover();

        }
    // 
  }

  function lookScript(){
  	  console.log($('#block-wrapper').froalaEditor('html.get'));
  }
</script>

  </body>
</html>

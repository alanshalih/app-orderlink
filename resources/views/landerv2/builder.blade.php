
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0"/>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/bootstrap/dist/css/bootstrap.min.css" type="text/css" />
  <link rel="stylesheet" href="/froala/css/froala_editor.css">
  <link rel="stylesheet" href="/froala/css/froala_style.css">
  <link rel="stylesheet" href="/froala/css/plugins/code_view.css">
  <link rel="stylesheet" href="/froala/css/plugins/colors.css">
  <link rel="stylesheet" href="/froala/css/plugins/emoticons.css">
  <link rel="stylesheet" href="/froala/css/plugins/image_manager.css">
  <link rel="stylesheet" href="/froala/css/plugins/image.css">
  <link rel="stylesheet" href="/froala/css/plugins/line_breaker.css">
  <link rel="stylesheet" href="/froala/css/plugins/quick_insert.css">
  <link rel="stylesheet" href="/froala/css/plugins/table.css">
  <link rel="stylesheet" href="/froala/css/plugins/file.css">
  <link rel="stylesheet" href="/froala/css/plugins/char_counter.css">
  <link rel="stylesheet" href="/froala/css/plugins/video.css">
  <link rel="stylesheet" href="/froala/css/plugins/emoticons.css">
  <link rel="stylesheet" href="/froala/css/plugins/fullscreen.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">

  <style>
      body {
          text-align: center;
          margin: 0;
      }

 

      <style>
  .class1 {
    text-align: center;
    color: red;
  }

  .class2 {
    height: 100vh;
    background-color: black;
    color: white;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  footer{   
    position: absolute;
    background-color: black;
    width: 100%;
    height: 200px;
    margin-top: -50px;
    z-index: 100000;
  }
</style>
  </style>
</head>

<body>
  <div id="editor">
      <div id='edit'>
        <h1>Klik to Edit</h1>
        <p> ini adalah text</p>
          <h3>Click here to edit the content</h3>
  <p><img id="edit" class="fr-fil fr-dib" src="https://www.froala.com/assets/editor/docs/photo14.jpg" alt="Old Clock" width="300"/></p>
  <p>The image can be dragged only between blocks and not inside them.</p>
  <div class="fr-fil fr-dib"> 
  <p> ini blok</p><p> block 2</p></div>
        <br>  
        <br>  
        <footer>  

        </footer>
      </div>
  </div>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
  <script type="text/javascript" src="/froala/js/froala_editor.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/align.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/code_beautifier.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/code_view.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/colors.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/emoticons.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/draggable.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/font_size.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/font_family.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/image.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/image_manager.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/line_breaker.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/quick_insert.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/link.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/lists.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/paragraph_format.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/paragraph_style.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/video.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/table.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/url.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/emoticons.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/file.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/entities.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/inline_style.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/save.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/fullscreen.min.js"></script>

  <script>
    $(function(){
      $('#edit').froalaEditor({
        toolbarInline: true,
        paragraphStyles: {
        class2: 'Class 2'
      },


      });

        console.log($('#edit').froalaEditor('html.get'));
    });

  </script>
<script>
 </script>
</body>
</html>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Stack Multipurpose HTML Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Site Description Here">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="/stack/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/stack/css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/stack/css/socicon.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/stack/css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/stack/css/flickity.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/stack/css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/stack/css/theme.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/stack/css/custom.css" rel="stylesheet" type="text/css" media="all" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i%7CMaterial+Icons" rel="stylesheet">
        <link rel="stylesheet" href="/froala/css/froala_editor.css">
  <link rel="stylesheet" href="/froala/css/froala_style.css">
  <link rel="stylesheet" href="/froala/css/plugins/code_view.css">
  <link rel="stylesheet" href="/froala/css/plugins/colors.css">
  <link rel="stylesheet" href="/froala/css/plugins/emoticons.css">
  <link rel="stylesheet" href="/froala/css/plugins/image_manager.css">
  <link rel="stylesheet" href="/froala/css/plugins/image.css">
  <link rel="stylesheet" href="/froala/css/plugins/line_breaker.css">
  <link rel="stylesheet" href="/froala/css/plugins/quick_insert.css">
  <link rel="stylesheet" href="/froala/css/plugins/table.css">
  <link rel="stylesheet" href="/froala/css/plugins/file.css">
  <link rel="stylesheet" href="/froala/css/plugins/char_counter.css">
  <link rel="stylesheet" href="/froala/css/plugins/video.css">
  <link rel="stylesheet" href="/froala/css/plugins/emoticons.css">
  <link rel="stylesheet" href="/froala/css/plugins/fullscreen.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">
    <style>	
  .o-block{
  position: relative;
}
#overlay-button{
  position: absolute;
  top: 2px;
  right: 5px;
  z-index: 2;

}

#overlay-button > .btn-group > .btn{
	border-color: #4a90e2;
	background-color: #4a90e2;
}
#overlay-button > .btn-group > .btn > i {

color : #fff;
}
    </style>
  </head>
  <body class=" ">
    <!--end of notification-->
    <div class="nav-container">
      <div class="bar bar--sm visible-xs">
        <div class="container">
          <div class="row">
            <div class="col-xs-3 col-sm-2">
              <a href="index.html">
              <img class="logo logo-dark" alt="logo" src="img/logo-dark.png" />
              <img class="logo logo-light" alt="logo" src="img/logo-light.png" />
              </a>
            </div>
            <div class="col-xs-9 col-sm-10 text-right">
              <a href="#" class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs">
              <i class="icon icon--sm stack-interface stack-menu"></i>
              </a>
            </div>
          </div>
          <!--end of row-->
        </div>
        <!--end of container-->
      </div>
      <!--end bar-->
      <nav id="menu1" class="bar  bar--sm bar-1 hidden-xs hidden-sm ">
        <div class="container">
          <div class="row">
            <div class="col-md-1 col-sm-2 hidden-xs">
              <div class="bar__module">
                <a href="index.html">
       <!--          <img class="logo logo-dark" alt="logo" src="img/logo-dark.png" />
                <img class="logo logo-light" alt="logo" src="img/logo-light.png" /> -->

                </a>
              </div>
              <!--end module-->
            </div>
            <div class="col-md-11 col-sm-12 text-right text-left-xs text-left-sm">
              <div class="bar__module">
                
              </div>
              <!--end module-->
              <div class="bar__module">
       <!--          <a class="btn btn--sm type--uppercase" href="#">
                <span class="btn__text">
                Try Builder
                </span>
                </a> -->
                <div class="modal-instance">
                    <a class="btn modal-trigger btn--sm btn--primary type--uppercase" href="#">
                <span class="btn__text">
                Tambah Blok
                </span>
                </a>
                    <div class="modal-container">
                      <div class="modal-content">
                        <section class="imageblock feature-large bg--white border--round ">
                        
            <section class=" ">
                <div class="container">
                    <div class="row">
                        <div class="masonry">
                            <div class="masonry-filter-container text-center">
                                <span>Category:</span>
                                <div class="masonry-filter-holder">
                                    <div class="masonry__filters" data-filter-all-text="All Categories"></div>
                                </div>
                            </div>
                            <!--end masonry filters-->
                            <div class="masonry__container">
                                <div class="masonry__item col-sm-3 text-center" data-masonry-filter="Cover">
                                    <div class="project-thumb">
                                        <a href="#" onclick="insertTemplate(1)">
                                            <img alt="Image" class="border--round" src="/stack/variant/img/sections/variant-cover-features-1.jpg" />
                                        </a>
                                        <h4>Cover Features 1</h4>
                                    </div>
                                </div>
                                <div class="masonry__item col-sm-3 text-center" data-masonry-filter="Cover">
                                    <div class="project-thumb">
                                        <a href="#" onclick="insertTemplate(2)">
                                            <img alt="Image" class="border--round" src="/stack/variant/img/sections/variant-cover-features-2.jpg" />
                                        </a>
                                        <h4>Cover Features 2</h4>
                                    </div>
                                </div>
                                <div class="masonry__item col-sm-3 text-center" data-masonry-filter="Cover">
                                    <div class="project-thumb">
                                        <a href="#" onclick="insertTemplate(3)">
                                            <img alt="Image" class="border--round" src="/stack/variant/img/sections/variant-cover-text-1.jpg" />
                                        </a>
                                        <h4>Cover Text 1</h4>
                                    </div>
                                </div>
                                <div class="masonry__item col-sm-3 text-center" data-masonry-filter="Cover">
                                    <div class="project-thumb">
                                        <a href="#" onclick="insertTemplate(4)">
                                            <img alt="Image" class="border--round" src="/stack/variant/img/sections/variant-cover-text-2.jpg" />
                                        </a>
                                        <h4>Cover Text 2</h4>
                                    </div>
                                </div>
                                <div class="masonry__item col-sm-3 text-center" data-masonry-filter="Cover">
                                    <div class="project-thumb">
                                        <a href="#" onclick="insertTemplate(5)">
                                            <img alt="Image" class="border--round" src="/stack/variant/img/sections/variant-cover-text-3.jpg" />
                                        </a>
                                        <h4>Cover Text 3</h4>
                                    </div>
                                </div>
                                  <div class="masonry__item col-sm-3 text-center" data-masonry-filter="Cover">
                                    <div class="project-thumb">
                                        <a href="#" onclick="insertTemplate(6)">
                                            <img alt="Image" class="border--round" src="/stack/variant/img/sections/variant-cover-text-4.jpg" />
                                        </a>
                                        <h4>Cover Text 4</h4>
                                    </div>
                                </div>
                                    <div class="masonry__item col-sm-3 text-center" data-masonry-filter="Cover">
                                    <div class="project-thumb">
                                        <a href="#" onclick="insertTemplate(7)">
                                            <img alt="Image" class="border--round" src="/stack/variant/img/sections/variant-cover-text-5.jpg" />
                                        </a>
                                        <h4>Cover Text 5</h4>
                                    </div>
                                </div>
                                
                                <!--end item-->
                              
                                <!--end item-->
                            </div>
                            <!--end of masonry container-->
                        </div>
                        <!--end masonry-->
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
                          <!--end of container-->
                        </section>
                      </div>
                    </div>
                  </div>
              </div>
              <!--end module-->
            </div>
          </div>
          <!--end of row-->
        </div>
        <!--end of container-->
      </nav>
      <!--end bar-->
    </div>
    <div class="main-container">
  
		
    </div>
    <!--<div class="loader"></div>-->
    <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
    <i class="stack-interface stack-up-open-big"></i>
    </a>
    <script src="/stack/js/jquery-3.1.1.min.js"></script>
    <script src="/stack/js/flickity.min.js"></script>
    <script src="/stack/js/easypiechart.min.js"></script>
    <script src="/stack/js/parallax.js"></script>
    <script src="/stack/js/typed.min.js"></script>
    <script src="/stack/js/datepicker.js"></script>
    <script src="/stack/js/isotope.min.js"></script>
    <script src="/stack/js/ytplayer.min.js"></script>
    <script src="/stack/js/lightbox.min.js"></script>
    <script src="/stack/js/granim.min.js"></script>
    <script src="/stack/js/countdown.min.js"></script>
    <script src="/stack/js/twitterfetcher.min.js"></script>
    <script src="/stack/js/spectragram.min.js"></script>
    <script src="/stack/js/smooth-scroll.min.js"></script>
    <script src="/stack/js/scripts.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
  <script type="text/javascript" src="/froala/js/froala_editor.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/align.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/code_beautifier.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/code_view.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/colors.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/emoticons.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/draggable.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/font_size.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/font_family.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/image.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/image_manager.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/line_breaker.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/quick_insert.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/link.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/lists.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/paragraph_format.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/paragraph_style.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/video.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/table.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/url.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/emoticons.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/file.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/entities.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/inline_style.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/save.min.js"></script>
  <script type="text/javascript" src="/froala/js/plugins/fullscreen.min.js"></script>

<script>	
var tempBlock = '';
		function insertTemplate(id){

		   $('div#block-wrapper').froalaEditor('destroy');

	     if(id == 1){
				$("#block-wrapper").append(`<div class="o-block"><section class="cover cover-features imagebg space--lg" data-overlay="2">
                <div class="background-image-holder" style="background: url(&quot;img/landing-24.jpg&quot;); opacity: 1;">
          
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9 col-md-7">
                            <h1>
                                Build stylish, lean sites with Stack
                            </h1>
                            <p class="lead">
                                Stack offers a clean and contemporary look to suit a range of purposes from corporate, tech startup, marketing site to digital storefront.
                            </p>
                            <a class="btn btn--primary type--uppercase" href="#">
                                <span class="btn__text">
                                    View The Demos
                                </span>
                            </a>
                        </div>
                    </div>
                    <!--end of row-->
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="feature feature-1 boxed boxed--border bg--white">
                                <h5>Built for teams</h5>
                                <p>
                                    Ideal for large and small organisations
                                </p>
                                <a href="#">
                                    Learn More
                                </a>
                            </div>
                            <!--end feature-->
                        </div>
                        <div class="col-sm-4">
                            <div class="feature feature-1 boxed boxed--border bg--white">
                                <h5>Modern Aesthetic</h5>
                                <p>
                                    A highly adaptable look that's simple
                                </p>
                                <a href="#">
                                    Learn More
                                </a>
                            </div>
                            <!--end feature-->
                        </div>
                        <div class="col-sm-4">
                            <div class="feature feature--featured feature-1 boxed boxed--border bg--white">
                                <h5>Beautiful markup</h5>
                                <p>
                                    Following BEM conventions for readability
                                </p>
                                <a href="#">
                                    Learn More
                                </a>
                                <span class="label">Hot</span>
                            </div>
                            <!--end feature-->
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section></div>`);
			}
      else if(id == 2){
        $("#block-wrapper").append(`<div class="o-block"><section class="cover cover-features imagebg space--lg" data-overlay="2">
                <div class="background-image-holder" style="background: url(&quot;img/landing-23.jpg&quot;); opacity: 1;">
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9 col-md-7">
                            <h1>
                                Build stylish, lean sites with Stack
                            </h1>
                            <p class="lead">
                                Stack offers a clean and contemporary look to suit a range of purposes from corporate, tech startup, marketing site to digital storefront.
                            </p>
                            <a class="btn btn--primary type--uppercase" href="#">
                                <span class="btn__text">
                                    View The Demos
                                </span>
                            </a>
                        </div>
                    </div>
                    <!--end of row-->
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="feature feature-2 boxed boxed--border bg--white">
                                <i class="icon icon-Clock-Back color--primary"></i>
                                <div class="feature__body">
                                    <p>
                                        Save time with a multitude of styled components designed to showcase your content
                                    </p>
                                </div>
                            </div>
                            <!--end feature-->
                        </div>
                        <div class="col-sm-4">
                            <div class="feature feature-2 boxed boxed--border bg--white">
                                <i class="icon icon-Duplicate-Window color--primary"></i>
                                <div class="feature__body">
                                    <p>
                                        Construct mockups or production-ready pages in-browser with Variant Page Builder
                                    </p>
                                </div>
                            </div>
                            <!--end feature-->
                        </div>
                        <div class="col-sm-4">
                            <div class="feature feature-2 boxed boxed--border bg--white">
                                <i class="icon icon-Life-Jacket color--primary"></i>
                                <div class="feature__body">
                                    <p>
                                        Take comfort in 6 months included support with a dedicated support forum
                                    </p>
                                </div>
                            </div>
                            <!--end feature-->
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section></div>`);
      }else if(id == 3){
        $("#block-wrapper").append(`<div class="o-block"><section class="cover height-80 imagebg text-center" data-overlay="3">
                <div class="background-image-holder" style="background: url(&quot;img/landing-3.jpg&quot;); opacity: 1;">
 
                </div>
                <div class="container pos-vertical-center">
                    <div class="row">
                        <div class="col-sm-12">
                            <h1>
                                Streamline your workflow with Stack
                            </h1>
                            <a class="btn btn--primary type--uppercase" href="#">
                                <span class="btn__text">
                                    View The Demos
                                </span>
                            </a>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section></div>`);
      }
      else if(id == 4){
        $("#block-wrapper").append(`<div class="o-block"><section class="cover height-80 imagebg text-center" data-overlay="3">
                <div class="background-image-holder" style="background: url(&quot;img/landing-8.jpg&quot;); opacity: 1;">
 
                </div>
                <div class="container pos-vertical-center">
                    <div class="row">
                        <div class="col-sm-8 col-md-7">
                            <h1>
                                Build stylish, lean sites with Stack
                            </h1>
                            <p class="lead">
                                Stack offers a clean and contemporary look to suit a range of purposes from corporate, tech startup, marketing site to digital storefront.
                            </p>
                            <a class="btn btn--primary type--uppercase" href="#">
                                <span class="btn__text">
                                    View The Demos
                                </span>
                            </a>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section></div>`);
      }     else if(id == 5){
        $("#block-wrapper").append(`<div class="o-block"><section class="cover height-80 imagebg text-center" data-overlay="3">
                <div class="background-image-holder" style="background: url(&quot;img/landing-1.jpg&quot;); opacity: 1;">
 
                </div>
               <div class="container pos-vertical-center">
                    <div class="row">
                        <div class="col-sm-8">
                            <img alt="Image" class="unmarg--bottom" src="img/headline-1.png" />
                            <h3>
                                Streamline your workflow with Stack.
                            </h3>
                            <a class="btn btn--primary type--uppercase" href="#">
                                <span class="btn__text">
                                    View The Demos
                                </span>
                            </a>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section></div>`);
      } else if(id == 6){
        $("#block-wrapper").append(`<div class="o-block"><section class="imageblock switchable height-100">
                <div class="imageblock__content col-md-6 col-sm-4 pos-right">
                    <div class="background-image-holder"  style="background: url(&quot;img/inner-7.jpg&quot;); opacity: 1;">
                    </div>
                </div>
                <div class="container pos-vertical-center">
                    <div class="row">
                        <div class="col-md-5 col-sm-7">
                            <h1>Streamline your workflow with Stack</h1>
                            <p class="lead">
                                Stack offers a clean and contemporary look to suit a range of purposes from corporate, tech startup, marketing site to digital storefront.
                            </p>
                            <a class="btn btn--primary type--uppercase" href="#">
                                <span class="btn__text">
                                    View The Demos
                                </span>
                            </a>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section></div>`);
      }else if(id == 6){
        $("#block-wrapper").append(`<div class="o-block">
                <section class="cover imagebg height-100 text-center" data-overlay="3">
                <div class="background-image-holder" style="background : url("img/landing-10.jpg")">
                </div>
                <div class="container pos-vertical-center">
                    <div class="row">
                        <div class="col-sm-9 col-md-8">
                            <h1>Streamline your workflow with Stack</h1>
                            <p class="lead">Stack offers a clean and contemporary look to suit a range of purposes from corporate, tech startup, marketing site to digital storefront.</p>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
                <div class="pos-absolute pos-bottom col-xs-12">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 text-left">
                                <div class="text-block">
                                    <h5>Teahupo'o Beach</h5>
                                    <span>French Polynesia</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section></div>`);
      }
      else if(id == 7){
        $("#block-wrapper").append(`<div class="o-block">
                <section class="cover imagebg height-100 text-center" data-overlay="3">
                <div class="background-image-holder" style="background : url('img/landing-10.jpg'); opacity:1;">
                </div>
                <div class="container pos-vertical-center">
                    <div class="row">
                        <div class="col-sm-9 col-md-8">
                            <h1>Streamline your workflow with Stack</h1>
                            <p class="lead">Stack offers a clean and contemporary look to suit a range of purposes from corporate, tech startup, marketing site to digital storefront.</p>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
                <div class="pos-absolute pos-bottom col-xs-12">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 text-left">
                                <div class="text-block">
                                    <h5>Teahupo'o Beach</h5>
                                    <span>French Polynesia</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section></div>`);
      }

		

		

		      $('#block-wrapper').froalaEditor({
	        toolbarInline: true,
	        paragraphStyles: {
	        class2: 'Class 2'
	      },


	      });

		      blockHover();

			
		}

		 
function hapusBlock(){
    tempBlock.remove();
  }
    function moveUp(){
    tempBlock.prev().insertAfter(tempBlock);
  }

   function moveDown(){
    tempBlock.next().insertBefore(tempBlock);
  }

  function blockHover(){
 $( '.o-block' ).hover( 
    function(){
      tempBlock = $(this);
      console.log(tempBlock);

      var button = `<div id="overlay-button"><div class="btn-group"><a  onclick="moveUp()" class="btn btn--xs  "><i class="fa fa-arrow-up"></i></a><a onclick="moveDown()"  class="btn btn--xs"><i class="fa fa-arrow-down"></i></a><a onclick="clone()" class="btn btn--xs"><i class="fa fa-copy"></i></a><a onclick="changeBackground()" class="btn btn--xs"><i class="fa fa-gear"></i></a><a onclick="hapusBlock()" class="btn btn--xs"><i class="fa fa-trash"></i></a></div></div>`;
        

        if($( this ).children().length == 1){
          $(this).prepend(button);
        }
        

        
  }, function(){

    // $(this).children().first().remove();
        if($( this ).children().length == 2){
          $(this).children().first().remove();
        }
  

  } );
  }

  function clone(){
    // console.log(tempBlock);
     if(tempBlock.children().length == 2){
          tempBlock.children().first().remove();
          tempBlock.clone().insertBefore( tempBlock );
          blockHover();

        }
    // 
  }

  function lookScript(){
  	  console.log($('#block-wrapper').froalaEditor('html.get'));
  }
</script>

  </body>
</html>
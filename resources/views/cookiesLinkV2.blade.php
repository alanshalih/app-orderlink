<!DOCTYPE html>

	<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
	<!--[if IE 7]><html class="lt-ie9 lt-ie8"> <![endif]-->
	<!--[if IE 8]><html class="lt-ie9"> <![endif]-->
	<!--[if gt IE 8]><!--> 
	
<html>

	<!--<![endif]-->
	<!-- Head -->
	<head>

		<!-- Meta -->
		<meta charset="utf-8">

		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">


		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


		<!-- Title -->
		<title>{{$getcampaign->title}}</title>

		<meta property="og:url" content="{{$getcampaign->url}}">

		<!-- Short-Cut Icon -->
		<link rel="shortcut icon" href="#" />

		<!-- Google Web Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600,700,900' rel='stylesheet' type='text/css'>
		
		<!-- Font Awesome Version - 4.6.3 -->
		<link href="/modal/font-awesome-4.6.3/css/font-awesome.min.css" rel="stylesheet" media="all">

		<!-- Bootstrap Style Sheet Version - 3.3.6 -->
		<link href="/modal/css/bootstrap.min.css" rel="stylesheet" media="all">


		<style>


				
		.modal.rbm_bottom_center {
		  top: auto;
		  overflow: visible; 
		}

		.modal.rbm_bottom_left {
		  top: auto;
		  overflow: visible; 
		}

		.modal.rbm_bottom_right {
		  top: auto;
		  overflow: visible; 
		}

		.modal.rbm_top_center {
		  bottom: auto;
		  overflow: visible; 
		}

		.modal.rbm_top_left {
		  bottom: auto;
		  overflow: visible; 
		}

		.modal.rbm_top_right {
		  bottom: auto;
		  overflow: visible; 
		}



		</style>

		<!-- Responsive Bootstrap Modal Popup Main Style Sheet -->
		<link href="/modal/css/responsive_bootstrap_modal_popup.css" rel="stylesheet" media="all">


	</head>

<body >
<div id="placeholder"></div>


	<!--
	In this example we have used a link to Trigger the Modal.
	You can also use images, links, buttons etc to Trigger the Modal.
	-->
	  <div class="navbar " role="navigation">
        <div class="container">
           
        </div>
    </div>
	
	<!-- End of Trigger Part -->
	
	<!-- Responsive Bootstrap Modal Popup -->
	@foreach($templates as $template)

	<div id="{{$template->modalId}}" class="{{$template->modalClass}}" role="dialog" data-backdrop="{{$template->data_backdrop}}">

		<!-- Modal Dialog-->
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">

				{!! $template->template !!}

				

			</div> <!-- /Modal content-->
		</div> <!-- /Modal Dialog-->
	</div> <!-- .modal -->
	@endforeach



    <!-- End Responsive Bootstrap Modal Popup -->

	<!-- jQuery Version - 1.12.4 -->
	<script src="/modal/js/jquery-1.12.4.min.js"></script>

	<!-- Bootstrap JS File Version - 3.3.6 -->
	<script src="/modal/js/bootstrap.min.js"></script>

	<!-- Responsive Bootstrap Modal Popup Main JS File -->
	<script src="/modal/js/responsive_bootstrap_modal_popup_min.js"></script>

	<script src="/libs/exitModal/exitModal.js"></script>

	<script>
		var templates = {!! json_encode($templates) !!};
		
		templates.forEach(function(item, index){

			if(item.pivot.event == 'onload')
			{
					setTimeout(function(){ $('#'+item.modalId).modal({show:true}); }, item.pivot.time*1000);
			}
			else
			{

	        $(document).ready(function(){

	            var timer;

	            var exitModalParams = {
	                numberToShown: 5,
	               
	                callbackOnModalShown: function() {
                    timer = setTimeout(function(){
                        var click = $('#'+item.modalId).data('exitModal').modalClick;

                        if(!click)
                        window.location.href = item.pivot.redirectTo;
                    }, 4000)
                
                	},
	                callbackOnModalHide: function() {
	                    clearTimeout(timer);
	                }
	            }

	            setTimeout(function(){ $('#'+item.modalId).exitModal(exitModalParams);
	                if($('#'+item.modalId).data('exit-modal')) {
	                    $(".destroyed-state").hide();
	                    $(".initialized-state").show();
	                } }, 1000);
		        });
			
			}


		});

	
	




    </script>

    <script>

    // var getcampaign = {!! json_encode($getcampaign) !!};


    $(document).ready( function(){
   $('#placeholder').html('<iframe id="website" src="'+getcampaign.url+'" style="position:absolute;border:0;width:100%;height:100%;"></iframe>');
	})
    </script>



</body>

</html>

 <!-- End -->
@extends('layouts.app')

@section('title','Dashboard')

@section('content')
<!-- ############ PAGE START-->
<div class="row-col">
    <div class="col-lg b-r">
        <div class="row no-gutter">
        <div class="box info">
            <div class="box-body">
               @if(count(Auth::user()->roles)==0)
 
                <p class="text-md profile-status pull-right"><a data-turbolinks="false"  href="/activate/user" class="btn btn-fw white text-info">AKTIFKAN PRO VERSION</a></p>
              @endif
              
              <div class="clear">
                <h3>Welcome Back, {{Auth::user()->name}}!</h3>
                @if(count(Auth::user()->roles)>0)
                <small class="block text-muted">{{Auth::user()->roles[0]->display_name}}</small>
                @endif
                    @if(count(Auth::user()->roles)==0)
               <span class="text-WHITE">Sudah Beli OrderLink di IDAFF? Silakan klik tombol <strong class="text-u-l ">'AKTIFKAN PRO VERSION'</strong>.</span>
              @endif



                
              </div>
            </div>
          </div>

          
            <div class="col-xs-6 col-sm-3 b-r b-b">
                <div class="padding">
            <!--         <div>
                        <span class="pull-right"><i class="fa fa-caret-up text-primary m-y-xs"></i></span>
                        <span class="text-muted l-h-1x"><i class="ion-ios-grid-view text-muted"></i></span>
                    </div> -->
                    <div class="text-center">
                        <h2 class="text-center _600">{{$klikToday}}</h2>
                        <p class="text-muted m-b-md">Klik Hari ini</p>
                  
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3 b-r b-b">
                <div class="padding">
     <!--                <div>
                        <span class="pull-right"><i class="fa fa-caret-up text-primary m-y-xs"></i></span>
                        <span class="text-muted l-h-1x"><i class="ion-ios-grid-view text-muted"></i></span>
                    </div> -->
                    <div class="text-center">
                        <h2 class="text-center _600">{{$klikBulanini ?? 0}}</h2>
                        <p class="text-muted m-b-md">Klik Bulan ini</p>
                  
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3 b-r b-b">
                <div class="padding">
   <!--                  <div>
                        <span class="pull-right"><i class="fa fa-caret-up text-primary m-y-xs"></i></span>
                        <span class="text-muted l-h-1x"><i class="ion-document text-muted"></i></span>
                    </div> -->
                    <div class="text-center">
                        <h2 class="text-center _600">{{Redis::get('user.'.Auth::id().'.Limiter') - $klikBulanini}}</h2>
                        <p class="text-muted m-b-md">Sisa Kredit Klik</p>
                       
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3 b-b">
                <div class="padding">
<!--                     <div>
                        <span class="pull-right"><i class="fa fa-caret-down text-danger m-y-xs"></i></span>
                        <span class="text-muted l-h-1x"><i class="ion-pie-graph text-muted"></i></span>
                    </div> -->
                    <div class="text-center">
                        <h2 class="text-center _600">{{$totalLink}}</h2>
                        <p class="text-muted m-b-md">Total Link</p>
                       
                    </div>
                </div>
            </div>
            @if($dompet > 0)
            <div class="col-xs-6 col-sm-3 b-r b-b">
                <div class="padding">
            <!--         <div>
                        <span class="pull-right"><i class="fa fa-caret-up text-primary m-y-xs"></i></span>
                        <span class="text-muted l-h-1x"><i class="ion-ios-grid-view text-muted"></i></span>
                    </div> -->
                    <div class="text-center">
                        <h2 class="text-center _600">Rp.{{$dompet*0.2}},-</h2>
                        <p class="text-muted m-b-md">Sisa Dompet Anda</p>
                  
                    </div>
                </div>
            </div>
              <div class="col-xs-6 col-sm-3 b-r b-b">
                <div class="padding">
            <!--         <div>
                        <span class="pull-right"><i class="fa fa-caret-up text-primary m-y-xs"></i></span>
                        <span class="text-muted l-h-1x"><i class="ion-ios-grid-view text-muted"></i></span>
                    </div> -->
                    <div class="text-center">
                        <h2 class="text-center _600">{{$dompet}}</h2>
                        <p class="text-muted m-b-md">Sisa Klik Tambahan</p>
                  
                    </div>
                </div>
            </div>
            @endif

           
        </div>
        <div class="padding">
            <div class="row">
                <div class="col-sm-4">
                <panel-dashboard-chart id="pixelink" title="Pixelink Stat"  description="tracking and retargeting tools" color="#2196f3" warnapanel="info" :hit="{{$pixelinkStat}}" ></panel-dashboard-chart>
                    <!-- <chart-panel title="Pixelink" description="tracking and retargeting tools" color="#2196f3" warnapanel="info" :hit="{{$pixelinkStat}}"></chart-panel> -->
                </div>
                <div class="col-sm-4">
                    <!-- <chart-panel title="Orderlink" description="link untuk kontak pribadi anda" color="#ef193c" warnapanel="danger" :hit="{{$orderlinkStat}}"></chart-panel>                 -->
                      <panel-dashboard-chart id="orderlink"  title="Orderlink Stat"  description="link untuk kontak pribadi anda" color="#ef193c" warnapanel="danger" :hit="{{$orderlinkStat}}"></panel-dashboard-chart>
                </div>
                <div class="col-sm-4">
                    <!-- <chart-panel title="ContentLink" description="auto free traffic booster" color='#22b66e' warnapanel="success" :hit="{{$contentlinkStat}}"></chart-panel>                 -->
                      <panel-dashboard-chart id="contentlink"  title="Contentlink Stat" description="auto free traffic booster" color='#22b66e' warnapanel="success" :hit="{{$contentlinkStat}}"></panel-dashboard-chart>
                </div>
            </div>

                        
        </div>
    </div>
    <div class="col-lg w-lg w-auto-md white bg">
        <div>

            <div class="p-a">
                <h6 class="text-muted m-a-0">Notification</h6>
            </div>
            <div class="streamline streamline-theme m-b">
                @foreach (Auth::user()->notifications as $notification)
                              
                
                      
              <div class="sl-item b-success">
                <div class="sl-content">
                  <div> <a href="{{$notification->data['link']}}" >{{$notification->data['text']}}</a>.</div>
                  <div class="sl-date text-muted">{{\Carbon\Carbon::createFromTimeStamp(strtotime($notification->created_at))->diffForHumans()}}</div>
                </div>
              </div>
                @endforeach
              
            </div>
        </div>
    </div>
</div>

@endsection

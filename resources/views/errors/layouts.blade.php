
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>aside - Bootstrap 4 web application</title>
  <meta name="description" content="Responsive, Bootstrap, BS4" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- for ios 7 style, multi-resolution icon of 152x152 -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
  <link rel="apple-touch-icon" href="images/logo.png">
  <meta name="apple-mobile-web-app-title" content="Orderlink">
  <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="shortcut icon" sizes="196x196" href="/images/logo.png">
  
  <link rel="stylesheet" href="/css/bootstrap/dist/css/bootstrap.min.css" type="text/css" />

  <!-- build:css css/styles/app.min.css -->
  <link rel="stylesheet" href="/css/styles/app.css" type="text/css" />

</head>
<body>
  <div class="app" id="app">

<!-- ############ LAYOUT START-->

  <div class="p-a black">
        <div class="navbar" data-pjax>
              <a data-toggle="collapse" data-target="#navbar" class="navbar-item pull-right hidden-md-up m-a-0 m-l">
                <i class="ion-android-menu"></i>
              </a>
              <!-- brand -->
              <a href="index.html" class="navbar-brand">
              	<div data-ui-include="'images/logo.svg'"></div>
              	<img src="images/logo.png" alt="." class="hide">
              	<span class="hidden-folded inline">Orderlink</span>
              </a>
              <!-- / brand -->
      
              <!-- navbar collapse -->
              <div class="collapse navbar-toggleable-sm pull-right pull-none-xs" id="navbar">
                <!-- link and dropdown -->
                <ul class="nav navbar-nav text-info-hover" data-ui-nav>
                  <li class="nav-item">
                    <a href="#features" data-ui-scroll-to class="nav-link">
                      <span class="nav-text">Fitur</span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#demos" data-ui-scroll-to class="nav-link">
                      <span class="nav-text">Demo</span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/register" class="nav-link">
                      <span class="nav-text text-info">
                        Sign Up
                      </span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/login" class="nav-link">
                      <span class="btn btn-md rounded info">
                        Log in
                      </span>
                    </a>
                  </li>
                </ul>
                <!-- / link and dropdown -->
              </div>
              <!-- / navbar collapse -->
        </div>
  </div>

  <!-- content -->
  <div id="content" class="app-content" role="main">
    <div class="app-body">

<!-- ############ PAGE START-->

<div class="row-col amber h-v">
  <div class="row-cell v-m">
    <div class="text-center col-sm-6 offset-sm-3 p-y-lg">
      @yield('content')
    </div>
  </div>
</div>

<!-- ############ PAGE END-->

    </div>
  </div>
  <!-- / -->

  
<!-- ############ LAYOUT END-->
  </div>

<!-- endbuild -->
</body>
</html>

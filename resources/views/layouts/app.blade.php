

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Orderlink - Tracking and List Building Tool</title>
  <meta name="description" content="Retargeting, Tracking, List Building, Viral Campaign" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- for ios 7 style, multi-resolution icon of 152x152 -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
  <link rel="apple-touch-icon" href="/images/logo.png">
  <meta name="apple-mobile-web-app-title" content="Flatkit">
  <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="shortcut icon" sizes="196x196" href="/images/logo.png">
  
  <!-- style -->
  <link rel="stylesheet" href="/css/animate.css/animate.min.css" type="text/css" />
  <link rel="stylesheet" href="/css/glyphicons/glyphicons.css" type="text/css" />
  <link rel="stylesheet" href="/css/font-awesome/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="/css/material-design-icons/material-design-icons.css" type="text/css" />
  <link rel="stylesheet" href="/css/ionicons/css/ionicons.min.css" type="text/css" />
  <link rel="stylesheet" href="/css/simple-line-icons/css/simple-line-icons.css" type="text/css" />
  <link rel="stylesheet" href="/css/bootstrap/dist/css/bootstrap.min.css" type="text/css" />
     <link rel="stylesheet" href="/css/toastr.min.css">
         <link rel="stylesheet" href="/css/loader.css?id=1.0">
    <link rel="stylesheet" href="/libs/button-loader/buttonLoader.css?id=1.0">

  <!-- build:css css/styles/app.min.css -->
  <link rel="stylesheet" href="/css/styles/app.css" type="text/css" />
  <link rel="stylesheet" href="/css/styles/style.css" type="text/css" />
  <!-- endbuild -->
  <link href="/modal/css/responsive_bootstrap_modal_popup.css?11" rel="stylesheet" media="all">
  
  <link rel="stylesheet" href="/css/styles/font.css" type="text/css" />
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chartist/0.10.1/chartist.min.css"> -->
 

    <script src="/libs/jquery/dist/jquery.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/turbolinks/5.0.0/turbolinks.min.js"></script>
<!-- Bootstrap -->
  <script src="/libs/tether/dist/js/tether.min.js"></script>
  <script src="/libs/bootstrap/dist/js/bootstrap.min.js"></script>

      <script src="https://cdn.jsdelivr.net/clipboard.js/1.5.13/clipboard.min.js"></script>
    <script src="/libs/toastr/toastr.min.js"></script>
<!-- core -->
  <script src="/libs/PACE/pace.min.js"></script>
  <script src="/libs/blockUI/jquery.blockUI.js"></script>
  <script src="/libs/jscroll/jquery.jscroll.min.js"></script>


  @yield('head')
                        
  <script src="/scripts/ui-include.js"></script>
  <script src="/scripts/ui-device.js"></script>
  <script src="/scripts/ui-form.js"></script>
  <script src="/scripts/ui-modal.js"></script>
  <script src="/scripts/ui-nav.js"></script>
  <script src="/scripts/ui-list.js"></script>
  <script src="/scripts/ui-scroll-to.js"></script>
  <script src="/scripts/ui-toggle-class.js"></script>
  <script src="/scripts/ui-taburl.js"></script>
      <script src="/scripts/loader.js?id=1.0"></script>
    <script src="/libs/button-loader/jquery.buttonLoader.js?id=1.0"></script>

    <!-- include summernote css/js-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.js"></script>

<script src="/modal/js/jquery.touchSwipe.min.js"></script>
<script src="/modal/js/responsive_bootstrap_modal_popup_min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script src="https://momentjs.com/downloads/moment.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/chartist/0.10.1/chartist.min.js"></script> -->



</head>
<body>

  <div class="app" id="app">

<!-- ############ LAYOUT START-->

  <!-- aside -->
  <div id="aside" class="app-aside fade nav-dropdown black">
    <!-- fluid app aside -->
    <div class="navside dk" data-layout="column">
      <div class="navbar no-radius">
        <!-- brand -->
        <a href="/" class="navbar-brand">
          <div data-ui-include="https://res.cloudinary.com/ffsandi/image/upload/v1484557360/OrderLink/nama_program_di_menu.png"></div>
          <img src="https://res.cloudinary.com/ffsandi/image/upload/v1484557360/OrderLink/nama_program_di_menu.png" alt="." >
          <span class="hidden-folded inline hide">Orderlink</span>
        </a>
        <!-- / brand -->
      </div>
      <div data-flex class="hide-scroll">
          <nav class="scroll nav-stacked nav-stacked-rounded nav-color">
            
            <ul class="nav" data-ui-nav>
              
              <li class="nav-header hidden-folded">
                <span class="text-xs">Menu Utama</span>
              </li>
         <!--         <li>
                <a  data-turbolinks="false" href="/v2/orderlink" class="b-default">
                  <span class="nav-icon">
                    <i class="fa fa-exchange"></i>
                  </span>
                  <span class="nav-text">Ke Orderlink v2</span>
                </a>
              </li> -->
              <li>
                <a href="/home" class="b-default">
                  <span class="nav-icon  ">
                    <i class="ion-home"></i>
                  </span>
                  <span class="nav-text">Dashboard</span>
                </a>
              </li>
              <li>
                <a data-turbolinks="false" href="/pixelink" class="b-default">
                  <span class="nav-icon  ">
                    <i class="ion-ios-pie"></i>
                  </span>
                  <span class="nav-text">Pixel Link</span>
                </a>
              </li>
              <li>
                <a data-turbolinks="false" href="/orderlink" class="b-default">
                  <span class="nav-icon  ">
                    <i class="ion-android-cart"></i>
                  </span>
                  <span class="nav-text">Order Link</span>
                </a>
              </li>

                  <li>
                <a>
                  <span class="nav-caret">
                    <i class="fa fa-caret-down"></i>
                  </span>
                  <span class="nav-icon">
                    <i class="ion-ios-pricetags"></i>
                  </span>
                  <span class="nav-text">Content Link</span>
                </a>
                <ul class="nav-sub nav-mega nav-mega-3">
                  <li>
                    <a data-turbolinks="false" href="/contentlink/campaign" >
                      <span class="nav-text">Campaign</span>
                    </a>
                  </li>
                  <li>
                    <a data-turbolinks="false" href="/contentlink" >
                      <span class="nav-text">Template Saya</span>
                    </a>
                  </li>
                  <li>
                    <a data-turbolinks="false" href="/contentlink/template" >
                      <span class="nav-text">Bikin Baru</span>
                    </a>
                  </li>
                  
                </ul>
              </li>
               <li>
                <a href="/page" class="b-default">
                  <span class="nav-icon">
                    <i class="ion-flag"></i>
                  </span>
                  <span class="nav-text">Page Creator</span>
                </a>
              </li>
              <li>
                <a href="/viral-research" class="b-default">
                  <span class="nav-icon">
                    <i class="ion-search"></i>
                  </span>
                  <span class="nav-text">Social Viral Search</span>
                </a>
              </li>
              @role(('admin'))
              <li>
                <a href="/user" class="b-default">
                  <span class="nav-icon">
                    <i class="ion-person"></i>
                  </span>
                  <span class="nav-text">User</span>
                </a>
              </li>
               <li>
                <a href="/userv2"  class="b-default">
                  <span class="nav-icon">
                    <i class="ion-person"></i>
                  </span>
                  <span class="nav-text">User V2</span>
                </a>
              </li>
               <li>
                <a href="/admin/compose/notif"  class="b-default">
                  <span class="nav-icon">
                    <i class="ion-person"></i>
                  </span>
                  <span class="nav-text">Send Notif</span>
                </a>
              </li>
               <li>
                <a href="/admin/userdomain"  class="b-default">
                  <span class="nav-icon">
                    <i class="ion-person"></i>
                  </span>
                  <span class="nav-text">User Domain</span>
                </a>
              </li>

              @endrole
            
              <li class="nav-header hidden-folded m-t">
                <span class="text-xs">Menu Lain</span>
              </li>

               <li >
                <a data-turbolinks="false" href="/affiliate">
                  <span class="nav-icon">
                    <i class="ion-flash"></i>
                  </span>
                  <span class="nav-text">Affiliate</span>
                </a>
            
              </li>


              <li>
                <a >
                  <span class="nav-caret">
                    <i class="fa fa-caret-down"></i>
                  </span>
                  <span class="nav-icon">
                    <i class="ion-pin"></i>
                  </span>
                  <span class="nav-text">Asset</span>
                </a>
                <ul class="nav-sub">
                  <li>
                    <a href="/asset/pixel" >
                      <span class="nav-text">Pixel FB+Google</span>
                    </a>
                  </li>
                  <li>
                    <a href="/list" >
                      <span class="nav-text">List</span>
                    </a>
                  </li>
                </ul>
              </li>

               <li>
                <a href="/tutorial" class="b-default">
                  <span class="nav-icon">
                    <i class="ion-coffee"></i>
                  </span>
                  <span class="nav-text">Tutorial</span>
                </a>
              </li>

                <li>
                <a href="/webinar" class="b-default">
                  <span class="nav-icon">
                    <i class="ion-ios-film"></i>
                  </span>
                  <span class="nav-text">Webinar</span>
                </a>
              </li>

            
              <li>
                <a>
                  <span class="nav-caret">
                    <i class="fa fa-caret-down"></i>
                  </span>
                  <span class="nav-icon">
                    <i class="ion-ios-help-outline"></i>
                  </span>
                  <span class="nav-text">Support</span>
                </a>
                <ul class="nav-sub">
                  <li>
                    <a href="http://m.me/orderlinksupport" target="_blank">
                      <span class="nav-text">Kirim Tiket</span>
                    </a>
                  </li>
                  <li>
                    <a href="#" >
                      <span class="nav-text">F.A.Q</span>
                    </a>
                  </li>
                </ul>
              </li>

               <li>
                <a href="/setting" class="b-default">
                  <span class="nav-icon">
                    <i class="ion-ios-gear"></i>
                  </span>
                  <span class="nav-text">Setting</span>
                </a>
              </li>
          
            
             
            </ul>
          </nav>
      </div>
      <div data-flex-no-shrink>
        <div class="nav-fold dropup">
          <a data-toggle="dropdown">
              <div class="pull-left">
                <img src="{{Auth::user()->avatar ?? 'https://www.gravatar.com/avatar/'.md5(Auth::user()->email)}}" alt="..." class="w-40 img-circle">
              </div>
              <div class="clear hidden-folded p-x">
                <span class="block _500 text-muted">{{Auth::user()->name}}</span>
                <div class="progress-xxs m-y-sm lt progress">
                    <div class="progress-bar info" style="width: {{Redis::SCARD('user.'.Auth::id().'.progress')*100/5}}%;">
                    </div>
                </div>
              </div>
          </a>
          <div class="dropdown-menu w dropdown-menu-scale ">

            <a class="dropdown-item" href="#">
              <span>Profile</span>
            </a>
            <a class="dropdown-item" href="/setting">
              <span>Settings</span>
            </a>
            <a class="dropdown-item" href="#">
              <span>Inbox</span>
            </a>
            <a class="dropdown-item" href="#">
              <span>Message</span>
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">
              Need help?
            </a>
               <a class="dropdown-item" href="/logout" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Sign out</a>
              

               
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- / -->
  
  <!-- content -->
  <div id="content" class="app-content box-shadow-z2 pjax-container" role="main">
    <div class="app-header  black lt b-b">
         
          <div class="navbar" data-pjax>
                <a data-toggle="modal" data-target="#aside" class="navbar-item pull-left hidden-lg-up p-r m-a-0">
                  <i class="ion-navicon"></i>
                </a>
                <div class="navbar-item pull-left h5" id="pageTitle">@yield('title')</div>
                <!-- nabar right -->
                <ul class="nav navbar-nav pull-right">
             
                  <li class="nav-item dropdown pos-stc-xs">
                    <a class="nav-link clear" data-toggle="dropdown" onclick="markAsRead()">
                      <i class="ion-android-notifications-none w-24"></i>
                      @if(count(Auth::user()->unreadNotifications)>0)
                      <span class="label danger pos-rlt m-r-xs">{{count(Auth::user()->unreadNotifications)}} belum dibaca</span>
                      @endif

                    </a>
                
                    
                    <!-- dropdown -->
                    <div class="dropdown-menu pull-right w-xl animated fadeIn no-bg no-border no-shadow">
                        <div class="scrollable" style="max-height: 220px">
                          <ul class="list-group list-group-gap m-a-0">
                          @foreach (Auth::user()->notifications as $notification)
                              
                            <li class="list-group-item dark-white box-shadow-z0 b">
                              <span class="pull-left m-r">
                                <img src="{{$notification->data['image'] ?? '/images/a0.jpg'}}" alt="..." class="w-40 img-circle">
                              </span>
                              <a href="{{$notification->data['link']}}" target="_blank" class="clear block">
                                {{$notification->data['text']}}<br>
                                <small class="text-muted">{{\Carbon\Carbon::createFromTimeStamp(strtotime($notification->created_at))->diffForHumans()}}</small>
                              </a>
                            </li>
                        @endforeach
                          
                           
                          </ul>
                        </div>
                    </div>
                    <!-- / dropdown -->
                  </li>
                               <li class="nav-item dropdown">
                <a class="nav-link clear" data-toggle="dropdown">
                <span class="avatar w-32">
                <img id="profile-pic" src="{{Auth::user()->avatar ?? 'https://www.gravatar.com/avatar/'.md5(Auth::user()->email)}}" class="w-full rounded" alt="..."> 
                </span>
                </a>
                <div class="dropdown-menu w dropdown-menu-scale pull-right">
                  <a class="dropdown-item" href="profile.html">
                  <span>Profile</span>
                  </a>
                  <a class="dropdown-item" href="setting.html">
                  <span>Settings</span>
                  </a>
                  <a class="dropdown-item" href="app.inbox.html">
                  <span>Inbox</span>
                  </a>
                  <a class="dropdown-item" href="app.message.html">
                  <span>Message</span>
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="docs.html">
                  Need help?
                  </a>
                  <a class="dropdown-item" href="/logout" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Sign out</a>
                  <form id="logout-form" action="/logout" method="POST" style="display: none;">
                      {{ csrf_field() }}
                  </form>
              

            
                </div>
              </li>
                </ul>
                <!-- / navbar right -->
          </div>
    </div>
    <div class="app-body" id="my-vue">

<!-- ############ PAGE START-->
    @yield('content')

<!-- ############ PAGE END-->

    </div>
  </div>
  <!-- / -->

  

<!-- ############ LAYOUT END-->
  </div>

<!-- build:js scripts/app.min.js -->
<!-- jQuery -->

    <script>

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      
      var modalSize = '';
   
      var Modal_id = ''; 

      var imageCover = '';

      var title = '';

      var calltoaction = '';

      var ModalbackgroundColor = '';
      
    </script>
    @yield('script')
    <script src="{{elixir('js/app.js')}}"></script>

    <script>

      
   var clipboard = new Clipboard('.btn');

    clipboard.on('success', function(e) {
        toastr.success('Link telah dicopy');
    });

    clipboard.on('error', function(e) {
        console.log(e);
    });



$(document).on('click', '.submit', function() { 

  var btn = $(this);
        $(btn).buttonLoader('start');
        setTimeout(function () {
            $(btn).buttonLoader('stop');
        }, 3000);

});



$(document).ready(function() {
  $('#summernote').summernote();
});

function refreshCache(alias){

  $.post("/contentlink/campaign/refresh", {data : alias} ,function(){
    toastr.success('Cache telah dibersihkan!');
   });

}

function markAsRead(){

console.log('keren');
 $.post("/notifications/read");
        }



    </script>

<!-- endbuild -->
</body>
</html>

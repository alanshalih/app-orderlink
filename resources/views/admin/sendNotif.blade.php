@extends('layouts.app')


@section('content')
@if (session('status'))
<script>
  var sites = {!! json_encode(session('status')) !!};
  toastr.success(sites);

</script>
@endif

<div class="padding">

  <div class="box">
    <div class="box-header">
    </div>

    <div class="p-l p-r">
    <form action="/admin/send/notif" class="p-b" method="POST">
    	{{csrf_field()}}
    	<div class="form-group">
    		<label for="">isi text</label>
    		<textarea  class="form-control" name="text"></textarea>
    		
    	</div>
    	<div class="form-group">
    	<label for="">link (go to after klik)</label>
    		<input type="text" class="form-control" name="link">
    		
    	</div>
    	<button class="btn btn-info">Kirim</button>
    </form>
    </div>
  </div>
</div>



<!-- ############ PAGE END-->

@endsection

@section('script')


@endsection
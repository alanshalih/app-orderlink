@extends('layouts.app')
@section('content')

<div class="padding">

  <div class="box container">
    <div class="box-header">
      <h2>Users Edit</h2>
    </div>


   <form action="/user/giverole" method="POST" class="form-horizontal">
    {{ csrf_field() }}

                <input type="hidden" name="user" value="{{$user->id}}">


                <div class="md-form-group">
                  <select class="md-input" name="role" id="">
                    @foreach ($roles as $role)
                    <option value="{{$role->id}}">{{$role->display_name}}</option>
                    @endforeach
                  </select>
                  <label>Role</label>
                </div>

                <div class="md-form-group">
                  <select class="md-input" name="bulan" id="">
                    <option value="1">1 Bulan</option>
                    <option value="3">3 Bulan</option>
                    <option value="6">6 Bulan</option>
                    <option value="12">1 Tahun</option>
                  </select>
                  <label>Selama</label>
                </div>




                <div class="md-form-group">
              <button type="submit" class="md-btn md-raised m-b-sm w-xs info text-white">Berikan!</button>
   
            </div>

                </form>
    <div>
    </div>
  </div>
</div>





<!-- ############ PAGE END-->

@endsection
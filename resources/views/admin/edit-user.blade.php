@extends('layouts.app')
@section('pixelink', 'active')
@section('content')
<div class="app-body-inner">
  <div class="row-col">
    <div class="white bg b-b">
      <div class="navbar no-radius box-shadow-z1">
        <a data-toggle="modal" data-target="#subnav" data-ui-modal class="navbar-item pull-left hidden-lg-up">
        <span class="btn btn-sm btn-icon info">
        <i class="fa fa-th"></i>
        </span>
        </a>
        <a data-toggle="modal" data-target="#list" data-ui-modal class="navbar-item pull-left hidden-md-up">
        <span class="btn btn-sm btn-icon white">
        <i class="fa fa-list"></i>
        </span>
        </a>
        <!-- nabar right -->
        <ul class="nav navbar-nav pull-right m-l">
          <li class="nav-item dropdown">
            <a class="nav-link text-muted" href="#" data-toggle="dropdown">
            <i class="fa fa-ellipsis-h"></i>
            </a>
            <div class="dropdown-menu pull-right text-color" role="menu">
              <a class="dropdown-item">
              <i class="fa fa-tag"></i>
              Tag item
              </a>
              <a class="dropdown-item">
              <i class="fa fa-pencil"></i>
              Edit item
              </a>
              <a class="dropdown-item">
              <i class="fa fa-trash"></i>
              Delete item
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item">
              <i class="fa fa-ellipsis-h"></i>
              More action
              </a>
            </div>
          </li>
        </ul>
        <!-- / navbar right -->
        <!-- link and dropdown -->
        <ul class="nav navbar-nav">
          <li class="nav-item">
            <a class="nav-link text-muted" data-toggle="modal" data-target="#modal-new" title="Reply">
            <span class="">
            <i class="fa fa-fw fa-plus"></i>
            <span class="hidden-sm-down">New contact</span>
            </span>
            </a>
          </li>
        </ul>
        <!-- / link and dropdown -->
      </div>
    </div>
    <div class="row-row">
      <div class="row-col">
        <div class="col-xs w modal fade aside aside-md b-r" id="subnav">
          <div class="row-col light bg">
            <!-- flex content -->
            <div class="row-row">
              <div class="row-body scrollable hover">
                <div class="row-inner">
                  <!-- content -->
                  <div class="navside m-t">
                    <nav class="nav-border b-primary" data-ui-nav>
                      <ul class="nav">
                        <li class="active">
                          <a href="#">
                          <span class="nav-label">
                          <b class="label warn rounded">32</b>
                          </span>
                          <span class="nav-text">All</span>
                          </a>
                        </li>
                        @foreach ($roles as $role)
                        <li>
                          <a href="/user/{{$role->name}}">
                          <!-- <span class="nav-label">
                          <b class="label primary rounded">4</b>
                          </span> -->
                          <span class="nav-text">{{$role->display_name}}</span>
                          </a>
                        </li>
                        @endforeach
                        
                       
                      </ul>
                    </nav>
                  </div>
                </div>
              </div>
            </div>
            <!-- / -->
            <!-- footer -->
            <div class="p-a b-t">
              <a href="#" class="btn btn-xs rounded primary"><i class="fa fa-plus m-r-xs"></i> New Group</a>
            </div>
            <!-- / -->
          </div>
        </div>
        <div class="col-xs modal fade aside aside-sm  b-r" id="list">
          <div class="row-col">
            <div class="row-row">
              <div class="row-col">
                <!-- col -->
                <div class="col-xs w-40 white bg b-r">
                  <div class="row-col">
                    <div class="row-row">
                      <div class="row-body scrollable hover">
                        <div class="row-inner">
                          <div class="text-center text-sm p-y-sm">
                            <a href="#" class="block text-muted">A</a>
                            <a href="#" class="block text-muted active text-primary _600">B</a>
                            <a href="#" class="block text-muted">C</a>
                            <a href="#" class="block text-muted">D</a>
                            <a href="#" class="block text-muted">E</a>
                            <a href="#" class="block text-muted">F</a>
                            <a href="#" class="block text-muted">G</a>
                            <a href="#" class="block text-muted">H</a>
                            <a href="#" class="block text-muted">I</a>
                            <a href="#" class="block text-muted">J</a>
                            <a href="#" class="block text-muted">K</a>
                            <a href="#" class="block text-muted">L</a>
                            <a href="#" class="block text-muted">M</a>
                            <a href="#" class="block text-muted">N</a>
                            <a href="#" class="block text-muted">O</a>
                            <a href="#" class="block text-muted">P</a>
                            <a href="#" class="block text-muted">Q</a>
                            <a href="#" class="block text-muted">R</a>
                            <a href="#" class="block text-muted">S</a>
                            <a href="#" class="block text-muted">T</a>
                            <a href="#" class="block text-muted">U</a>
                            <a href="#" class="block text-muted">V</a>
                            <a href="#" class="block text-muted">W</a>
                            <a href="#" class="block text-muted">X</a>
                            <a href="#" class="block text-muted">Y</a>
                            <a href="#" class="block text-muted">Z</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- col -->
                <div class="col-xs">
                  <div class="row-col white bg">
                    <!-- flex content -->
                    <div class="row-row">
                      <div class="row-body scrollable hover">
                        <div class="row-inner">
                          <!-- left content -->
                          <div class="list" >
                          	@foreach ($users as $usr)
                            <div class="list-item ">
                              <div class="list-left">
                                <span class="w-40 avatar circle blue-grey">
                                {{strtoupper($usr->name[0])}}
                                </span>
                              </div>
                              <div class="list-body">
                                <div class="pull-right dropdown">
                                  <a href="#" data-toggle="dropdown" class="text-muted"><i class="fa fa-fw fa-ellipsis-v"></i></a>
                                  <div class="dropdown-menu pull-right text-color" role="menu">
                                    <a class="dropdown-item">
                                    <i class="fa fa-tag"></i>
                                    Tag item
                                    </a>
                                    <a class="dropdown-item">
                                    <i class="fa fa-pencil"></i>
                                    Edit item
                                    </a>
                                    <a class="dropdown-item">
                                    <i class="fa fa-trash"></i>
                                    Delete item
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item">
                                    <i class="fa fa-ellipsis-h"></i>
                                    More action
                                    </a>
                                  </div>
                                </div>
                                <div class="item-title">
                                  <a href="/user/{{$usr->id}}" class="_500">{{$usr->name}}</a>
                                </div>
                                <small class="block text-muted text-ellipsis">
                                {{$usr->email}}
                                </small>
                              </div>
                            </div>
                           @endforeach
                          </div>
                          <!-- / -->
                        </div>
                      </div>
                    </div>
                    <!-- / -->
                  </div>
                </div>
              </div>
            </div>
            <!-- footer -->
            <div class="white bg p-a b-t clearfix">
              <div class="btn-group pull-right">
                <a href="#" class="btn btn-xs white circle"><i class="fa fa-fw fa-angle-left"></i></a>
                <a href="#" class="btn btn-xs white circle"><i class="fa fa-fw fa-angle-right"></i></a>
              </div>
              <span class="text-sm text-muted">Total: <strong>10</strong></span>
            </div>
            <!-- / -->
          </div>
        </div>
        <div class="col-xs" id="detail">
          <div class="row-col white bg">
            <!-- flex content -->
            <div class="row-row">
              <div class="row-body scrollable hover">
                <div class="row-inner">
                  <!-- content -->
                  <div class="padding">

                  	<h5>Berikan Role pada {{$user->name}}</h5>
                   
                   <form action="/user/giverole" method="POST" class="form-horizontal">
    {{ csrf_field() }}

                <input type="hidden" name="user" value="{{$user->id}}">


		            <div class="md-form-group">
		              <select class="md-input" name="role" id="">
		              	@foreach ($roles as $role)
		              	<option value="{{$role->id}}">{{$role->display_name}}</option>
		              	@endforeach
		              </select>
		              <label>Role</label>
		            </div>

                <div class="md-form-group">
                  <select class="md-input" name="bulan" id="">
                    <option value="1">1 Bulan</option>
                    <option value="3">3 Bulan</option>
                    <option value="6">6 Bulan</option>
                    <option value="12">1 Tahun</option>
                  </select>
                  <label>Selama</label>
                </div>




		            <div class="md-form-group">
              <button type="submit" class="md-btn md-raised m-b-sm w-xs info text-white">Berikan!</button>
   
            </div>

		            </form>

		            </div>

                  <!-- / -->
                </div>
              </div>
            </div>
            <!-- / -->
            <!-- footer -->

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-new">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header _600">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
        New Contact
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="form-group row">
            <label class="col-lg-2 form-control-label">Name:</label>
            <div class="col-lg-8">
              <input type="text" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-2 form-control-label">Title:</label>
            <div class="col-lg-8">
              <input type="text" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-2 form-control-label">Description</label>
            <div class="col-lg-10">
              <div class="b-a">
                <div data-ui-jp="summernote" data-ui-options="{height: 150,
                  toolbar: [
                  ['style', ['bold', 'italic', 'underline', 'clear']],
                  ['color', ['color']],
                  ['para', ['ul', 'ol', 'paragraph']],
                  ['height', ['height']]
                  ]}">
                </div>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-lg-8 offset-lg-2">
              <button class="btn primary btn-sm p-x-md">Save</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
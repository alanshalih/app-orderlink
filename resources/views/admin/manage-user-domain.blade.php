@extends('layouts.app')


@section('content')


<div class="padding">

  <div class="box">
    <div class="box-header">
    </div>

    <div class="p-l p-r">
   <div class="list-group m-b col-md-6">
              @foreach($domains as $domain)
              <span class="list-group-item">
                <span class="pull-right">  
                  <a class="nav-link" data-toggle="dropdown">
                    <i class="fa fa-fw fa-ellipsis-v"></i>
                  </a>
                  <div class="dropdown-menu pull-right">
                    <a  onclick="event.preventDefault();
                document.getElementById('delete-domain{{$domain->id}}').submit();"  class="dropdown-item" href="#">Terima</a>
                  </div>
                </span>

                <span class="pull-right label {{$domain->status == 'submitted' ? 'info' : 'success'}}">{{$domain->status}}</span>
      
                {{$domain->domain}}
              </span>

              <form id="delete-domain{{$domain->id}}" action="/admin/userdomain/{{$domain->id}}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>
              @endforeach
            </div>
    </div>
  </div>
</div>



<!-- ############ PAGE END-->

@endsection

@section('script')


@endsection
@extends('layouts.app')


@section('content')


<div class="padding">

  <div class="box">
    <div class="box-header">
    </div>

    <div class="p-l p-r">
<user-table :roles="{{$roles}}"></user-table>
    </div>
  </div>
</div>



<!-- ############ PAGE END-->

@endsection

@section('script')


@endsection
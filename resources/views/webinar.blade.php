@extends('layouts.app')
@section('head')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.0/css/rowReorder.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">

  <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/rowreorder/1.2.0/js/dataTables.rowReorder.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
@endsection
@section('content')


{{Carbon\Carbon::setLocale('id')}}
<!-- ############ PAGE START-->
<div class="padding">
  
    <div class="m-b-lg row">
      <div class="col-sm-12">
 

    <table id="webinar" >
        <thead>
            <tr>
                <th>id.</th>
                <th>Nama</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody>
             @foreach($webinarlists as $user)
              <tr data-expanded="false">
              <td>{{$user->id}}</td>
              <td>{{$user->firstname}}</td>
              <td>{{$user->email}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    
    </div>
  </div>
</div>

<!-- ############ PAGE END-->

@endsection


@section('script')

<script>
  $(document).ready(function() {

    var table2 = $('#webinar').DataTable( {
      dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel'
        ],
        responsive: true,
    } );
} );
</script>
@endsection
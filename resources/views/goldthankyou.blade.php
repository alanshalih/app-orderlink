@extends('layouts.app')


@section('head')
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '157095107992587'); // Insert your pixel ID here.
fbq('track', 'Purchase', {value: 447000, currency: 'IDR'});

fbq('init', '926629954099014'); // Insert your pixel ID here.
fbq('track', 'Purchase', {value: 447000, currency: 'IDR'});

fbq('init', '200022016998409'); // Insert your pixel ID here.
fbq('track', 'Purchase', {value: 447000, currency: 'IDR'});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

  $.post( "/activate/user?user={{$user->id}}&role=1&bulan=12");


</script>



<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

@endsection

@section('content')



<div class="padding">

  <div class="row m-b">

    <div class="col-sm-8">
      <div class="box">
        <div class="box-header info">
             <h2>Hallo, {{Auth::user()->name}}</h2>  
        </div>
        <div class="box-body">
          <p >kini akun Anda sudah aktif dengan paket <strong>Gold</strong> Member selama 1 Tahun. Kini Anda bisa lebih leluasa dalam memaksimalkan semua fitur hebat yang ada di depan Anda. </p>

<p>Silakan buka menu tutorial untuk mendapatkan panduan penggunaan OrderLink. Pahami cara kerjanya pelan-pelan, dan jika ada yang ingin ditanyakan setelah melihat tutorial dan mencoba praktek silakan hubungi kami melalui menu support</p> 

<p>Terimakasih, enjoy!</p>

<p>Sandi & Maulana
~ OrderLink Team</p>
        </div>
      </div>
    </div>

  </div>
  

</div>

<!-- Google Code for OrderLink Gold Tahunan Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 928823537;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "H2MaCI69mHAQ8fHyugM";
var google_conversion_value = 447000.00;
var google_conversion_currency = "IDR";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/928823537/?value=447000.00&amp;currency_code=IDR&amp;label=H2MaCI69mHAQ8fHyugM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

@endsection

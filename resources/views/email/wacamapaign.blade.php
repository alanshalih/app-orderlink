<!DOCTYPE html PUBLIC " -//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>                                                            
    <title>
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <style type="text/css">    body, html {
      width: 100% !important;
      margin: 0;
      padding: 0;
      -webkit-font-smoothing: antialiased;
      -webkit-text-size-adjust: none;
      -ms-text-size-adjust: 100%;
    }
      table td, table {
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
      }
      #outlook a {
        padding: 0;
      }
      .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
        line-height: 100%;
      }
      .ExternalClass {
        width: 100%;
      }
      @media only screen and (max-width: 480px) {
        table, table tr td, table td {
          width: 100% !important;
        }
        img {
          width: inherit;
        }
        .layer_2 {
          max-width: 100% !important;
        }
      }
    </style>
  </head>
  <body style="padding: 0px; margin: 0px;">                                                                                    
    <table style="height: 100%; width: 100%; background-color: #efefef;" align="center">        
      <tbody>            
        <tr>                
          <td valign="top" id="dbody" data-version="2.22" style="padding-top:30px;padding-bottom:30px;background-color:#efefef;width:100%;height:100%;">
            <!--[if (gte mso 9)|(IE)]><table style="width:600px" width="600" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td><![endif]-->                    
            <table class="layer_1" align="center" border="0" cellpadding="0" cellspacing="0" style="max-width:600px;box-sizing:border-box;width:100%;">                        
              <tbody>                                                                                                                
                <tr>                                
                  <td class="drow" valign="top" align="center" style="background-color:#ffffff;box-sizing:border-box;font-size:0px;text-align:center;">                                                                        
                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td><![endif]-->
                    <div class="layer_2" style="display:inline-block;vertical-align:top;width:100%;max-width:600px;">                                        
                      <table class="edcontent" style="border-collapse: collapse; width: 100%;" border="0" cellpadding="20" cellspacing="0">                                            
                        <tbody>                                                
                          <tr>                                                    
                            <td class="edtext" valign="top" style="text-align:left;color:#5f5f5f;font-size:16px;font-family:Helvetica,Arial,sans-serif;word-break:break-word;direction:ltr;box-sizing:border-box;">
                              <p style="margin:0px;padding:0px;">Simpan email ini dengan baik, jika perlu anda boleh menandai email ini sebagai email penting. sehingga nantinya jika butuh email ini, &nbsp;anda dengan mudah mencarinya.
                              </p>
                              <p style="margin:0px;padding:0px;">
                                <br>
                              </p>
                              <p style="margin:0px;padding:0px;">Btw, tanpa basa-basi lagi. ini detil Link Auto-Order anda
                              </p>
                              
                           
                              <p style="margin:0px;padding:0px;">
                                <br>
                                <a href="http://wa.orderlink.in/no/{{$data->phone}}">http://wa.orderlink.in/no/{{$data->phone}}</a>
                              </p>
                                <br>
                                 <p style="margin:0px;padding:0px;">sebelum dicopy, tes dulu udah work atau belum linknya.. jika udah work, anda bisa mengcopy link di atas ke setiap status promo anda&nbsp;
                              </p>
                              <p style="margin:0px;padding:0px;">
                                <br>
                              </p>
                              <p style="margin:0px;padding:0px;">o iya, anda juga bisa melihat statistik link di atas, seperti udah berapa kali link itu di klik. lihat statistik disini
                              </p>
                     
                                <p style="margin:0px;padding:0px;">
                                <br>
                                <a href="http://wa.orderlink.in/stat/{{$data->phone}}?token={{$data->token}}">http://wa.orderlink.in/stat/{{$data->phone}}?token={{$data->token}}</a>
                              </p>
                              <p style="margin:0px;padding:0px;">
                                <br>
                              </p>
              
                              <p style="margin:0px;padding:0px;">
                                mau ganti isi pesan? bisa kok. isi lagi aja form disini http://wa.orderlink.in
                              </p>
                              <p style="margin:0px;padding:0px;"><br>Terima Kasih Udah Mencoba WhatsApp Auto-Order. salah satu fitur terkeren dari http://orderlink.in
                              </p>
                              <p style="margin:0px;padding:0px;">
                                <br>
                              </p>
                              <p style="margin:0px;padding:0px;">
                                <br>
                                salam,
                                <br>
                                <strong>Sandi & Maulana</strong>
                              </p>
                            </td>                                              
                          </tr>                                          
                        </tbody>                                      
                      </table>                                  
                    </div>
                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->                              
                  </td>                          
                </tr>
                <tr>
                  <td class="drow" valign="top" align="center" style="background-color:#ffffff;box-sizing:border-box;font-size:0px;text-align:center;">
                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td><![endif]-->
                    <div class="layer_2" style="display:inline-block;vertical-align:top;width:100%;max-width:600px;">    
                      <table border="0" cellspacing="0" cellpadding="0" class="edcontent" style="border-collapse: collapse;width:100%">        
                        <tbody>
                          <tr>            
                            <td valign="top" class="breakline">                
                              <div style="border-style: solid none none; border-width: 1px 0px 0px; margin: 8px 32px; border-top-color: #a6a6a6;">
                              </div>          
                            </td>      
                          </tr>  
                        </tbody>
                      </table>
                    </div>
                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                  </td>
                </tr>
                <tr>
                  <td class="drow" valign="top" align="center" style="background-color:#ffffff;box-sizing:border-box;font-size:0px;text-align:center;">
                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td><![endif]-->
                    <div class="layer_2" style="display:inline-block;vertical-align:top;width:100%;max-width:600px;">                                        
                      <table class="edcontent" style="border-collapse: collapse;width:100%" border="0" cellpadding="10" cellspacing="0">                                            
                        <tbody>                                                
                          <tr>                                                    
                            <td class="edtext" valign="top" style="text-align:left;color:#5f5f5f;font-size:16px;font-family:Helvetica,Arial,sans-serif;word-break:break-word;direction:ltr;box-sizing:border-box;">
                              <p style="margin:0px;padding:0px;text-align:center;font-size:11px;">
                                <span style="color: #000000;">
                                  <span style="font-size: 16px;">
                                    <strong>
                                      <span style="color: #ff0000;">Perhatian!
                                      </span>
                                    </strong>
                                  </span>
                                </span>
                              </p>
                              <p style="margin:0px;padding:0px;text-align:center;font-size:11px;">
                                <span style="color: #000000;">
                                  <span style="font-size: 16px;">Update Akan Selalu Dikirim Via Email Ini, 
                                    <br>Pastikan Email Ini Ada di dalam Inbox Primary Anda.
                                  </span>
                                </span>
                              </p>
                              <p style="margin:0px;padding:0px;text-align:center;font-size:11px;">
                                <span style="color: #000000;">
                                  <span style="font-size: 16px;">
                                    <br>
                                  </span>
                                </span>
                              </p>
                              <p style="margin:0px;padding:0px;text-align:center;font-size:11px;">
                                <span style="color: #000000;">
                                  <span style="font-size: 16px;">
                                    <strong>Masuk folder SPAM/Promotion? 
                                      <br>
                                    </strong>Pindahkan ke primary/utama dan whitelisting email ini ya :)
                                  </span>
                                </span>
                              </p>
                              <p style="margin:0px;padding:0px;text-align:center;font-size:11px;">
                                <br>
                              </p>
                              <p style="margin:0px;padding:0px;text-align:center;font-size:11px;">
                                <br>
                              </p>
                              <p style="margin:0px;padding:0px;text-align:center;font-size:11px;">
                                <span style="font-size: 12px;">Jika merasa email ini kurang informatif, Anda dapat&nbsp;
                                </span>
                                <a href="{unsubscribe}" style="color:#14288e;font-size:16px;font-family:Helvetica,Arial,sans-serif;text-decoration:none;"><span style="font-size: 12px;">unsubscribe</span></a> <span style="font-size: 12px;">&nbsp;tapi ga akan dapat update lagi
                                </span>
                                <br>
                                <span style="font-size: 12px;">                                                                            {accountcontactinfo}
                                </span>                                                      
                              </p>
                            </td>                                              
                          </tr>                                          
                        </tbody>                                      
                      </table>                                  
                    </div>
                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                  </td>
                </tr>                            
                <tr>
                  <td class="drow" valign="top" align="center" style="background-color:#000000;box-sizing:border-box;font-size:0px;text-align:center;">
                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td><![endif]-->
                    <div class="layer_2" style="display:inline-block;vertical-align:top;width:100%;max-width:600px;">    
                      <table border="0" cellspacing="0" cellpadding="28" class="edcontent" style="border-collapse: collapse;width:100%">        
                        <tbody>
                          <tr>            
                            <td valign="top" class="edimg" style="box-sizing:border-box;text-align:center;">
                              <img src="https://api.elasticemail.com/userfile/226b9ade-85a6-4882-8996-6b027adb4272/nama_program_di_menu.png" alt="Image" width="206" style="border-width: 0px; border-style: none; max-width: 206px; width: 100%;">
                            </td>      
                          </tr>  
                        </tbody>
                      </table>
                    </div>
                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                  </td>
                </tr>
                <tr>
                  <td class="drow" valign="top" align="center" style="background-color:#02225f;box-sizing:border-box;font-size:0px;text-align:center;">
                    <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td><![endif]-->
                    <div class="layer_2" style="display:inline-block;vertical-align:top;width:100%;max-width:600px;">                                        
                      <table class="edcontent" style="border-collapse: collapse;width:100%;" border="0" cellpadding="20" cellspacing="0">                                            
                        <tbody>                                                
                          <tr>                                                    
                            <td class="edtext" valign="top" style="text-align:left;color:#5f5f5f;font-size:16px;font-family:Helvetica,Arial,sans-serif;word-break:break-word;direction:ltr;box-sizing:border-box;">
                              <p style="margin:0px;padding:0px;text-align:center;">
                                <strong>
                                  <em data-elastictext-tag="em" data-verified="redactor">
                                    <span style="color: #ffffff;">Powerful List Building - Link Tracking - Retargeting Platform.
                                    </span>
                                  </em>
                                </strong>
                              </p>
                            </td>                                              
                          </tr>                                          
                        </tbody>                                      
                      </table>                                  
                    </div>
                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                  </td>
                </tr>                                                                                                          
              </tbody>                  
            </table>                
            <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
          </td>          
        </tr>      
      </tbody>  
    </table>
  </body>
</html>

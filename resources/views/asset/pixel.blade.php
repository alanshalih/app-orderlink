@extends('layouts.app')
@section('content')


<!-- ############ PAGE START-->
<div class="padding">
	
<div class="row">
	
	<div class="col-md-6">
		<div class="box">
				<div class="box-header">
				<h2>Daftar Pixel Facebook Anda</h2>
				</div>
		<div class="box-body">
			<list-pixel :pixels_="{{$pixel}}" type="facebook"></list-pixel>	
		</div>

		
		</div>
	</div>

	
	<div class="col-md-6">
		<div class="box">
				<div class="box-header">
				<h2>Daftar Pixel Google Anda</h2>
				</div>
		<div class="box-body">
			<list-pixel :pixels_="{{$google}}" type="google"></list-pixel>	
		</div>


	</div>
</div>

</div>
<!-- ############ PAGE END-->

@endsection
@extends('layouts.app')
@section('head')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.0/css/rowReorder.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">

  <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/rowreorder/1.2.0/js/dataTables.rowReorder.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
@endsection
@section('content')


<!-- ############ PAGE START-->
<div class="padding">
  <div class="box">
    <div class="box-header">
      <h2>Daftar List Anda</h2>
      <small>berikut ini adalah data hasil list building anda</small>
    </div>

    <div class="padding">
       <table id="example" >
  <thead>
    <tr>
      <th>Nama</th>
      <th>Email</th>
      <th >nohp</th>
      <th>template_id</th>
      <th >website</th>
    </tr>
  </thead>
  <tbody>
 @foreach($lists as $list)
    <tr>
      <td>{{$list->nama}}</td>
      <td>{{$list->email}}</td>
      <td>{{$list->nohp}}</td>
      <td>{{$list->template_id}}</td>
      <td>{{$list->website}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
    </div>
  </div>
</div>

<!-- ############ PAGE END-->

@endsection


@section('script')

<script>
  $(document).ready(function() {
    var table = $('#example').DataTable( {
      dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel'
        ],
        responsive: true,
    } );
} );
</script>
@endsection
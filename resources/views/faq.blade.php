@extends('layouts.app')
@section('content')

<div class="padding">
  <div class="row">
    <div class="col-sm-8 col-md-9">
      <h4 class="m-b-sm text-md">Quick support</h4>
      <div class="m-b" id="accordion">
        <div class="panel box no-border m-b-xs">
          <div class="box-header p-y-sm">
            <span class="pull-right label text-sm">3214</span>
            <a data-toggle="collapse" data-parent="#accordion" data-target="#c_1">
              Q: How to create an icon like the demo app
            </a>
          </div>
          <div id="c_1" class="collapse in">
            <div class="box-body">
              <div class="text-sm text-muted">
                <div class="pull-left m-r"><span class="text-md w-32 avatar rounded success">A</span></div> 
                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, <br><br>raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="panel box no-border m-b-xs">
          <div class="box-header p-y-sm">
            <span class="pull-right label text-sm">430</span>
            <a data-toggle="collapse" data-parent="#accordion" data-target="#c_2">
              Q: How to build my custom color
            </a>
          </div>
          <div id="c_2" class="collapse">
            <div class="box-body">
                <div class="pull-left m-r"><span class="text-md w-32 avatar rounded info">A</span></div> 
                <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
              </p>
            </div>
          </div>
        </div>
        <div class="panel box no-border m-b-xs">
          <div class="box-header p-y-sm">
            <span class="pull-right label text-sm">230</span>
            <a data-toggle="collapse" data-parent="#accordion" data-target="#c_3">
              Q: What is the app requriements
            </a>
          </div>
          <div id="c_3" class="collapse">
            <div class="box-body">
              <div class="pull-left m-r"><span class="text-md w-32 avatar rounded warning">A</span></div> 
              <p class="text-sm text-muted clear">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
              </p>
            </div>
          </div>
        </div>
        <div class="panel box no-border m-b-xs">
          <div class="box-header p-y-sm">
            <span class="pull-right label text-sm">49</span>
            <a data-toggle="collapse" data-parent="#accordion" data-target="#c_4">
              Q: Where to find the API
            </a>
          </div>
          <div id="c_4" class="collapse">
            <div class="box-body">
              <div class="pull-left m-r"><span class="text-md w-32 avatar rounded success">A</span></div> 
              <p class="text-sm text-muted clear">Vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. nesciunt you probably haven't heard of them accusamus labore sustainable
              </p>
            </div>
          </div>
        </div>
        <div class="panel box no-border m-b-xs">
          <div class="box-header p-y-sm">
            <span class="pull-right label text-sm">6</span>
            <a data-toggle="collapse" data-parent="#accordion" data-target="#c_5">
              Q: How to add my router
            </a>
          </div>
          <div id="c_5" class="collapse">
            <div class="box-body">
              <div class="pull-left m-r"><span class="text-md w-32 avatar rounded success">A</span></div> 
              <p class="text-sm text-muted clear">Vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. nesciunt you probably haven't heard of them accusamus labore sustainable
              </p>
            </div>
          </div>
        </div>
      </div>
      <h4 class="m-t-md text-md">Ga nemu jawaban di atas? kirim ticket</h4>
      <form name="form-contact" class="form-validation m-b-lg">
        <div class="form-group">
          <label>Pertanyaan Seputar?</label>
          <select class="form-control c-select">
            <option>Website Errors</option>
            <option>Cara Pemakaian</option>
            <option>Saran Pengembanngan</option>
          </select>
        </div>
        <div class="form-group">
          <label>Pertanyaan Anda</label>
          <textarea class="form-control" rows="6" placeholder="hmm.."></textarea>
        </div>
        <button type="submit" class="btn btn-info">Submit</button>
      </form>
    </div>
    <div class="col-sm-4 col-md-3">
      <h4 class="m-b-sm text-md">Ask experts</h4>
      <div class="box">
            <div class="item">
              <div class="item-bg info h6">
                <p class="p-a">"Layaknya karya seni, Orderlink berikan yang terbaik!"</p>
              </div>
              <div class="p-a p-y-lg pos-rlt">
                <img src="images/a0.jpg" alt="." class="img-circle w-56" style="margin-bottom: -7rem">
              </div>
          </div>
            <div class="p-a">
              <a href="#" class="text-md m-t block">Maulana Shalihin</a>
              <p><small>Developer</small></p>
                <div>
                <a href="" class="btn btn-icon btn-social rounded white btn-sm">
                <i class="fa fa-facebook"></i>
                <i class="fa fa-facebook indigo"></i>
              </a>
              <a href="" class="btn btn-icon btn-social rounded white btn-sm">
                <i class="fa fa-twitter"></i>
                <i class="fa fa-twitter light-blue"></i>
              </a>
              <a href="" class="btn btn-icon btn-social rounded white btn-sm">
                <i class="fa fa-google-plus"></i>
                <i class="fa fa-google-plus red"></i>
              </a>
              </div>
            </div>
          </div>
          <div class="box">
            <div class="item">
              <div class="item-bg primary h6">
                <p class="p-a">"Siap memberikan yang terbaik untuk anda!"</p>
              </div>
              <div class="p-a p-y-lg pos-rlt">
                <img src="images/a0.jpg" alt="." class="img-circle w-56" style="margin-bottom: -7rem">
              </div>
          </div>
            <div class="p-a">
              <a href="#" class="text-md m-t block">Farhadz Fadhillah Sandi</a>
              <p><small>Marketing Strategist</small></p>
                <div>
                <a href="" class="btn btn-icon btn-social rounded white btn-sm">
                <i class="fa fa-facebook"></i>
                <i class="fa fa-facebook indigo"></i>
              </a>
              <a href="" class="btn btn-icon btn-social rounded white btn-sm">
                <i class="fa fa-twitter"></i>
                <i class="fa fa-twitter light-blue"></i>
              </a>
              <a href="" class="btn btn-icon btn-social rounded white btn-sm">
                <i class="fa fa-google-plus"></i>
                <i class="fa fa-google-plus red"></i>
              </a>
              </div>
            </div>
          </div>
      <div class="m-b">
        <p><i class="fa fa-fw text-muted m-r-xs fa-envelope"></i> cs@orderlink.in</p>
        <p><i class="fa fa-fw text-muted m-r-xs fa-phone"></i> (62)878 155 79530</p>
        <p><i class="fa fa-fw text-muted m-r-xs fa-clock-o"></i> Mon-Fri 9:00 - 16:00</p>
      </div>
    </div>
  </div>
</div>


@endsection
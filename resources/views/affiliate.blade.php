@extends('layouts.app')
@section('head')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.0/css/rowReorder.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">

  <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/rowreorder/1.2.0/js/dataTables.rowReorder.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
  <script src="//cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
@endsection
@section('content')


{{Carbon\Carbon::setLocale('id')}}
<!-- ############ PAGE START-->
<div class="padding">
  
    <div class="m-b-lg row">
      <div class="col-sm-12">
        <div class="row row-sm">
                            <div class="alert alert-info col-md-8">
            <h3>PANDUAN AFFILIATE</h3>
            <p>1. Dapatkan Info Affiliate, Group Sharing, Senjata Promosi dengan <strong><a target="_blank" href="http://orderlink.in/jv-indonesia-istimewa/">klik disini</a></strong></p>
            <p>2. Form yang dibawah adalah form untuk menancapkan cookie dari website ini, bukan dari idaff. Kami menyarankan anda mengambil link affiliasi anda di Idaff, kemudian paste pada form di bawah ini.</p>
            <p>3. kegunaan dari cookies website ini adalah anda bisa mengetahui siapa lead yang sudah daftar trial/beli</p>
            <p>4. Lead yang anda kumpulkan di tabel bawah, <strong>belum tentu</strong> membeli via link affiliasi anda di idaff.</p>
            <p>5. Launching Orderlink.in dan Kontes Affiliasi dimulai dari tanggal 13 Februari s.d 20 Februari 2017.</p>

        </div>
            <div class="col-sm-6 ">
            <div class="text-center">
            </div>
              <form action="/affiliate/link/create" method="POST">
              {{csrf_field()}}

                <div class="input-group input-group-lg">
                <input type="text" name="link" class="form-control" placeholder="Paste Link Anda disini...">
         
                 </div>

              <button class="btn btn-info " style="width: 100%" type="submit">Tancapkan Cookies</button>
              </form>
          <br>
          @if(count($links)>0)
          <h6>Link Affiliate anda</h6>
          @endif
          <div class="list-group info m-b">
          @foreach($links as $link)
          <li  class="list-group-item">
            <span class="pull-right"> <span class="label rounded dark pos-rlt text-sm m-r-xs"><b class="arrow bottom b-dark pull-in"></b>{{Redis::ZSCORE("affiliate_link", $link->id) ?? 0}}</span>
            <button class="btn btn-outline btn-xs b-white" data-clipboard-action="copy" data-clipboard-target="a#link{{$loop->iteration}}"><i class="fa fa-copy"></i> 
              
            </button> 
                     <button onclick="event.preventDefault();
                                                     document.getElementById('delete-link').submit();"  class="btn btn-xs danger"><i class="fa fa-close text-white"></i></button>

                      <form id="delete-link" action="/affiliate/link/delete/{{$link->id}}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                        </form>
                    </span>
            <a id="link{{$loop->iteration}}" href="{{$link->alias}}" target="_blank">{{$link->alias}}</a><br> >>>
            {{$link->url}} 
          </li>
          @endforeach
        </div>


            </div>

        
        </div>
      </div>
    </div>

  <div class="box">
    <div class="box-header">
      <h2>Daftar Referral Anda</h2>
      <small>berikut ini adalah orang yang gabung Orderlink.in via link affiliate anda</small>
    </div>

    <div class="padding">
 
<table id="example" >
        <thead>
            <tr>
                <th>id.</th>
                <th>Nama</th>
                <th>Email</th>
                <th>Waktu Gabung</th>
            </tr>
        </thead>
        <tbody>
             @foreach($downline as $user)
              <tr data-expanded="false">
              <td>{{$user->id}}</td>
              <td>{{$user->name}}</td>
              <td>{{$user->email}}</td>
              <td>{{\Carbon\Carbon::createFromTimeStamp(strtotime($user->created_at))->diffForHumans()}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

    </div>
  </div>

  <div class="box">
    <div class="box-header">
      <h2>Daftar Referral Webinar Orderlink</h2>
      <small>berikut ini adalah orang yang gabung Webinar via link affiliate anda</small>
    </div>

    <div class="padding">
 


    <table id="webinar" >
        <thead>
            <tr>
                <th>id.</th>
                <th>Nama</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody>
             @foreach($webinarlists as $user)
              <tr data-expanded="false">
              <td>{{$user->id}}</td>
              <td>{{$user->firstname}}</td>
              <td>{{$user->email}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    </div>
  </div>
</div>

<!-- ############ PAGE END-->

@endsection


@section('script')

<script>
  $(document).ready(function() {
    var table = $('#example').DataTable( {
      dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel'
        ],
        responsive: true,
    } );
    var table2 = $('#webinar').DataTable( {
      dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel'
        ],
        responsive: true,
    } );
} );
</script>
@endsection
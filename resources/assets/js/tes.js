import Lodash from 'lodash';

import Vue from 'vue';


window._ = Lodash;

window.Vue = Vue;

import  example from './components/Example.vue';
// Vue.component('example', require('./components/Example.vue'));
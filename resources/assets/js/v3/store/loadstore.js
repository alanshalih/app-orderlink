export default{
	template: '<div></div>',
	mounted(){
		

		if(!this.$store.state.stores){
        // need refactoring 
                
          this.$http.get('/v3/store').then(response => {

            // get body data
            this.stores = (response.body);

            var stores = {
              name : 'stores',
              value : this.stores
            }

            this.$store.commit('init', stores);

             this.$emit('storecall', this.stores);

          }, response => {
            // error callback
          });

      }else{

        this.stores = this.$store.state.stores;

        this.$emit('storecall', this.stores);

      }


	},
	data(){
		return {
			stores : ''
		}
	}
}
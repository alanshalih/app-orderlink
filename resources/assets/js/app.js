
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// import Chartist from 'chartist';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */


Vue.component('example', require('./components/Example.vue'));
Vue.component('create-pixelink-form', require('./components/CreatePixelinkForm.vue'));
Vue.component('edit-pixelink', require('./components/EditPixelink.vue'));
Vue.component('contentlink-campaign', require('./components/CreateContentlinkCampaign.vue'));
Vue.component('edit-contentlink-campaign', require('./components/EditContentlink.vue'));
Vue.component('google-input', require('./components/GoogleRemarketingInput.vue'));
Vue.component('pixel-input', require('./components/PixelInput.vue'));
Vue.component('create-orderlink-form', require('./components/CreateOrderlinkForm.vue'));
Vue.component('edit-orderlink', require('./components/EditOrderlink.vue'));
Vue.component('google-input-default', require('./components/GoogleIPD.vue'));
Vue.component('pixel-input-default', require('./components/PixelIPD.vue'));
// Vue.component('chart', require('./components/Chart.vue'));
// Vue.component('chart-panel', require('./components/PanelStatistik.vue'));
Vue.component('panel-dashboard-chart', require('./components/DashboardPanelStat.vue'));
Vue.component('hit-chart', require('./components/ShowHitStatistik.vue'));
Vue.component('platform-chart', require('./components/PlatformChart.vue'));
Vue.component('list-pixel', require('./components/ListPixel.vue'));
Vue.component('facebook-share', require('./components/FacebookShare.vue'));
Vue.component('social-viral-research', require('./components/SocialViralResearch.vue'));
Vue.component('create-contentlink-from-research', require('./components/CreateContentlinkFromViralResearch.vue'));
Vue.component('change-loader-text', require('./components/ChangeLoaderText.vue'));
Vue.component('advance-tab', require('./components/AdvancePanelTab.vue'));

// Vue.use(VueTables.client, {
//   perPage: 25
// })


// // thirdly, register components to Vue
Vue.component('user-table', require('./components/UserTable.vue'));


// dom to image




const vm = new Vue({
    el: '#my-vue',

    data: {

    	contentlink:{

        id: Modal_id,
        
        title: title,
        deskripsi: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam nibh. Nunc varius facilisis eros. Sed erat. In in velit quis arcu ornare laoreet. Curabitur adipiscing luctus massa.',
        starRating : 3.5,
        harga: 'Rp.10.000,00',
        image1: imageCover,
        image2: '/images/prizes-logo.png',
        kuantitas : true,
        warna: true,
        ukuran: true,
        cta: calltoaction,
        action: 'http://www.google.co.id',
        linkText : 'http://www.google.co.id',
        method: 'POST',
        formHTML: '',
        emailPlaceholder : 'masukan email anda',
        pakaiAutoresponder : 'tidak',
        afterSubmit : 'tetap',
        afterSubsProps : 'Terima Kasih',
 

        // selected properties
        modalSize : modalSize,
        color_scheme : 'rbm_blue',
        background_color : ModalbackgroundColor,
        backdrop_color : 'rbm_bd_black',
        transparency : 'rbm_bd_semi_trnsp',
        border_radius : 'rbm_none_radius',
        position : 'rbm_center',
        animation : 'rbmFadeInUp',
        animation_duration : 'rbm_duration_md',
        animation_time_function : 'rbm_ease',
        hover_effect: 'rbm_btn_x_in_shtr',
        transition_slider : 'rbms_ease',
        shadow : 'rbm_shadow_none',
        data_backdrop: true,
        no_backdrop : '',
      
        },

         properties : {
        method: ['POST','GET'],
        color_scheme : [{class:'rbm_blue',name:'biru'},{class:'rbm_green',name:'hijau'},{class:'rbm_red',name:'merah'},{class:'rbm_gold',name:'emas'},{class:'rbm_brown', name:'coklat'}],
        background_color : [{class:'rbm_bg_white',name: 'putih'},{class:'rbm_bg_light_white', name:'abu-abu'},{class:'rbm_bg_black',name:'hitam'},{class:'rbm_bg_light_black',name:'hitam cerah'}, {class:'', name:'tanpa background'}],
        backdrop_color : [{class : 'rbm_bd_black', name:'hitam'},{class : 'rbm_bd_white', name:'putih'},{class : 'rbm_bd_gray', name:'abu-abu'}],
        transparency : [{class : 'rbm_bd_trnsp', name:'transparan'},{class : 'rbm_bd_less_trnsp', name:'sedikit transparan'},{class : 'rbm_bd_semi_trnsp', name:'semi transparan'},{class : 'rbm_bd_dark_trnsp', name:'gelap'},{class : 'rbm_bd_thick', name:'gelab banget'}],
        border_radius : [{class : 'rbm_none_radius', name:'kotak'},{class : 'rbm_less_radius', name:'sedikit melingkar'},{class : 'rbm_semi_radius', name:'semi melingkar'},{class : 'rbm_full_radius', name:'full melingkar'}],
        position: [{class : 'rbm_top_left', name:'atas kiri'},{class : 'rbm_top_right', name:'atas kanan'},
        {class : 'rbm_center', name:'tengah'},{class : 'rbm_bottom_left', name:'bawah kiri'},
        {class : 'rbm_bottom_right', name:'bawah kanan'},{class : 'rbm_top_center', name:'atas tengah'},
        {class : 'rbm_left', name:'kiri'},{class : 'rbm_right', name:'kanan'},{class : 'rbm_bottom_center', name:'bawah tengah'}],
        animation : [{class : 'rbmBounceIn', name:'BounceIn'},{class : 'rbmBounceInUp', name:'BoundInUp'},
        {class : 'rbmBounceInDown', name:'BounceInDown'},{class : 'rbmBounceInLeft', name:'BoundInLeft'},
        {class : 'rbmBounceInRight', name:'BounceInRight'},{class : 'rbmFadeInDown', name:'FadeInDown'},
        {class : 'rbmFadeInUp', name:'rbmFadeInUp'},{class : 'rbmFadeInLeft', name:'rbmFadeInLeft'},
        {class : 'rbmFadeInRight', name:'rbmFadeInRight'},{class : 'rbmFlipX', name:'rbmFlipX'},{class : 'rbmFlipY', name:'rbmFlipY'},
        {class : 'rbmSpeedIn', name:'rbmSpeedIn'},{class : 'rbmRotateIn', name:'rbmRotateIn'},
        {class : 'rbmRotateInDownLeft', name:'rbmRotateInDownLeft'},{class : 'rbmRotateInDownRight', name:'rbmRotateInDownRight'},
        {class : 'rbmRotateInUpLeft', name:'rbmRotateInUpLeft'},{class : 'rbmRotateInUpRight', name:'rbmRotateInUpRight'},
         {class : 'rbmRollIn', name:'rbmRollIn'},{class : 'rbmZoomIn', name:'rbmZoomIn'},
         {class : 'rbmZoomOut', name:'rbmRollIn'},{class : 'rbmZoomInRotate', name:'rbmZoomInRotate'},{class : 'rbmZoomOutRotate', name:'rbmZoomOutRotate'},
         {class : 'rbmZoomInDown', name:'rbmZoomInDown'},{class : 'rbmZoomInUp', name:'rbmZoomInUp'},
         {class : 'rbmZoomInLeft', name:'rbmZoomInLeft'},
        {class : 'rbmZoomInRight', name:'rbmZoomInRight'}],
        animation_duration : [{class : 'rbm_duration_md', name:'cepat'},{class : 'rbm_duration_lg', name:'sedang'},{class : 'rbm_duration_vl', name:'agak lama'}],
        animation_time_function : [{class : 'rbm_ease', name:'ease'},
        {class : 'rbm_easeOutInCubic', name:'easeOutInCubic'},
        {class : 'rbm_easeOutExpo', name:'rbm_easeOutExpo'},
        {class : 'rbm_easeOutQuint', name:'rbm_easeOutQuint'},
        {class : 'rbm_easeInOut', name:'rbm_easeInOut'},
        {class : 'rbm_swing', name:'rbm_swing'},
        {class : 'rbm_easeOutQuad', name:'rbm_easeOutQuad'},
        {class : 'rbm_easeOutSine', name:'rbm_easeOutSine'},
        {class : 'rbm_speedy', name:'rbm_speedy'},
        {class : 'rbm_easeOutCubic', name:'rbm_easeOutCubic'},
        {class : 'rbm_easeOutQuart', name:'rbm_easeOutQuart'},
        {class : 'rbm_slowSpeedy', name:'rbm_slowSpeedy'},
        {class : 'rbm_easeOutCirc', name:'rbm_easeOutCirc'},
        {class : 'rbm_easeOutQuint', name:'rbm_easeOutQuint'}],
        hover_effect : [{class : 'rbm_btn_x_in_shtr', name:'horizontal'},{class : 'rbm_btn_y_in_shtr', name:'vertical'},{class : 'rbm_btn_x_out_shtr', name:'horizontal out'},{class : 'rbm_btn_y_out_shtr', name:'vertical out'}],
        transition_slider : [{class : 'rbms_ease', name:'ease'},{class : 'rbms_easeOutCubic', name:'easeOutCubic'},{class : 'rbms_easeOutQuart', name:'easeOutQuart'},{class : 'rbms_slowSpeedy', name:'slowSpeed'},{class : 'rbms_easeOutCirc', name:'easeOutCirc'},{class : 'rbms_easeOutQuint', name:'easeOutQuint'},{class : 'rbms_easeOutInCubic', name:'EaseOutInCubic'},{class : 'rbms_easeOutExpo', name:'easeOutExpo'},{class : 'rbms_easeOutSine', name:'easeOutSine'},{class : 'rbms_swing', name:'Swing'},{class : 'rbms_easeOutQuad', name:'easeOutQuad'}],
        shadow : [{class: 'rbm_shadow_none', name: 'tanpa shadow'},
                    {class: 'rbm_shadow_sm_black', name: 'shadow hitam sedikit'},
                    {class: 'rbm_shadow_sm_white', name: 'shadow putih sedikit'},
                    {class: 'rbm_shadow_md_black', name: 'shadow hitam sedang'},
                    {class: 'rbm_shadow_md_white', name: 'shadow putih sedang'},
                    {class: 'rbm_shadow_lg_white', name: 'shadow putih tebal'},
                    {class: 'rbm_shadow_lg_black', name: 'shadow hitam tebal'},
                    ]
        }

    },

 
    

    mounted(){
        var hello = $('.ecommerce').html();

        

        var parseData = JSON.parse(hello);

        this.contentlink = parseData;

        this.contentlink.formHTML =  $('<div />').html(parseData.formHTML).text();

    },

    computed: {
    // a computed getter
        modalIdHref: function () {
          // `this` points to the vm instance
          return '#'+this.contentlink.id;
        },

        transparency: {
        // setter
        get: function () {
          return this.contentlink.transparency;
        },
        set: function (newValue) {
          this.contentlink.transparency = newValue;
          if(newValue == 'rbm_bd_trnsp')
          {
            this.contentlink.data_backdrop = false;
          }
        
        }
      }


    },


    



 



});

console.log(vm);
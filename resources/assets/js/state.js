import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
  },
  mutations: {
    increment (state, n) {
      state.count = n
    },
    init (state, n){
    	state[n.name] = n.value
    },
    push (state, n){
    	state[n.name].unshift(n.value) 
    },
    delete (state, n){
      var id = state[n.name].indexOf(n.id);

      state[n.name].splice(id,1);
    },
    edit (state, n){

      var id = state[n.name].indexOf(n.id);

      state[n.name][id] = n.value;

      // state[n.name].splice(id,1,n.value);

    }
  }
})

export default store
/* in ES 6 */
// import domtoimage from 'dom-to-image';
/* in ES 5 */
var domtoimage = require('dom-to-image');


function domtoImageConvert(){
var node = document.getElementById('modal-preview');

domtoimage.toPng(node)
    .then(function (dataUrl) {
        var img = new Image();
        img.src = dataUrl;
        document.body.appendChild(img);
        // console.log(dataUrl);
    })
    .catch(function (error) {
        console.error('oops, something went wrong!', error);
    });
}
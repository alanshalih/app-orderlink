window._ = require('lodash')

new Clipboard('.btn')

import Vue from 'vue'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import VueResource from 'vue-resource'




import Store from './state'

import VueHtml5Editor from 'vue-html5-editor'

Vue.use(VueHtml5Editor, {
    // 全局组件名称，使用new VueHtml5Editor(options)时该选项无效 
    // global component name
    name: "vue-html5-editor",
    // 是否显示模块名称，开启的话会在工具栏的图标后台直接显示名称
    // if set true,will append module name to toolbar after icon
    showModuleName: false,
    // 自定义各个图标的class，默认使用的是font-awesome提供的图标
    // custom icon class of built-in modules,default using font-awesome
    icons: {
      text: "custom-icons-edit",
      color: "custom-icons-brush",
      font: "custom-icons-font",
      align: "custom-icons-justify",
      list: "custom-icons-list",
      link: "custom-icons-link",
      unlink: "custom-icons-unlink",
      tabulation: "custom-icons-table",
      image: "fa fa-file-image-o",
      hr: "custom-icons-line",
      eraser: "custom-icons-eraser",
      undo: "custom-icons-undo",
      "full-screen": "fa fa-arrows-alt",
      info: "fa fa-info",
    },
    // 配置图片模块
    // config image module
    image: {
        // 文件最大体积，单位字节  max file size
        sizeLimit: 512 * 1024,
        // 上传参数,默认把图片转为base64而不上传
        // upload config,default null and convert image to base64
        upload: {
          url: null,
          headers: {},
          params: {},
          fieldName: {}
        },
        // 压缩参数,默认使用localResizeIMG进行压缩,设置为null禁止压缩
        // compression config,default resize image by localResizeIMG (https://github.com/think2011/localResizeIMG)
        // set null to disable compression
        compress: {
          width: 1600,
          height: 1600,
          quality: 80
        },
        // 响应数据处理,最终返回图片链接
        // handle response data，return image url
        uploadHandler(responseText){
            //default accept json data like  {ok:false,msg:"unexpected"} or {ok:true,data:"image url"}
            var json = JSON.parse(responseText)
            if (!json.ok) {
              alert(json.msg)
            } else {
              return json.data
            }
          }
        },
    // 语言，内建的有英文（en-us）和中文（zh-cn）
    //default en-us, en-us and zh-cn are built-in
    language: "en-us",
    // 自定义语言
    i18n: {
        //specify your language here
        "zh-cn": {
          "align": "对齐方式",
          "image": "图片",
          "list": "列表",
          "link": "链接",
          "unlink": "去除链接",
          "table": "表格",
          "font": "文字",
          "full screen": "全屏",
          "text": "排版",
          "eraser": "格式清除",
          "info": "关于",
          "color": "颜色",
          "please enter a url": "请输入地址",
          "create link": "创建链接",
          "bold": "加粗",
          "italic": "倾斜",
          "underline": "下划线",
          "strike through": "删除线",
          "subscript": "上标",
          "superscript": "下标",
          "heading": "标题",
          "font name": "字体",
          "font size": "文字大小",
          "left justify": "左对齐",
          "center justify": "居中",
          "right justify": "右对齐",
          "ordered list": "有序列表",
          "unordered list": "无序列表",
          "fore color": "前景色",
          "background color": "背景色",
          "row count": "行数",
          "column count": "列数",
          "save": "确定",
          "upload": "上传",
          "progress": "进度",
          "unknown": "未知",
          "please wait": "请稍等",
          "error": "错误",
          "abort": "中断",
          "reset": "重置"
        }
      },
    // 隐藏不想要显示出来的模块
    // the modules you don't want
    hiddenModules: [],
    // 自定义要显示的模块，并控制顺序
    // keep only the modules you want and customize the order.
    // can be used with hiddenModules together
    visibleModules: [
    "text",
    "color",
    "font",
    "align",
    "list",
    "link",
    "unlink",
    "tabulation",
    "hr",
    "eraser",
    "undo",
    ],
    // 扩展模块，具体可以参考examples或查看源码
    // extended modules
    modules: {
        //omit,reference to source code of build-in modules
      }
    })

Vue.use(Vuex)

Vue.use(Vuetify)

Vue.use(VueRouter)

Vue.use(VueResource)

// chartjs package
require('chart.js');
    // vue-charts package
    require('hchs-vue-charts');
    Vue.use(VueCharts);

    Vue.http.headers.common = {
      'X-CSRF-TOKEN': window.Laravel.csrfToken,
      'X-Requested-With': 'XMLHttpRequest'
    }



    window.store = Store

    Vue.component('mainlayout', require('./v3/mainlayout.vue'))

    const routes = [ 
    { path: '/', component: require('./v3/index.vue')},
    { path: '/token', component: require('./v3/generate-token.vue')},
    { path: '/email', component: require('./v3/email/layout.vue'), children : [{
      path : '/',
      component: require('./v3/email/home.vue')
    },
    {
      path : '/email/profile',
      component: require('./v3/email/profile.vue')
    }]},
    { path: '/pixelink', component: require('./v3/pixelink/index.vue')},
     { path: '/pixelink/create', component: require('./v3/pixelink/create.vue')},
    { path: '/orderlink', component: require('./v3/orderlink/index.vue')},
    { path: '/orderlink/create', component: require('./v3/orderlink/create.vue')},
    { path: '/store', component: require('./v3/store/index.vue')},
    { path: '/store/create', component: require('./v3/store/create.vue')},
    { path: '/store/:id', component: require('./v3/store/single.vue'),
    children: [
    {
                  // UserProfile will be rendered inside User's <router-view>
                  // when /user/:id/profile is matched
                  path: '/',
                  component: require('./v3/store/home.vue')
                },
                {
                  // UserProfile will be rendered inside User's <router-view>
                  // when /user/:id/profile is matched
                  path: 'products',
                  component: require('./v3/products/index-desktop.vue')
                },
                {
                  // UserProfile will be rendered inside User's <router-view>
                  // when /user/:id/profile is matched
                  path: 'setting',
                  component: require('./v3/store/setting.vue')
                },
                {
                  // UserProfile will be rendered inside User's <router-view>
                  // when /user/:id/profile is matched
                  path: 'products/create',
                  component: require('./v3/products/create-desktop.vue')
                },
                ,
                {
                  // UserProfile will be rendered inside User's <router-view>
                  // when /user/:id/profile is matched
                  path: 'products/:pid',
                  component: require('./v3/products/single-desktop.vue')
                },
                {
                  // UserProfile will be rendered inside User's <router-view>
                  // when /user/:id/profile is matched
                  path: 'orders',
                  component: require('./v3/pos/index-desktop.vue')
                },
                {
                  // UserProfile will be rendered inside User's <router-view>
                  // when /user/:id/profile is matched
                  path: 'orders/create',
                  component: require('./v3/pos/create-desktop.vue')
                },
                ,
                {
                  // UserProfile will be rendered inside User's <router-view>
                  // when /user/:id/profile is matched
                  path: 'orders/:oid',
                  component: require('./v3/pos/single-desktop.vue')
                },{
                  // UserProfile will be rendered inside User's <router-view>
                  // when /user/:id/profile is matched
                  path: 'sms-notifications',
                  component: require('./v3/store/sms-notifications.vue')
                }
                ,{
                  // UserProfile will be rendered inside User's <router-view>
                  // when /user/:id/profile is matched
                  path: 'email-notifications',
                  component: require('./v3/store/email-notifications.vue')
                }
                ]
              },
      // { path: '/pos', component: require('./v3/pos/index-desktop.vue')},
      // { path: '/pos/create', component: require('./v3/pos/create-desktop.vue')},
      // { path: '/pos/:id', component: require('./v3/pos/index-desktop.vue')},
      // { path: '/products', component: require('./v3/products/index-desktop.vue')},
      // { path: '/products/create', component: require('./v3/products/create-desktop.vue')},
      // { path: '/products/:id', component: require('./v3/products/single-desktop.vue')},

      ]

      const router = new VueRouter({
  routes // short for routes: routes
})

      const vm = new Vue({

        el: '#appv3',
        router,
        store,
      })



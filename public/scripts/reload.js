      $(document).on('click', '#list .item-title', function() { 
          $('#detail').block({
            message: '<i class="fa fa-lg fa-refresh fa-spin"></i>' ,
            css: {
              border: 'none', 
              backgroundColor: 'transparent',
              color: '#fff',
              padding: '30px',
              width: '100%'
            },
            timeout: 1000
          }); 
      });
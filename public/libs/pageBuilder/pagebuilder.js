
    var editM = false;

    var tempImageLocation = '';

    var tempBackgroundSelector = '';

    var tempBackgroundColor = '';

    var tempBackgroundImage = '';

    var tempBlock = '';

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


  function edit() 
  {
    $('.click2edit').summernote({focus: false,
      codemirror: { // codemirror options
      theme: 'monokai'
      },

        toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['fontsize', ['fontsize','fontname','style']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height','fullscreen','undo','redo','codeview']]
  ]

    });

  }

  function save() {
      $('.click2edit').each(function(  ) {
       $(this).summernote('destroy');
      })


      $('img').click(function() {
        tempImageLocation = $(this);
        console.log($('#bookId').val($(this).attr('src')));
      $('#myModal').modal();

      });
  }




  function editMode(e) {
      e.preventDefault();
      if(editM){
        editM = false;
        $('#edit-icon').removeClass('fa-check').addClass('fa-pencil');
        $('#edit-icon').parent().css('background-color', '#2196f3');
        save();
      }else{
        editM = true;
          $('#edit-icon').removeClass('fa-pencil').addClass('fa-check');
          $('#edit-icon').parent().css('background-color', '#ed5249');
        edit();
      }
  }

  function updateBackgroundColor(jscolor) {
      // 'jscolor' instance can be used as a string
      tempBackgroundColor = tempBackgroundSelector.css('background-color');

      console.log(jscolor);

      tempBackgroundSelector.css('background-color', '#'+jscolor);
  }

  function isParalax() {

      tempBackgroundSelector.css('background-attachment', 'fixed');
  }

   function updateBackgroundImage(value) {
      // variabel menyimpan background sebelum di edit, jadi ketika ga jadi di edit bs di restore
      tempBackgroundImage = tempBackgroundSelector.css('background-image');

      tempBackgroundSelector.css('background-image', 'url("'+value+'")');
  }

  function closeModalWithoutSave(){
      tempBackgroundSelector.css('background-color', tempBackgroundColor);

      tempBackgroundSelector.css('background-image', tempBackgroundImage);
      $('#modal-background').modal('hide');
  }

  function changeImage (data) {
      tempImageLocation.attr('src', data);
      $('#myModal').modal('hide');
  }



  $(document).ready(function(){
    
    $('img').click(function() {
    // console.log( $(this).attr('src','/stylish/img/portfolio-2.jpg') );
    tempImageLocation = $(this);
    ($('#bookId').val($(this).attr('src')));
      $('#myModal').modal();

     });
  });


  function click2EditHover(){
    $( '.click2edit' ).hover( 
    function(){
      tempBlock = $(this);
        var button = '<div id="overlay-button"><a  onclick="moveUp()" class="btn btn-outlined btn-primary"><i class="fa fa-arrow-up"></i></a><a onclick="moveDown()"  class="btn btn-outlined btn-success"><i class="fa fa-arrow-down"></i></a><a onclick="clone()" class="btn btn-outlined btn-info"><i class="fa fa-copy"></i></a><a onclick="changeBackground()" class="btn btn-outlined btn-warning"><i class="fa fa-file-image-o"></i></a><a onclick="hapusBlock()" class="btn btn-outlined btn-danger"><i class="fa fa-trash"></i></a></div>'
        

        if($( this ).children().length == 1){
          $(this).prepend(button);
        }
  }, function(){

    // $(this).children().first().remove();
  
    if($( this ).children().length == 2){
          $(this).children().first().remove();
        }

  } );
  }
  click2EditHover();

  function moveUp(){
    tempBlock.prev().insertAfter(tempBlock);
  }

   function moveDown(){
    tempBlock.next().insertBefore(tempBlock);
  }

  function hapusBlock(){
    tempBlock.remove();
  }

  function clone(){
    // console.log(tempBlock);
     if(tempBlock.children().length == 2){
          tempBlock.children().first().remove();
          tempBlock.clone().insertBefore( tempBlock );
          click2EditHover();
        }
    // 
  }

  function changeBackground(){

    tempBackgroundSelector = (tempBlock.children().last());
    var uri = (tempBackgroundSelector.css('background-image')).replace('url("', "").replace('")',"");
    ($('#gambar').val(uri));
    ($('#color').val(tempBackgroundSelector.css('background-color')));
    $('#modal-background').modal();

  }

  function savePage(){
    var text = '';
    $('.click2edit').each(function( index ) {
      text=text+'<div class="click2edit">'+( $( this ).html() )+'</div>';
    });

    $('#simpan-data').val(text);

    $('#simpan-form').submit();

  }


